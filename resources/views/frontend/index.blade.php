
@extends('frontend.master')
@section('title','E-SHOP || HOME PAGE')
@section('main-content')



<main class="main">
            <div class="page-content">

                <section class="intro-section">
                    @if(count($banners)>0)
                    @foreach($banners as $key=>$banner)
                   
                    @endforeach
 
                    <div class=" intro-slider owl-carousel owl-theme owl-nav-arrow row animation-slider cols-1 gutter-no mb-8 "
                        data-owl-options="{
                                'nav': true,
                                'dots': false,
                                'loop': false,
                                'items': 1,
                                'responsive': {
                                    '0': {
                                        'nav': false,
                                        'autoplay': true
                                    },
                                    '768': {
                                        'nav': true
                                    }
                                }
                            }">
                            
                             @foreach($banners as $key=>$banner)
                        <div class="banner banner-fixed banner1" >
                            <figure >
                                <img class="img-fluid" src="{{$banner->photo}}" alt="banner" width="1903" height="400"
                                    style="background-color: #f8f6f6;">
                            </figure>
                            <div class="banner-content y-50 pb-1">
                                <h4 class="banner-subtitle title-underline2 font-weight-normal text-dim slide-animate appear-animate"
                                    data-animation-options="{
                                                'name': 'fadeInUpShorter',
                                                'delay': '.2s'
                                            }">
                                    <span>{!! html_entity_decode($banner->description) !!}</span></h4>
                                <h3 class="banner-title text-dark lh-1 mb-7 slide-animate appear-animate"
                                    data-animation-options="{
                                                'name': 'fadeInUpShorter',
                                                'delay': '.4s'
                                            }">
                                   {{$banner->title}}</h3>
                                <a href="{{route('product-grids')}}" class="btn btn-dark slide-animate appear-animate"
                                    data-animation-options="{
                                                'name': 'fadeInUpShorter',
                                                'delay': '.6s'
                                            }">SHop
                                    now<i class="p-icon-arrow-long-right"></i></a>
                            </div>
                        </div>
                        
                           @endforeach   
                    </div>
               
                    @endif

                   

                   
                    <div class="container">
                        <div class="owl-carousel owl-theme owl-box-border row cols-md-3 cols-sm-2 cols-1 appear-animate"
                            data-owl-options="{
                                                'nav': false,
                                                'dots': false,
                                                'loop': false,
                                                'responsive': {
                                                    '0': {
                                                        'items': 1,
                                                        'autoplay': true
                                                    },
                                                    '576': {
                                                        'items': 2,
                                                        'autoplay': true
                                                    },
                                                    '768': {
                                                        'items': 3,
                                                        'dots': false
                                                    }
                                                }
                                            }">
                            <div class="icon-box icon-box-side">
                                <span class="icon-box-icon">
                                    <i class="p-icon-shipping-solid"></i>
                                </span>
                                <div class="icon-box-content">
                                    <h4 class="icon-box-title">FREE SHIPPING & RETURN</h4>
                                    <p>Free shipping on orders over Rs 99</p>
                                </div>
                            </div>
                            <div class="icon-box icon-box-side">
                                <span class="icon-box-icon">
                                    <i class="p-icon-quality"></i>
                                </span>
                                <div class="icon-box-content">
                                    <h4 class="icon-box-title">QUALITY GUARANTEED</h4>
                                    <p>We offer high quality of products</p>
                                </div>
                            </div>
                            <div class="icon-box icon-box-side">
                                <span class="icon-box-icon">
                                    <i class="p-icon-fax2"></i>
                                </span>
                                <div class="icon-box-content">
                                    <h4 class="icon-box-title">SECURE PAYMENT</h4>
                                    <p>We ensure secure payment!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="container mt-1 mb-3 pt-7 mb-7 appear-animate">
                    
                    <div class="tab tab-nav-center product-tab product-tab-type2">
                         
                        <ul class="nav nav-tabs cennterr">
                            @if($product_lists)
                        @foreach($product_lists->skip(5)->take(3) as $key=>$product)
                             @php 
                            $photo=explode(',',$product->photo);
                            // dd($photo);
                            @endphp
                            <li class="nav-item imgg">
                                <a class="nav-link " href="{{route('product-detail',$product->slug)}}">
                                    <figure>
                                        <img src="{{$photo[0]}}"
                                            alt="Nav img"  height="700px !important"/>
                                    </figure>
                                    <div class="nav-title"> {{$product->title}}</div>
                                </a>
                            </li>
                           <!--  <li class="nav-item">
                                <a class="nav-link" href="#fruits2">
                                    <figure>
                                        <img src="{{asset('frontend/images/elements/product_tab/nav-2.jpg')}}" width="160" height="130"
                                            alt="Nav img" />
                                    </figure>
                                    <div class="nav-title">Fruits</div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#coffees">
                                    <figure>
                                        <img src="{{asset('frontend/images/elements/product_tab/nav-3.jpg')}}" width="160" height="130"
                                            alt="Nav img" />
                                    </figure>
                                    <div class="nav-title">Coffees</div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#meats">
                                    <figure>
                                        <img src="{{asset('frontend/images/elements/product_tab/nav-4.jpg')}}" width="160" height="130"
                                            alt="Nav img" />
                                    </figure>
                                    <div class="nav-title">Meats</div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#vegetables">
                                    <figure>
                                        <img src="{{asset('frontend/images/elements/product_tab/nav-5.jpg')}}" width="160" height="130"
                                            alt="Nav img" />
                                    </figure>
                                    <div class="nav-title">Vegetables</div>
                                </a>
                            </li> -->
                            @endforeach
                            @endif
                        </ul>
                        </div>
                        <div style="display: flex;" class="respbtn"><button class="w-100 btn" style="margin-right: 2px;">New Arrivel</button> <button class="w-100 btn">New Produsts</button></div>
                        <div class="tab-content">
                            <div class="tab-pane active" id="canned">
                                <div class="row cols-lg-4 cols-md-3 cols-2" data-owl-options="{
                                            'items': 4,
                                            'nav': false,
                                            'dots': true,
                                            'margin': 20,
                                            'loop': false,
                                            'responsive': {
                                                '0': {
                                                    'items': 2
                                                },
                                                '768': {
                                                    'items': 3
                                                },
                                                '992': {
                                                    'items': 4
                                                }
                                            }
                                        }">
                                        @php
                                        $product_lists=DB::table('products')->where('is_featured',1)->where('status','active')->orderBy('id','DESC')->limit(25)->skip(8)->take(28)->get();
                                        @endphp

                                @if($product_lists)
                                @foreach($product_lists as $key=>$product)
                                <div class="">
                                <div class="product-wrap bordr" style="box-sizing: border-box;">
                           
                                    <div class="product shadow-media text-center" style="padding-bottom: 10px">
                                        <figure class="product-media">
                                            <a href="{{route('product-detail',$product->slug)}}">
                                                @php 
                                                $photo=explode(',',$product->photo);
                                                // dd($photo);
                                                @endphp
                                               <img style="padding:60px 20px; background-color: #f8f8f8; box-shadow: 7px 7px 5px -6px #888; margin-bottom: 14px;" src="{{$photo[0]}}" alt="product" />
                                                <img class="hover-img" src="{{$photo[0]}}" alt="{{$photo[0]}}" width="295"
                                                    height="369" />

                                            </a>
                                            @if($product->discount)
                                                    <span class="price-dec">{{$product->discount}} % Off</span>
                                                        
                                                    @else
                                                        
                                                    
                                                    @endif

                                            {{-- <div class="product-action-vertical">
                                                
                                                <a title="Wishlist" href="{{route('add-to-wishlist',$product->slug)}}" class="wishlist" data-id="{{$product->id}}"><i class=" p-icon-heart-solid "></i></a>

                                                <div class="product-action-2">
                                                        <a title="Add to cart" href="{{route('add-to-cart',$product->slug)}}" style="margin-right: 18px;">
                                                           <i class=" p-icon-cart-solid "></i>
                                                        </a>
                                                    </div>
                                               
                                            </div> --}}
                                        </figure>
                                        <div class="product-details">
                                            {{-- <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:60%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="product-simple.html#content-reviews"
                                                    class="rating-reviews">(14)</a>
                                            </div> --}}
                                            <h5 class="product-name">
                                                <a href=" {{route('product-detail',$product->slug)}}">
                                                  {{$product->title}}
                                                </a>
                                            </h5>
                                             @php
                                             $after_discount=($product->price-($product->price*$product->discount)/100);
                                             @endphp
                                            <span class="product-price">
                                                @if(!empty($product->discount))
                                                <del class="old-price">Rs{{number_format($product->price,2)}}</del>
                                               @endif
                                                <ins class="new-price">Rs{{number_format($after_discount,2)}}</ins>
                                                
                                               
                                            </span>
                                            
                                            
                                        </div>
                                        <span class="product-price buy">
                                            <a href="{{route('product-detail',$product->slug)}}">Buy Now</a>
                                         </span>
                                    </div>


                               
                                </div>
                            </div>
                                @endforeach
                                @endif
                               

    

                                </div>
                            </div>
                           
                            
                
                            </div>
                        </div>
                    </div>
                
                {{-- </section>
                 <section class="feature-section appear-animate">
                    <div class="container pt-8 mb-10 pb-2">
                        
                        <div class="row cols-lg-5 cols-md-3 cols-2"
                            data-owl-options="{
                                            'items': 5,
                                            'nav': false,
                                            'dots': true,
                                            'margin': 20,
                                            'loop': false,
                                            'responsive': {
                                                '0': {
                                                    'items': 2
                                                },
                                                '768': {
                                                    'items': 3,
                                                    'dots': true
                                                },
                                                '992': {
                                                    'items': 5
                                                },
                                                '1400': {
                                                    'nav': true,
                                                    'dots': false
                                                }
                                            }
                                        }">
                                        @php
                                       $featured=DB::table('products')->where('is_featured',1)->where('status','active')->orderBy('id','DESC')->limit(22)->skip(8)->take(25)->get();
                                       @endphp
                                        @if($featured)
                                 @foreach($featured as $data)
                              <div class="product shadow-media text-center bordr">
                                <figure class="product-media">
                                      @php 
                                     $photo=explode(',',$data->photo);
                                     @endphp
                                    <a href="{{route('product-detail',$data->slug)}}">
                                        <img style="padding:60px 20px; background-color: #f8f8f8; box-shadow: 7px 7px 5px -6px #888;" src="{{$photo[0]}}" alt="product" width="232"
                                            height="290" />
                                            <img class="hover-img" src="{{$photo[0]}}" alt="{{$photo[0]}}" width="295"
                                                    height="369" />
                                    </a>
                                    <span class="price-dec">{{$data->discount}}%</span> --}}
                                    {{-- <div class="product-action-vertical">
                                                
                                                <a title="Wishlist" href="{{route('add-to-wishlist',$product->slug)}}" class="wishlist" data-id="{{$product->id}}"><i class=" p-icon-heart-solid "></i></a>

                                                <div class="product-action-2">
                                                        <a title="Add to cart" href="{{route('add-to-cart',$product->slug)}}" style="margin-right: 18px;">
                                                           <i class=" p-icon-cart-solid "></i>
                                                        </a>
                                                    </div>
                                               
                                            </div> --}}
                                {{-- </figure>
                                <div class="product-details"> --}}
                                    {{-- <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:60%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="product-simple.html#content-reviews" class="rating-reviews">(12)</a>
                                    </div> --}}
                                    
                                    {{-- <h5 class="product-name">
                                        <a href="product-simple.html">
                                         {{$data->title}}
                                        </a>
                                    </h5>
                                    <span class="product-price buy">
                                       <a href="{{route('product-detail',$data->slug)}}">Buy Now</a>
                                    </span>
                                    
                                </div>
                            </div>
                             @endforeach
                             @endif
                            <!-- End .product -->
                            
                        
                        </div>
                    </div>
                  
                </section> --}}
                 {{-- <section class="feature-section appear-animate">
                    <div class="container mt-10 pt-8 mb-10 pb-2">
                        
                        <div class="owl-carousel owl-theme owl-nav-arrow owl-nav-outer owl-nav-image-center row cols-lg-5 cols-md-3 cols-2"
                            data-owl-options="{
                                            'items': 5,
                                            'nav': false,
                                            'dots': true,
                                            'margin': 20,
                                            'loop': false,
                                            'responsive': {
                                                '0': {
                                                    'items': 2
                                                },
                                                '768': {
                                                    'items': 3,
                                                    'dots': true
                                                },
                                                '992': {
                                                    'items': 5
                                                },
                                                '1400': {
                                                    'nav': true,
                                                    'dots': false
                                                }
                                            }
                                        }">
                                        @php
                        $product_lists=DB::table('products')->where('status','active')->orderBy('id','DESC')->limit(6)->skip(14)->take(6)->get();
                    @endphp
                    @foreach($product_lists as $product)
                              <div class="product shadow-media text-center">
                                <figure class="product-media">
                                      @php 
                                            $photo=explode(',',$product->photo);
                                            // dd($photo);
                                        @endphp
                                    <a href="{{route('product-detail',$product->slug)}}">
                                        <img style="padding:60px 20px; background-color: #f8f8f8; box-shadow: 7px 7px 5px -6px #888;" src="{{$photo[0]}}" alt="product" width="232">
                                      <img class="hover-img" src="{{$photo[0]}}" alt="{{$photo[0]}}" width="295"
                                                    height="369" />
                                    </a>
                                    <span class="price-dec">Rs{{number_format($product->discount,2)}}</span>
                                    <div class="product-action-vertical">
                                                
                                                <a title="Wishlist" href="{{route('add-to-wishlist',$product->slug)}}" class="wishlist" data-id="{{$product->id}}"><i class=" p-icon-heart-solid "></i></a>

                                                <div class="product-action-2">
                                                        <a title="Add to cart" href="{{route('add-to-cart',$product->slug)}}" style="margin-right: 18px;">
                                                           <i class=" p-icon-cart-solid "></i>
                                                        </a>
                                                    </div>
                                               
                                            </div>
                                </figure>
                                <div class="product-details">
                                
                                    <h5 class="product-name">
                                        <a href="product-simple.html">
                                         {{$product->title}}
                                        </a>
                                    </h5>
                                   
                                </div>
                            </div>
                             @endforeach
                            <!-- End .product -->
                            
                        
                        </div>
                    </div>
                  
                </section> --}}
                <section class="recent-section container mt-10 pt-7 mb-10 pb-6">
                    <h4 class="subtitle title-underline2 text-uppercase text-center" style="    width: 100%;
    /*border-bottom: 2px solid rgba(255,156,40,0.3);
    border-top: 2px solid rgba(255,156,40,0.3);*/
   background-color: #f8f8f8 !important;
    padding: 12px;
    font-weight: 600;
"><span>Our Blog</span>
                    </h4>
                    <h2 class="title justify-content-center text-center">Recent Stories And Articles</h2>
                    <div class="owl-carousel owl-theme row cols-lg-3 cols-sm-2 cols-1 mb-10 pb-8" data-owl-options="{
                                        'items': 3,
                                        'nav': false,
                                        'dots': true,
                                        'margin': 20,
                                        'loop': false,
                                        'responsive': {
                                            '0': {
                                                'items': 1
                                            },
                                            '568': {
                                                'items': 2
                                            },
                                            '992': {
                                                'items': 3
                                            }
                                        }
                                }">
                                @if($posts)
                        @foreach($posts as $post)
                        <div class="post post-border post-center overlay-zoom overlay-dark">
                            <figure class="post-media">
                                <a href="{{route('blog.detail',$post->slug)}}">
                                    <img src="{{$post->photo}}" width="400" height="250" alt="post">
                                </a>
                                <div class="post-calendar">
                                   {{$post->created_at->format('d M , Y. D')}}
                                </div>
                            </figure>
                            <div class="post-details">
                               
                                <h3 class="post-title"><a href="{{route('blog.detail',$post->slug)}}">{{$post->title}}</a>
                                </h3>
                                 @php 
                                    $author_info=DB::table('users')->select('name')->where('id',$post->added_by)->get();
                                    @endphp
                                <div class="post-meta">
                                    <span class="float-right">
                                                <i class="fa fa-user" aria-hidden="true"></i> 
                                                @foreach($author_info as $data)
                                                    @if($data->name)
                                                        {{$data->name}}
                                                    @else
                                                        Anonymous
                                                    @endif
                                                @endforeach
                                            </span>
                                    
                                    
                                </div>
                                <p class="post-content">{!! html_entity_decode($post->summary) !!}<a href="{{route('blog.detail',$post->slug)}}" class="ml-2 text-primary">Continue Reading</a></p>
                            </div>
                        </div>
                          @endforeach
                        @endif
                        
                    </div>
                  
                   


                </section>
               <!--  <section class="benefit-section appear-animate" style="background: #fafaf8;">
                    <div class="container">
                        <h4 class="subtitle title-underline2 text-uppercase text-center"><span>Why Top Products?</span>
                        </h4>
                        <h2 class="title justify-content-center text-center pb-6 mb-10">100% Natural, 100% Organic</h2>
                        <figure class="img-back floating">
                            <img class="layer" src="{{asset('frontend/images/demos/demo1/banner/banner1.jpg')}}" width="674" height="514"
                                alt="banner" />
                        </figure>
                        <div class="row appear-animate">
                            <div class="col-md-6">
                                <div class="icon-box ml-md-4">
                                    <span class="icon-box-icon">
                                        <i class="p-icon-heartbeat-solid" style="font-size: 2.05em;"></i>
                                    </span>
                                    <div class="icon-box-content">
                                        <h4 class="icon-box-title">Good for Health</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur
                                            eiusmod tempor incididunt ut labore.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pos-right">
                                <div class="icon-box mr-md-4">
                                    <span class="icon-box-icon" style="margin-bottom: 1.9rem;">
                                        <i class="p-icon-quality"></i>
                                    </span>
                                    <div class="icon-box-content">
                                        <h4 class="icon-box-title">High Nutrition</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur
                                            eiusmod tempor incididunt ut labore.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="icon-box ml-md-4">
                                    <span class="icon-box-icon" style="margin-bottom: 2rem;">
                                        <i class="p-icon-fruit"></i>
                                    </span>
                                    <div class="icon-box-content">
                                        <h4 class="icon-box-title">Always Fresh</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur
                                            eiusmod tempor incididunt ut labore.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pos-right">
                                <div class="icon-box mr-md-4">
                                    <span class="icon-box-icon" style="margin-bottom: 15px;">
                                        <i class="p-icon-filter" style="font-size: 1.9em;"></i>
                                    </span>
                                    <div class="icon-box-content">
                                        <h4 class="icon-box-title">No Fertilizer</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur
                                            eiusmod tempor incididunt ut labore.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                -->
               
                
            </div>
        </main>
        @endsection
       