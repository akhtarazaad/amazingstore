@extends('frontend.master')

@section('title','E-SHOP || Register Page')

@section('main-content')


    <style>
        .column {
            width: 334px;
            padding: 10px;
        }

        #featured {
            max-width: 800px;
            max-height: 600px;

            cursor: pointer;
            border: 1px solid gray;

        }

        .thumbnail {
            object-fit: cover;
            max-width: 180px;
            max-height: 100px;
            cursor: pointer;
            opacity: 0.5;
            margin: 5px;
            border: 1px solid gray;

        }

        .thumbnail:hover {
            opacity: 1;
        }

        .active {
            opacity: 1;
        }

        #slide-wrapper {
            margin-top: 30px;
            max-width: 500px;
            display: flex;
            min-height: 80px;
            align-items: center;
        }

        #slider {
            width: 340px;
            display: flex;
            flex-wrap: nowrap;
            overflow-x: auto;
            margin-left: -10px;
        }

        #slider::-webkit-scrollbar {
            width: 8px;

        }

        #slider::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);

        }

        #slider::-webkit-scrollbar-thumb {
            background-color: #dede2e;
            outline: 1px solid slategrey;
            border-radius: 100px;

        }

        #slider::-webkit-scrollbar-thumb:hover {
            background-color: #18b5ce;
        }

        .arrow {
            width: 30px;
            height: 30px;
            cursor: pointer;
            transition: .3s;
        }

        .arrow:hover {
            opacity: .5;
            width: 35px;
            height: 35px;
        }

        #img-container {
            z-index: 1;
            max-width: 200px;
            position: absolute;
        }

        #lens {
            z-index: 2;
            position: absolute;

            height: 150px;
            width: 150px;
            border: 1px solid gray;
            background-repeat: no-repeat;
            cursor: none;

        }
    </style>


        <!-- End Header -->
        <div class="main single-product">
            <nav class="breadcrumb-nav">
                <div class="container">
                    <div class="product-navigation">
                        <ul class="breadcrumb">
                            <li><a href="{{ route('home') }}">Home</a></li>
                            <li><a href="{{ route('product-grids') }}">Products</a></li>
                            <li>Product Detail</li>
                        </ul>


                    </div>
                </div>
            </nav>


            @php
                $photo = explode(',', $product_detail->photo);
                // dd($photo);
            @endphp
{{-- <input type="hidden" name="tittle" value="{{ $product_detail->cat_info['title'] }}"> --}}
            <div class="row justify-content-center" style="margin-left: 10px;">
                <div class="col-md-4">
                    @foreach ($photo as $data)
                    <div id="img-container" style="width: 400px">
                        <img src="{{ asset($data) }}" />
                    </div>
                        <div id="slide-wrapper">


                            <div id="slider">
                                <img class="thumbnail active" src="{{ asset($data) }}">
                                <img class="thumbnail" src="{{ asset($data) }}">
                                <img class="thumbnail" src="{{ asset($data) }}">

                            </div>
                        </div>
                        @if (session('error'))
                            <div class="alert alert-danger alert-dismissable fade show">
                                <button class="close" data-dismiss="alert" aria-label="Close">×</button>
                                {{ session('error') }}
                            </div>
                        @endif
                </div>

                {{-- {{session('érror')}} --}}
                {{-- from start here --}}
                <div class="col-md-4">
                    <form name="placeorder" id="placeorder" class="form" method="post"
                        action="{{ route('placeorder.submit') }}">

                        @csrf

                        <input type="hidden" name="products_id" value="{{ $product_detail }}">

                        <div class="product-details">


                            <h1 class="product-name">{{ $product_detail->title }}</h1>
                            <div class="product-meta">
                                <label>CATEGORIES:</label><a
                                    href="{{ route('product-cat', $product_detail->cat_info['slug']) }}">{{ $product_detail->cat_info['title'] }}</a><br>
                                @if ($product_detail->sub_cat_info)
                                    <label>Stock:</label><a
                                        href="{{ route('product-sub-cat', [$product_detail->cat_info['slug'], $product_detail->sub_cat_info['slug']]) }}">{{ $product_detail->sub_cat_info['title'] }}</a>
                                    </p>
                                @endif


                            </div>
                            
                         

                            @php
                                $after_discount = $product_detail->price - ($product_detail->price * $product_detail->discount) / 100;
                            @endphp
                            <p class="product-price mb-1">
                                <del class="old-price">Rs{{ number_format($product_detail->price, 2) }}</del>
                                <ins class="new-price">Rs{{ number_format($after_discount, 2) }}</ins>
                            </p>
                            <input name="total_amount" type="hidden" value="{{ $after_discount }}">
                            <p class="product-short-desc">{!! $product_detail->summary !!}
                            </p>
                            @if ($product_detail->size)
                                <div class="product-form product-unit mb-2 pt-1">
                                    <label>Size</label>
                                    <div class="product-form-group pt-1">
                                        @php
                                            $sizes = explode(',', $product_detail->size);
                                            // dd($sizes);
                                        @endphp
                                        <div class="product-variations mb-1">
                                            @foreach ($sizes as $size)
                                                <a href="#">{{ $size }}</a>
                                            @endforeach
                                        </div>
                                    </div>

                                </div>
                            @endif



                            {{-- </div> --}}
                            <div class="input-group">
                                <button type="button" class="quantity-minus p-icon-minus-solid"
                                    data-field="quantity"></button>
                                <input type="hidden" name="slug" value="{{ $product_detail->slug }}">
                                <input type="text" name="quantity" class="input-number" data-min="1" data-max="1000"
                                    value="1" id="quantity">
                                <div class="button plus">
                                    <button class="quantity-plus p-icon-plus-solid" style="padding: 11px" data-field="quantity"></button>
                                </div>
                            </div>

                        </div>
                        @endforeach





                        {{-- <div class="product-form product-qty pt-1 mb-5"> --}}




                        {{-- </div> --}}





                        <input type="radio" name="payment_method" value="cash on delivery">
                        <label>cash on delivery</label><br>


                        <input type="radio" name="payment_method" value="JazzCash">
                        <label>JazzCash & Easypaisa</label>





                       

                </div>

                <div class="col-md-4">
                    <div class="login-forms">
                        <h2>QUICK BUY</h2>
                        <p>Please register in order to checkout more quickly</p>


                        <div class="okman">
                            <div class="col-12">
                                <div class="form-groups">

                                    <input style="color: gray" class="inpt" type="text" name="first_name"
                                        placeholder="Your Name *" required="required" value="{{ old('name') }}">
                                    @error('first_name')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-groups">

                                    <input style="color: gray" class="inpt" type="text" name="email"
                                        placeholder="Your Email *" required="required" value="{{ old('email') }}">
                                    @error('email')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-groups">

                                    <input style="color: gray" class="inpt" type="text" name="phone"
                                        placeholder="Your Phone_No *" required="required" value="{{ old('phone') }}">
                                    @error('phone')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>



                            <div class="col-12">
                                <div class="form-groups">

                                    <input style="color: gray" class="inpt" type="text" name="address"
                                        placeholder="Enter_address" required="required" value="{{ old('address') }}">
                                    @error('address')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group login-btn" style="margin: 10px">
                                    <button class="btn btnnn" type="submit">Buy Now</button>
                                    {{-- <a href="{{route('login.form')}}" class="btn btnnn">Login</a> --}}


                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>




            {{-- </section> --}}

            </form>


            <div class="mt-2 iconsss" align="center">

               

                <form name="singleaddtocard" id="singleaddtocard" action="{{ route('single-add-to-cart') }}"
                    method="POST">
                    @csrf

                    <input type="hidden" name="slug" value="{{ $product_detail->slug }}">
                    <input type="hidden" name="quantity" class="input-number" data-min="1" data-max="1000" value="1"
                        id="quantity">
                    <button class="btn btn-product btn-cart ls-normal font-weight-semi-bold" type="submit"
                        style="width: 100%; background-color: #0DA7E6; color: white; border-radius: 16px;"><i class="p-icon-cart-solid mr-2" style="font-weight: 700"></i>Add To Card</button>

                </form>
                <a href="https://api.whatsapp.com/send?phone=03036682519"><button
                    class="whtsapp btn-product"><i style="font-size: 25px"
                        class="fa-brands fa-whatsapp mr-3"></i>ORDER ON WHATSAPP
                </button></a> 
            </div>


            <div class="product-content">
                <div class="content-description" style="margin: 14px">
                    <h2 class="title title-line title-underline mb-lg-8">
                        {{-- <span>
                            Description
                        </span> --}}
                    </h2>
                    {{-- <h3 class="content-title">
                About this Product
            </h3> --}}
                    {{-- <div class="row">
                <div class="col-12 col-md-6 col-lg-6 d-flex align-items-center">
                    <div class="inner-video banner banner-fixed banner-video overlay-dark">
                        <figure>
                            @foreach ($photo as $data)
                                <a href="#">
                                    <img src="{{ asset($data) }}" alt="product-banner" width="610" height="400"
                                        style="background-color: #526e45;">
                                </a>
                                <video>
                                    <source src="video/memory-of-a-woman.mp4" type="video/mp4">
                                </video>
                            @endforeach
                        </figure>
                        <div class="banner-content text-center y-50 x-50">
                            <a class="video-btn video-play" href="video/memory-of-a-woman.mp4"
                                data-video-source="hosted"><i class="fas fa-play"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-6 with-content-index pt-3 pt-md-0 content-index-1 pl-2 pl-lg-7">

                    <p class="mb-3">
                        {!! $product_detail->description !!}
                    </p>
                </div>
            </div> --}}


                </div>


            </div>
           </div>
    </div>
    <div class="container">
        <section class="mt-3">
            <h2 class="text-center mb-7">Related Products</h2>
            <div class="row cols-lg-4 cols-md-3 cols-2" data-owl-options="{
                'items': 4,
                'nav': false,
                'dots': true,
                'margin': 20,
                'loop': false,
                'responsive': {
                    '0': {
                        'items': 2
                    },
                    '768': {
                        'items': 3
                    },
                    '992': {
                        'items': 4
                    }
                }
            }">
                {{-- {{$product_detail->rel_prods}} --}}
                @foreach ($product_detail->rel_prods as $data)
                    @if ($data->id !== $product_detail->id)
                        <div class="product-wrap bordr">
                            <div class="product shadow-media text-center" style="padding-bottom: 10px">
                                <figure class="product-media">
                                    <a href="{{ route('product-detail', $data->slug) }}">
                                        @php
                                            $photo = explode(',', $data->photo);
                                        @endphp
                                        <img style="padding:60px 20px; background-color: #f8f8f8; box-shadow: 7px 7px 5px -6px #888; margin-bottom: 14px;"
                                            src="{{ asset($photo[0]) }}" alt="product" width="295" height="369" />
                                        <img class="hover-img" src="{{ asset($photo[0]) }}"
                                            alt="{{ $photo[0] }}">

                                    </a>
                                    <span class="price-dec">{{ $data->discount }} % Off</span>
                                    {{-- <span class="out-of-stock">Hot</span> --}}
                                    <div class="product-label-group">
                                        <label class="product-label label-new">New</label>
                                    </div>
                                    {{-- <div class="product-action-vertical">
                                                
                                                <a title="Wishlist" href="{{route('add-to-wishlist',$data->slug)}}" class="wishlist" data-id="{{$data->id}}"><i class=" p-icon-heart-solid "></i></a>

                                                <div class="product-action-2">
                                                        <a title="Add to cart" href="{{route('add-to-cart',$data->slug)}}" style="margin-right: 18px;">
                                                           <i class=" p-icon-cart-solid "></i>
                                                        </a>
                                                    </div>
                                               
                                            </div> --}}
                                </figure>

                                <div class="product-details">
                                    {{-- <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:60%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="product-simple.html#content-reviews"
                                                    class="rating-reviews">(14)</a>
                                            </div> --}}
                                    <h5 class="product-name">
                                        <a href=" {{ route('product-detail', $data->slug) }}">
                                            {{ $data->title }}
                                        </a>
                                    </h5>
                                    @php
                                        $after_discount = $data->price - ($data->discount * $data->price) / 100;
                                    @endphp
                                    <span class="product-price">
                                        <del class="old-price">Rs{{ number_format($data->price, 2) }}</del>
                                        <ins class="new-price">Rs{{ number_format($after_discount, 2) }}</ins>
                                    </span>

                                </div>
                                <span class="product-price buy">
                                    <a>Buy Now</a>
                                </span>
                            </div>
                            <!-- End .product -->
                        </div>
                    @endif
                @endforeach

            </div>
            <!-- End .row -->
        </section>

    </div>
    </div>
    <!-- End Main -->
    @endsection