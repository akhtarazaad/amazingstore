@extends('frontend.master')

@section('title','E-SHOP || About Us')

@section('main-content')

 <main class="main">
            <div class="page-header" style="background-color: #f9f8f4; padding: 50px;">
                <h1 class="page-title font-weight-light text-capitalize pt-2 text-center">About Us</h1>
            </div><br>
            <div class="page-content about-page">
                <div class="container">
                    <section class="row align-items-center">
                    	@php
                        $settings=DB::table('settings')->get();
                        @endphp
                        <div class="col-lg-6">
                            <figure>
                                <img src="@foreach($settings as $data) {{$data->photo}} @endforeach" alt="@foreach($settings as $data) {{$data->photo}} @endforeach" width="610" height="450" alt="image" />
                            </figure>
                        </div>
                        <div class="col-lg-6">
                            <div class="pl-lg-8 pr-lg-3 pt-5 pt-lg-0">
                                
                                <h2 class="desc-title mb-4">Welcome To <span>Amazing Store</span></h2>
                                <p class="mb-3 ml-1">@foreach($settings as $data) {{$data->description}} @endforeach </p>
                                <p class="mb-8 ml-1">
                                    Porttitor rhoncus dolor purus non enim praesent elementum facilisis leo.
                                </p>
                               <div class="button">
                                <a href="{{route('blog')}}" class="btn">Our Blog</a>
                                <a href="{{route('contact')}}" class="btn primary">Contact Us</a>
                            </div>
                            </div>
                        </div>
                    </section>
                </div>
               
            </div>
        </main>
@endsection