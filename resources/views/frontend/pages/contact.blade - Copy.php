@extends('frontend.master')
@section('title','Amazing Store || Contact Us')
@section('main-content')
@php
$settings=DB::table('settings')->get();
@endphp
	 <main class="main">
            <div class="page-header" style="background-color: #f9f8f4; padding: 50px;">
                <h1 class="page-title font-weight-light text-capitalize pt-2 text-center">Contact Us</h1>
            </div>
            <div class="page-content contact-page">
                <div class="container">
                    <section class="mt-10 pt-8">
                        <h2 class="title title-center mb-8">Contact Information</h2>
                        <div class="owl-carousel owl-theme row cols-lg-4 cols-md-3 cols-sm-2 cols-1 mb-10"
                            data-owl-options="{
                                'nav': false,
                                'dots': false,
                                'loop': false,
                                'margin': 20,
                                'autoplay': true,
                                'responsive': {
                                    '0': {
                                        'items': 1,
                                        'autoplay': true
                                    },
                                    '576': {
                                        'items': 2
                                    },
                                    '768': {
                                        'items': 3
                                    },
                                    '992': {
                                        'items': 4,
                                        'autoplay': false
                                    }
                                }
                            }">
                            <div class="icon-box text-center">
                                <span class="icon-box-icon mb-4">
                                    <i class="p-icon-map"></i>
                                </span>
                                <div class="icon-box-content">
                                    <h4 class="icon-box-title">Address</h4>
                                    <p class="text-dim">@foreach($settings as $data) {{$data->address}} @endforeach</p>
                                </div>
                            </div>
                            <div class="icon-box text-center">
                                <span class="icon-box-icon mb-4">
                                    <i class="p-icon-phone-solid"></i>
                                </span>
                                <div class="icon-box-content">
                                    <h4 class="icon-box-title">Call us Now:</h4>
                                    <p class="text-dim">@foreach($settings as $data) {{$data->phone}} @endforeach</p>
                                </div>
                            </div>
                            <div class="icon-box text-center">
                                <span class="icon-box-icon mb-4">
                                    <i class="p-icon-message"></i>
                                </span>
                                <div class="icon-box-content">
                                    <h4 class="icon-box-title">E-mail Address</h4>
                                    <p class="text-dim"><a href="mailto:info@yourwebsite.com">@foreach($settings as $data) {{$data->email}} @endforeach</a></p>
                                </div>
                            </div>
                            <div class="icon-box text-center">
                                <span class="icon-box-icon mb-4">
                                    <i class="p-icon-clock"></i>
                                </span>
                                <div class="icon-box-content">
                                    <h4 class="icon-box-title">Opening Hours</h4>
                                    <p class="text-dim">Mon-Fri: 10:00 - 18:00</p>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </section>

                    <section class="mt-10 pt-2 mb-10 pb-8">
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <figure>
                                    <img src="{{asset('frontend/images/subpage/contact/1.jpg')}}" width="600" height="557"
                                        alt="About Image" />
                                </figure>
                            </div>
                            <div class="col-md-6 pl-md-4 mt-8 mt-md-0">
                                <h2 class="title mb-1">Leave a Comment</h2>
                                <h3>Write us a message @auth @else<span style="font-size:12px;" class="text-danger">[You need to login first]</span>@endauth</h3>
                                <form method="post" action="{{route('contact.store')}}" id="contactForm">
                                	@csrf
                                    <div class="row">
                                        <div class="col-md-6 mb-4">
                                            <input type="text" id="name" name="name"
                                                placeholder="Your Name" required>
                                        </div>
                                         <div class="col-md-6 mb-4">
                                            <input type="text" id="subject" name="subject"
                                                placeholder="Your Subject" required>
                                        </div>
                                        <div class="col-md-6 mb-4">
                                            <input type="email" id="email" name="email"
                                                placeholder="Your Email" required>
                                        </div>
                                        <div class="col-md-6 mb-4">
                                            <input type="number" id="phone" name="phone"
                                                placeholder="Your Phone Number" required>
                                        </div>
                                       
                                        <div class="col-12 mb-4">
                                            <textarea id="message" name="message" placeholder="Your Message"
                                                required></textarea>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-dark">Send Message</button>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- Google Maps - Go to the bottom of the page to change settings and map location. -->
                <div class="bg-light google-map" id="googlemaps" style="height: 45rem;">
                	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14130.857353934944!2d85.36529494999999!3d27.6952226!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sne!2snp!4v1595323330171!5m2!1sne!2snp" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
                <!-- End Map Section -->
            </div>
        </main>
       
@endsection

