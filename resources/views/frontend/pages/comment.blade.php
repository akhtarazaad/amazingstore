@foreach($comments as $comment)
{{-- {{dd($comments)}} --}}
@php $dep = $depth-1; @endphp
<div class="display-comment"   @if($comment->parent_id != null) style="margin-left:40px;" @endif>
    <div class="comment-list">
        <div class="single-comment">
            @if($comment->user_info['photo'])
                <img src="{{asset($comment->user_info['photo'])}}" width="100" height="100" alt="#">
            @else 
                <img src="{{asset('backend/img/avatar.png')}}" alt="">
            @endif
            <div class="content">
                {{-- {{$post}} --}}
                 <div class="comment-user">
                    <h4><a href="#">{{$comment->user_info['name']}}</a></h4>
                    <span class="comment-date">{{$comment->created_at->format('g: i a')}} On {{$comment->created_at->format('M d Y')}}</span>
                </div>
                 <div class="comment-content mb-2">
                                                        <p>{{$comment->comment}} </p>
                                                    </div>
                @if($dep)
                <div class="button">
                    <a href="#" class="btn btn-reply reply" data-id="{{$comment->id}}"><i class="fa fa-reply" aria-hidden="true"></i>Reply</a>
                    <a href="" class="btn btn-reply cancel" style="display: none;" ><i class="fa fa-trash" aria-hidden="true"></i>Cancel</a>
                </div>
                @endif
            </div>
        </div>
    </div>
    @include('frontend.pages.comment', ['comments' => $comment->replies, 'depth' => $dep])

</div>    
@endforeach