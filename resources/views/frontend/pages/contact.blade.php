@extends('frontend.master')

@section('title','E-SHOP || Register Page')

@section('main-content')
@php
                            $settings=DB::table('settings')->get();
                        @endphp 

       <div class="main">
           
            <nav class="breadcrumb-nav has-border">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{route('home')}}">Home</a></li>
                        <li>Contact Us</li>
                    </ul>
                </div>
            </nav>
            <div class="page-content contact-page">
                <div class="container">
                    <section class="mt-10 pt-8">
                        <h2 class="title title-center mb-8">Contact Information</h2>
                        <div class="owl-carousel owl-theme row cols-lg-4 cols-md-3 cols-sm-2 cols-1 mb-10"
                            data-owl-options="{
                                'nav': false,
                                'dots': false,
                                'loop': false,
                                'margin': 20,
                                'autoplay': true,
                                'responsive': {
                                    '0': {
                                        'items': 1,
                                        'autoplay': true
                                    },
                                    '576': {
                                        'items': 2
                                    },
                                    '768': {
                                        'items': 3
                                    },
                                    '992': {
                                        'items': 4,
                                        'autoplay': false
                                    }
                                }
                            }">
                            <div class="icon-box text-center">
                                <span class="icon-box-icon mb-4">
                                    <i class="p-icon-map"></i>
                                </span>
                                <div class="icon-box-content">
                                    <h4 class="icon-box-title">Address</h4>
                                    <p class="text-dim">@foreach($settings as $data) {{$data->address}} @endforeach</p>
                                </div>
                            </div>
                            <div class="icon-box text-center">
                                <span class="icon-box-icon mb-4">
                                    <i class="p-icon-phone-solid"></i>
                                </span>
                                <div class="icon-box-content">
                                    <h4 class="icon-box-title">Phone Number</h4>
                                    <p class="text-dim">@foreach($settings as $data) {{$data->phone}} @endforeach</p>
                                </div>
                            </div>
                            <div class="icon-box text-center">
                                <span class="icon-box-icon mb-4">
                                    <i class="p-icon-message"></i>
                                </span>
                                <div class="icon-box-content">
                                    <h4 class="icon-box-title">E-mail Address</h4>
                                    <p class="text-dim"><a href="mailto:info@yourwebsite.com">@foreach($settings as $data) {{$data->email}} @endforeach</a></p>
                                </div>
                            </div>
                            <div class="icon-box text-center">
                                <span class="icon-box-icon mb-4">
                                    <i class="p-icon-clock"></i>
                                </span>
                                <div class="icon-box-content">
                                    <h4 class="icon-box-title">Opening Hours</h4>
                                    <p class="text-dim">Mon-Fri: 09:00AM - 09:00PM</p>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </section>

                    <section class="mt-10 pt-2 mb-10 pb-8">
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <figure>
                                    <img src="{{('frontend/images/logo.png')}}" width="300" height="557"
                                        alt="About Image" />
                                </figure>
                            </div>
                            <div class="col-md-6 pl-md-4 mt-8 mt-md-0">
                                <h2 class="title mb-1">Leave a Comment</h2>
                                 <h3>Write us a message @auth @else<span style="font-size:12px;" class="text-danger">[You need to login first]</span>@endauth</h3>
                                  <div style="margin: 10px; margin-left: -1px;">
                                  @include('frontend.flash-message')
                                 </div>
                               <form method="post" action="{{ route('contact.store') }}">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6 mb-4">
                                            <input type="text" class="form-control {{ $errors->has('name') ? 'error' : '' }}" name="name" id="name" placeholder="Enter Your Name">
                                        </div>
                                         <div class="col-md-6 mb-4">
                                              <input type="email" class="form-control {{ $errors->has('email') ? 'error' : '' }}" name="email" id="email" placeholder="Enter Your Email">
                                        </div>
                                        <div class="col-md-6 mb-4">
                                            <input type="text" class="form-control {{ $errors->has('phone') ? 'error' : '' }}" name="phone" id="phone" placeholder="Enter Your Phone Number">
                                        </div>
                                        <div class="col-md-6 mb-4">
                                             <input type="text" class="form-control {{ $errors->has('subject') ? 'error' : '' }}" name="subject" id="subject" placeholder="Subject">
                                        </div>
                                       
                                        <div class="col-12 mb-4">
                                             <textarea class="form-control {{ $errors->has('message') ? 'error' : '' }}" name="message" id="message"rows="4" placeholder="Message"></textarea>
                                        </div>
                                    </div>
                                    <input type="submit" name="send" value="Submit" class="btn btn-dark btn-block">
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- Google Maps - Go to the bottom of the page to change settings and map location. -->
              <div class="bg-light google-map" id="googlemaps" style="height: 45rem;">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3401.179299215167!2d74.31958651515059!3d31.519235081369988!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x391905def8cd0627%3A0xf4a0ea5cff02afa2!2sDiversity%20Tech!5e0!3m2!1sen!2s!4v1647447967883!5m2!1sen!2s" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
                <!-- End Map Section -->
            </div>
        </div>
        <!-- End Main -->
  @endsection