@extends('frontend.master')

@section('title','E-SHOP || PRODUCT PAGE')

@section('main-content')

   <main class="main">
             <div class="page-header img">
                <h1 class="page-title font-weight-light text-capitalize pt-2 text-center">Product Page</h1>
            </div><br>
            
            <div class="page-content mb-10 shop-page">
                <div class="container">
                    <div class="row main-content-wrap">
                        <aside
                            class="col-lg-3 sidebar widget-sidebar sidebar-fixed sidebar-toggle-remain shop-sidebar sticky-sidebar-wrapper">
                            <div class="sidebar-overlay"></div>
                            <a class="sidebar-close" href="#"><i class="p-icon-times"></i></a>
                            <div class="sidebar-content">
                                <div class="sticky-sidebar pt-7" data-sticky-options="{'top': 10}">
                                    <div class="widget ">
                                        <h3 class="widget-title title-underline"><span class="title-text">Filter by
                                                Price</span></h3>
                                        <div class="widget-body">
                                            <div class="product_filter">
                                                <button type="submit" class="filter_button">Filter</button>
                                                <div class="label-input">
                                                    <span>Range:</span>
                                                    <input style="" type="text" id="amount" readonly/>
                                                    <input type="hidden" name="price_range" id="price_range" value="@if(!empty($_GET['price'])){{$_GET['price']}}@endif"/>
                                                </div>
                                                </div><!-- End Filter Price Form -->
                                        </div>
                                    </div>
                                    <div class="widget ">
                                        <h3 class="widget-title title-underline"><span
                                                class="title-text">Categories</span></h3>
                                        <ul class="categor-list">
                                        @php
                                            // $category = new Category();
                                            $menu=App\Models\Category::getAllParentWithChild();
                                        @endphp
                                        @if($menu)
                                        <li>
                                            @foreach($menu as $cat_info)
                                                    @if($cat_info->child_cat->count()>0)
                                                        <li><a href="{{route('product-cat',$cat_info->slug)}}">{{$cat_info->title}}</a>
                                                            <ul>
                                                                @foreach($cat_info->child_cat as $sub_menu)
                                                                    <li><a href="{{route('product-sub-cat',[$cat_info->slug,$sub_menu->slug])}}">{{$sub_menu->title}}</a></li>
                                                                @endforeach
                                                            </ul>
                                                        </li>
                                                    @else
                                                        <li><a href="{{route('product-cat',$cat_info->slug)}}">{{$cat_info->title}}</a></li>
                                                    @endif
                                            @endforeach
                                        </li>
                                        @endif
                                        {{-- @foreach(Helper::productCategoryList('products') as $cat)
                                            @if($cat->is_parent==1)
                                                <li><a href="{{route('product-cat',$cat->slug)}}">{{$cat->title}}</a></li>
                                            @endif
                                        @endforeach --}}
                                    </ul>
                                    </div>
                                    <div class="widget widget-sidebar">
                                        <h3 class="widget-title title-underline"><span class="title-text">Recent
                                                Posts</span></h3>
                                                 {{-- {{dd($recent_products)}} --}}
                                                 @foreach($recent_products as $product)
                                        <div class="widget-body pt-6 pb-4">
                                              @php 
                                            $photo=explode(',',$product->photo);
                                        @endphp
                                                <div class="post-col">
                                                    <div class="post post-list-sm">
                                                        <figure class="post-media">
                                                            <a href="blog-single.html">
                                                                <img src="{{$photo[0]}}" width="85"
                                                                    height="85" alt="{{$photo[0]}}" />
                                                            </a>
                                                        </figure>
                                                        <div class="post-details">
                                                            <h5 class="post-title"><a href="{{route('product-detail',$product->slug)}}">{{$product->title}}</a>
                                                            </h5>
                                                            @php
                                                    $org=($product->price-($product->price*$product->discount)/100);
                                                    @endphp
                                                            <div class="post-meta">
                                                                 <p class="price"><del class="text-muted">${{number_format($product->price,2)}}</del>   ${{number_format($org,2)}}  </p>
                                                            </div>
                                                             <li><i class="fa fa-user" aria-hidden="true"></i> 
                                               
                                            </li>

                                                            
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                
                                           
                                        </div>
                                         @endforeach
                                    </div>

                                     <div class="widget ">
                                        <h3 class="widget-title title-underline"><span class="title-text">Brands</span>
                                        </h3>
                                       <ul class="categor-list">
                                        @php
                                            $brands=DB::table('brands')->orderBy('title','ASC')->where('status','active')->get();
                                        @endphp
                                        @foreach($brands as $brand)
                                            <li><a href="{{route('product-brand',$brand->slug)}}">{{$brand->title}}</a></li>
                                        @endforeach
                                    </ul>
                                    </div>
                                </div>
                            </div>
                        </aside>

                        <div class="col-lg-4 main-content pl-lg-3">
                             <div class="shop-top">
                                <div class="row">
                                    <div class="shop-shorter">
                                        <div class="single-shorter">
                                            <label>Show :</label>
                                            <select class="show" name="show" onchange="this.form.submit();">
                                                <option value="">Default</option>
                                                <option value="9" @if(!empty($_GET['show']) && $_GET['show']=='9') selected @endif>09</option>
                                                <option value="15" @if(!empty($_GET['show']) && $_GET['show']=='15') selected @endif>15</option>
                                                <option value="21" @if(!empty($_GET['show']) && $_GET['show']=='21') selected @endif>21</option>
                                                <option value="30" @if(!empty($_GET['show']) && $_GET['show']=='30') selected @endif>30</option>
                                            </select>
                                        </div>
                                        <div class="single-shorter">
                                            <label>Sort By :</label>
                                            <select class='sortBy' name='sortBy' onchange="this.form.submit();">
                                                <option value="">Default</option>
                                                <option value="title" @if(!empty($_GET['sortBy']) && $_GET['sortBy']=='title') selected @endif>Name</option>
                                                <option value="price" @if(!empty($_GET['sortBy']) && $_GET['sortBy']=='price') selected @endif>Price</option>
                                                <option value="category" @if(!empty($_GET['sortBy']) && $_GET['sortBy']=='category') selected @endif>Category</option>
                                                <option value="brand" @if(!empty($_GET['sortBy']) && $_GET['sortBy']=='brand') selected @endif>Brand</option>
                                            </select>
                                        </div>
                                    </div>
                                    </div>
                                    <ul class="view-mode">
                                        <li class="active"><a href="javascript:void(0)"><i class="fa fa-th-large"></i></a></li>
                                        <li><a href="{{route('product-lists')}}"><i class="fa fa-th-list"></i></a></li>
                                    </ul>
                                </div>
                           
                </div>
                
            </div>
            <!-- End .page-content-->
        </main>

@endsection
@push('styles')
<style>
    .pagination{
        display:inline-flex;
    }
    .filter_button{
        /* height:20px; */
        text-align: center;
        background:#F7941D;
        padding:8px 16px;
        margin-top:10px;
        color: white;
    }
</style>
@endpush
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    {{-- <script>
        $('.cart').click(function(){
            var quantity=1;
            var pro_id=$(this).data('id');
            $.ajax({
                url:"{{route('add-to-cart')}}",
                type:"POST",
                data:{
                    _token:"{{csrf_token()}}",
                    quantity:quantity,
                    pro_id:pro_id
                },
                success:function(response){
                    console.log(response);
                    if(typeof(response)!='object'){
                        response=$.parseJSON(response);
                    }
                    if(response.status){
                        swal('success',response.msg,'success').then(function(){
                            document.location.href=document.location.href;
                        });
                    }
                    else{
                        swal('error',response.msg,'error').then(function(){
                            // document.location.href=document.location.href;
                        });
                    }
                }
            })
        });
    </script> --}}
    <script>
        $(document).ready(function(){
        /*----------------------------------------------------*/
        /*  Jquery Ui slider js
        /*----------------------------------------------------*/
        if ($("#slider-range").length > 0) {
            const max_value = parseInt( $("#slider-range").data('max') ) || 500;
            const min_value = parseInt($("#slider-range").data('min')) || 0;
            const currency = $("#slider-range").data('currency') || '';
            let price_range = min_value+'-'+max_value;
            if($("#price_range").length > 0 && $("#price_range").val()){
                price_range = $("#price_range").val().trim();
            }
            
            let price = price_range.split('-');
            $("#slider-range").slider({
                range: true,
                min: min_value,
                max: max_value,
                values: price,
                slide: function (event, ui) {
                    $("#amount").val(currency + ui.values[0] + " -  "+currency+ ui.values[1]);
                    $("#price_range").val(ui.values[0] + "-" + ui.values[1]);
                }
            });
            }
        if ($("#amount").length > 0) {
            const m_currency = $("#slider-range").data('currency') || '';
            $("#amount").val(m_currency + $("#slider-range").slider("values", 0) +
                "  -  "+m_currency + $("#slider-range").slider("values", 1));
            }
        })
    </script>
@endpush