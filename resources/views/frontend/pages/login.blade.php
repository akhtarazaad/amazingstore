@extends('frontend.master')

@section('title','E-SHOP || Register Page')

@section('main-content')
        <!-- End Header -->
        <div class="main">
            {{-- <div class="page-header" style="background-color: #fff7ec">
                <h1 class="page-title">My Account</h1>
            </div> --}}
            <nav class="breadcrumb-nav has-border">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{route('home')}}">Home</a></li>
                        <li>My account</li>
                    </ul>
                </div>
            </nav>
            <div class="page-content">
                <div class="container pt-8 pb-10">
                    <div class="login-popup mx-auto pl-6 pr-6 pb-9">
                        <div class="form-box">
                            <div class="tab tab-nav-underline tab-nav-boxed">
                                <ul class="nav nav-tabs nav-fill align-items-center justify-content-center mb-4">
                                    <li class="nav-item">
                                        <a class="nav-link active lh-1 ls-normal" href="#signin-1">Login</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#register-1" class="nav-link lh-1 ls-normal">Register</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="signin-1">
                                         <form method="post" action="{{route('login.submit')}}">
                                        @csrf
                                            <div class="form-group">
                                                <input type="email" id="singin-email-1" name="email"
                                                    placeholder="Username or Email Address" required="required">
                                                    @error('email')
                                                    <span class="text-danger">{{$message}}</span>
                                                    @enderror

                                                <input type="password" name="password"
                                                    placeholder="Password" required="required">
                                                     @error('password')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="form-footer">
                                                <div class="form-checkbox">
                                                    <input name="news" type="checkbox" id="signin-remember-1"
                                                        name="signin-remember-1">
                                                    <label for="signin-remember-1">Remember
                                                        me</label>
                                                </div>
                                                <a href="{{route('register.form')}}" class="lost-link d-block ">Lost your password?</a>
                                            </div>
                                            <button class="btn btn-dark btn-block" type="submit">Login</button>
                                        </form>
                                    </div>
                                    <div class="tab-pane" id="register-1">
                                         <form  method="post" action="{{route('register.submit')}}">
                                            @csrf
                                            <div class="form-group">
                                                <input type="text"  name="name"
                                                    placeholder="Username" required="required">
                                                    @error('name')
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror

                                                <input type="text"  name="email"
                                                    placeholder="Your Email Address" required="required">
                                                    @error('email')
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror
                                          
                                        <input type="text"  name="phone"
                                                    placeholder="Your phone no" required="required">
                                                    @error('phone')
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror

                                        <input type="text"  name="address"
                                                    placeholder="Your address" required="required">
                                                    @error('address')
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror

                                                <input type="password" 
                                                    name="password" placeholder="Password" required="required">
                                                    @error('password')
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror

                                             <input type="password" 
                                            name="password_confirmation" placeholder="Confirm Password" required="required">
                                            @error('password_confirmation')
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror

                                            </div>
                                            <div class="form-footer mb-5">
                                               
                                            </div>
                                            <button class="btn btn-dark btn-block" type="submit">Register</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Main -->
        @endsection