@extends('frontend.master')

@section('title','Amazing Store || Blog Page')

@section('main-content')
  <main class="main">
            <div class="page-header" style="background-color: #f9f8f4; padding: 50px;">
                <h1 class="page-title font-weight-light text-capitalize pt-2 text-center">Blog Page</h1>
            </div><br>

            <div class="page-content">
                <div class="container mb-10 pb-6">
                    <div class="row">

                         <div class="col-lg-9 pr-lg-6">
                            <div class="row posts cols-md-2">
                                @foreach($posts as $post)
                               {{-- {{$post}} --}}
                                <article class="post post-border post-classic post-classic-sm overlay-zoom mb-8">
                                    
                                    <figure class="post-media">
                                        <a href="{{route('blog.detail',$post->slug)}}">
                                            <img src="{{$post->photo}}" width="905" height="500" alt="{{$post->photo}}">
                                        </a>
                                    </figure>
                                    @php 
                                    $author_info=DB::table('users')->select('name')->where('id',$post->added_by)->get();
                                    @endphp
                                    <div class="post-details text-center">
                                        <div class="post-calendar">
                                            {{$post->created_at->format('d M, Y. D')}}
                                        </div>
                                        <h4 class="post-title"><a href="{{route('blog.detail',$post->slug)}}">{{$post->title}}</a></h4>
                                        <p class="post-content">{!! html_entity_decode($post->summary) !!}
                                        </p>
                                        <a href="{{route('blog.detail',$post->slug)}}" class="btn btn-outline btn-dark">Read
                                            more
                                        </a>
                                        <span class="float-right">
                                                <i class="fa fa-user" aria-hidden="true"></i> 
                                                @foreach($author_info as $data)
                                                    @if($data->name)
                                                        {{$data->name}}
                                                    @else
                                                        Anonymous
                                                    @endif
                                                @endforeach
                                            </span>
                                    </div>

                                </article>
                                  @endforeach
                                  <div class="col-12">
                            <!-- Pagination -->
                            {{-- {{$posts->appends($_GET)->links()}} --}}
                            <!--/ End Pagination -->
                        </div>
                            </div>
                        </div>
                        <aside class="col-lg-3 right-sidebar sidebar-fixed sticky-sidebar-wrapper">
                            <div class="sidebar-overlay">
                            </div>
                            <a class="sidebar-close" href="#"><i class="p-icon-times"></i></a>
                            <a href="#" class="sidebar-toggle"><i class="fas fa-chevron-left"></i></a>
                            <div class="sidebar-content">
                                <div class="sticky-sidebar"
                                    data-sticky-options="{'paddingOffsetTop': 89, 'paddingOffsetBottom': 20}">
                                    <div class="widget widget-search border-no mb-9">
                                        <form method="GET" action="{{route('blog.search')}}" class="form-simple">
                                            <input type="text" name="search" autocomplete="off"
                                                placeholder="Enter your keywords..." required />
                                            <button class="btn btn-search btn-link" type="submit">
                                                <i class="p-icon-search-solid"></i>
                                            </button>
                                        </form>
                                    </div>
                                    <div class="widget widget-sidebar widget-collapsible border-no">
                                        <h3 class="widget-title title-underline"><span
                                                class="title-text">Categories</span>
                                        </h3>
                                        <ul class="widget-body filter-items search-ul">
                                    @if(!empty($_GET['category']))
                                    @php 
                                    $filter_cats=explode(',',$_GET['category']);
                                    @endphp
                                    @endif
                                    <form action="{{route('blog.filter')}}" method="POST">
                                    @csrf
                                    {{-- {{count(Helper::postCategoryList())}} --}}
                                    @foreach(Helper::postCategoryList('posts') as $cat)
                                    <li>
                                        <a href="{{route('blog.category',$cat->slug)}}">{{$cat->title}} </a>
                                    </li>
                                    @endforeach
                                </form>
                                        </ul>
                                    </div>
                                   
                                    <div class="widget widget-posts widget-sidebar widget-collapsible">
                                        <h3 class="widget-title title-underline"><span class="title-text">Browse
                                                Tags</span></h3>
                                        <div class="widget-body widget-tags">
                                          
                                @if(!empty($_GET['tag']))
                                    @php 
                                        $filter_tags=explode(',',$_GET['tag']);
                                    @endphp
                                @endif
                                <form action="{{route('blog.filter')}}" method="POST">
                                    @csrf
                                    @foreach(Helper::postTagList('posts') as $tag)
                                        <li>
                                            <li>
                                                <a href="{{route('blog.tag',$tag->title)}}">{{$tag->title}} </a>
                                            </li>
                                        </li>
                                    @endforeach
                                </form>
                           
                                            
                                        </div>
                                    </div>
                                    <div class="widget widget-sidebar">
                                        <h3 class="widget-title title-underline"><span class="title-text">Recent
                                                Posts</span></h3>
                                                 @foreach($recent_posts as $post)
                                        <div class="widget-body pt-6 pb-4">
                                             @php 
                                            $author_info=DB::table('users')->select('name')->where('id',$post->added_by)->get();
                                        @endphp
                                                <div class="post-col">
                                                    <div class="post post-list-sm">
                                                        <figure class="post-media">
                                                            <a href="blog-single.html">
                                                                <img src="{{$post->photo}}" width="85"
                                                                    height="85" alt="{{$post->photo}}" />
                                                            </a>
                                                        </figure>
                                                        <div class="post-details">
                                                            <h5 class="post-title"><a href="blog-single.html">{{$post->title}}</a>
                                                            </h5>
                                                            <div class="post-meta">
                                                                <a href="blog-single.html" class="post-date">{{$post->created_at->format('d M, y')}}</a>
                                                            </div>
                                                             <li><i class="fa fa-user" aria-hidden="true"></i> 
                                                @foreach($author_info as $data)
                                                    @if($data->name)
                                                        {{$data->name}}
                                                    @else
                                                        Anonymous
                                                    @endif
                                                @endforeach
                                            </li>

                                                            
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                
                                           
                                        </div>
                                         @endforeach
                                    </div>
                                    
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </main>
@endsection
@push('styles')
    <style>
        .pagination{
            display:inline-flex;
        }
    </style>

@endpush