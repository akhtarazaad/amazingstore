@extends('frontend.master')

@section('title','E-SHOP || Register Page')

@section('main-content')
        <!-- End Header -->

        <div class="main">
            
            {{-- <div class="page-header cph-header pl-4 pr-4" style="background-color: #fff7ec">
                <h1 class="page-title font-weight-light text-capitalize">Products</h1>
                <div class="category-container row justify-content-center cols-2 cols-xs-3 cols-sm-4 cols-md-6 pt-6">
                    @if(count($products)>0)
                    
                   @foreach($products->take(5) as $product)
                   
                    <div class="category category-ellipse mb-4 mb-md-0">
                         @php 
                         
                        $photo=explode(',',$product->photo);
                        
                         @endphp
                        <a href="Javascript://">
                            <figure >
                                <img src=" {{asset($photo[0])}}" alt="category">
                            </figure>
                        </a>
                        <div class="category-content">
                            <h3 class="category-name"><a href="Javascript://"> {{$product->title}}</a>
                            </h3>
                        </div>
                       
                    </div>
                    <!-- <div class="category category-ellipse mb-4 mb-md-0">
                        <a href="#">
                            <figure>
                                <img src="{{asset('frontend/images/shop/cat-2.jpg')}}" alt="category" width="160" height="161">
                            </figure>
                        </a>
                        <div class="category-content">
                            <h3 class="category-name"><a href="#">Fruits</a>
                            </h3>
                        </div>
                    </div>
                    <div class="category category-ellipse mb-4 mb-md-0">
                        <a href="#">
                            <figure>
                                <img src="{{asset('frontend/images/shop/cat-3.jpg')}}" alt="category" width="160" height="161">
                            </figure>
                        </a>
                        <div class="category-content">
                            <h3 class="category-name"><a href="#">Vegetables</a>
                            </h3>
                        </div>
                    </div>
                    <div class="category category-ellipse">
                        <a href="#">
                            <figure>
                                <img src="{{asset('frontend/images/shop/cat-4.jpg')}}" alt="category" width="160" height="161">
                            </figure>
                        </a>
                        <div class="category-content">
                            <h3 class="category-name"><a href="#">Meats</a>
                            </h3>
                        </div>
                    </div>
                    <div class="category category-ellipse">
                        <a href="#">
                            <figure>
                                <img src="{{asset('frontend/images/shop/cat-5.jpg')}}" alt="category" width="160" height="161">
                            </figure>
                        </a>
                        <div class="category-content">
                            <h3 class="category-name"><a href="#">Coffee</a>
                            </h3>
                        </div>
                    </div>
                    <div class="category category-ellipse">
                        <a href="#">
                            <figure>
                                <img src="{{asset('frontend/images/shop/cat-6.jpg')}}" alt="category" width="160" height="161">
                            </figure>
                        </a>
                        <div class="category-content">
                            <h3 class="category-name"><a href="#">Snack</a>
                            </h3>
                        </div>
                    </div> -->
                     @endforeach
                    @endif
                </div>
            </div> --}}
            {{-- <nav class="breadcrumb-nav has-border">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{route('home')}}">Home</a></li>
                        <li>Products</li>
                    </ul>
                </div>
            </nav> --}}
           
            <div class="page-content mb-10 shop-page">
                <div class="container">
                    <div class="row main-content-wrap">
                       
                        <aside
                            class="col-lg-3 sidebar widget-sidebar sidebar-fixed sidebar-toggle-remain shop-sidebar sticky-sidebar-wrapper">
                            <div class="sidebar-overlay"></div>
                            <a class="sidebar-close" href="#"><i class="p-icon-times"></i></a>
                            <div class="sidebar-content">
                                <div class="sticky-sidebar pt-7" data-sticky-options="{'top': 10}">
                                  <!--   <div class="widget widget-collapsible">
                                        <h3 class="widget-title title-underline"><span class="title-text">Filter by
                                                Price</span></h3>
                                        <div class="widget-body">
                                            <form action="#" class="pt-2">
                                                <div class="filter-price-slider"></div>

                                                <div class="filter-actions">
                                                    <button type="submit" class="btn btn-dim btn-filter">Filter</button>
                                                </div>
                                            </form> 
                                        </div>
                                    </div> -->
                                    <div class="widget widget-collapsible" style="  background-color: rgb(41, 52, 98);
                                    color: whitesmoke;">
                                        <h3 class="widget-title"><span
                                                class="title-text" style="color: whitesmoke; padding-left: 10px;">Categories</span></h3>
                                       <ul class="categor-list">
                                        @php
                                            // $category = new Category();
                                            $menu=App\Models\Category::getAllParentWithChild();
                                        @endphp
                                        @if($menu)
                                        
                                            @foreach($menu as $cat_info)
                                           

                                                    @if($cat_info->child_cat->count()>0)
                                                    
                                                        <li><a href="{{route('product-cat',$cat_info->slug)}}">{{$cat_info->title}} </a>
                                                            
                                                            <ul> 
                                                                
                                                                @foreach($cat_info->child_cat as $sub_menu)
                                                                    <li><a href="{{route('product-sub-cat',[$cat_info->slug,$sub_menu->slug])}}">{{$sub_menu->title}}
                                                                        

                                                                    </a></li>
                                                                    
                                                                   
                                                                @endforeach
                                                            </ul>
                                                        </li>
                                                    @else
                                                        <li><a href="{{route('product-cat',$cat_info->slug)}}">{{$cat_info->title}} </a></li>
                                                    @endif
                                            @endforeach
                                       
                                        @endif
                                        {{-- @foreach(Helper::productCategoryList('products') as $cat)
                                            @if($cat->is_parent==1)
                                                <li><a href="{{route('product-cat',$cat->slug)}}">{{$cat->title}}</a></li>
                                            @endif
                                        @endforeach --}}
                                    </ul>
                                    </div>
                                    
                                   
                                    <div class="widget widget-collapsible" style="background-color: rgb(41, 52, 98);
                                    color: whitesmoke;
                                    ">
                                        <h3 class="widget-title"><span class="title-text" style="color: whitesmoke; padding-left: 10px;">Brand</span>
                                        </h3>
                                       <ul class="categor-list">
                                        @php
                                            $brands=DB::table('brands')->orderBy('title','ASC')->where('status','active')->get();
                                        @endphp
                                        @foreach($brands as $brand)
                                            <li><a href="{{route('product-brand',$brand->slug)}}">{{$brand->title}}</a></li>
                                        @endforeach
                                    </ul>
                                    </div>
                                    
                                   
                                  <!--   <div class="widget widget-collapsible">
                                        <h3 class="widget-title title-underline"><span class="title-text">Product
                                                Tags</span></h3>
                                        <div class="widget-body mt-1">
                                            <a href="#" class="tag">
                                                Organic
                                            </a>
                                            <a href="#" class="tag">
                                                greenhouse
                                            </a>
                                            <a href="#" class="tag">
                                                fat
                                            </a>
                                            <a href="#" class="tag">
                                                healthy
                                            </a>
                                            <a href="#" class="tag">
                                                dairy
                                            </a>
                                            <a href="#" class="tag">
                                                vitamin
                                            </a>
                                            <a href="#" class="tag">
                                                diet
                                            </a>
                                            <a href="#" class="tag">
                                                nutrition
                                            </a>
                                            <a href="#" class="tag">
                                                cholesterol
                                            </a>
                                        </div>
                                    </div> -->
                                      {{-- <div class="widget widget-collapsible">
                                        <h3 class="widget-title title-underline"><span class="title-text">Recent Post</span></h3>
                                     @if($products)
                                     @foreach($products as $key=>$product)
                                        <div class="widget-body pt-6 pb-4">
                                             @php 
                                            $photo=explode(',',$product->photo);
                                            // dd($photo);
                                            @endphp
                                            
                                                <div class="post-col">
                                                    <div class="post post-list-sm">
                                                        @foreach($photo as $data)
                                                        <figure class="post-media">
                                                            <a href="blog-single.html">
                                                                <img src="{{asset($data)}}" width="85"
                                                                    height="85" alt="{{asset($data)}}" />
                                                            </a>
                                                        </figure>
                                                        @endforeach
                                                        <div class="post-details">
                                                            <h5 class="post-title"><a href="blog-single.html">{{$product->title}}</a>
                                                            </h5>
                                                             @php
                                                $after_discount=($product->price-($product->price*$product->discount)/100);
                                            @endphp
                                            <h3><small><del class="text-muted">Rs{{number_format($product->price,2)}}</del></small>    Rs{{number_format($after_discount,2)}}  </h3>
                                                            
                                               
                                            </li>

                                                            
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                
                                           
                                        </div>
                                         @endforeach
                                         @endif
                                    </div>
                                </div>
                            </div> --}}
                        </aside>
                        <div class="col-lg-9 main-content pl-lg-6">
                            <nav class="toolbox sticky-toolbox sticky-content fix-top">
                                <div class="toolbox-left">
                                    <a href="#"
                                        class="toolbox-item left-sidebar-toggle btn btn-outline btn-primary btn-icon-right d-lg-none"><span>Filter</span><i
                                            class="p-icon-category-1 ml-md-1"></i></a>
                                    <div class="toolbox-item toolbox-sort select-menu">
                                        <label>Sort By :</label>
                                       <select class='sortBy' name='sortBy' onchange="this.form.submit();">
                                                <option value="">Default</option>
                                                <option value="title" @if(!empty($_GET['sortBy']) && $_GET['sortBy']=='title') selected @endif>Name</option>
                                                <option value="price" @if(!empty($_GET['sortBy']) && $_GET['sortBy']=='price') selected @endif>Price</option>
                                                <option value="category" @if(!empty($_GET['sortBy']) && $_GET['sortBy']=='category') selected @endif>Category</option>
                                                <option value="brand" @if(!empty($_GET['sortBy']) && $_GET['sortBy']=='brand') selected @endif>Brand</option>
                                            </select>
                                    </div>
                                </div>
                                <div class="toolbox-right">
                                    <div class="toolbox-item toolbox-show select-box">
                                        <label>Show :</label>
                                      <select class="show" name="show" onchange="this.form.submit();">
                                                <option value="">Default</option>
                                                <option value="9" @if(!empty($_GET['show']) && $_GET['show']=='9') selected @endif>09</option>
                                                <option value="15" @if(!empty($_GET['show']) && $_GET['show']=='15') selected @endif>15</option>
                                                <option value="21" @if(!empty($_GET['show']) && $_GET['show']=='21') selected @endif>21</option>
                                                <option value="30" @if(!empty($_GET['show']) && $_GET['show']=='30') selected @endif>30</option>
                                            </select>
                                    </div>
                                    <div class="toolbox-item toolbox-layout">
                                        <a href="Javascript://" class="p-icon-list btn-layout"></a>
                                        <a href="{{route('product-lists')}}" class="p-icon-grid btn-layout active"></a>
                                    </div>
                                </div>
                            </nav>
                            <div class="row product-wrapper cols-md-3 cols-2">
                                {{-- {{$products}} --}}
                            @if(count($products)>0)
                            @foreach($products as $product)
                            
                                <div class="product-wrap">
                           
                                    <div class="product shadow-media text-center bordr">
                                        <figure class="product-media">
                                            <a href="{{route('product-detail',$product->slug)}}">
                                                 @php 
                                                $photo=explode(',',$product->photo);
                                                 @endphp
                                                <img style="padding:60px 20px; background-color: #f8f8f8; margin-bottom: 14px;" src="{{asset($photo[0])}}" alt="{{asset($photo[0])}}" width="295"
                                                    height="369" />
                                                <img class="hover-img" src="{{asset($photo[0])}}" alt="{{asset($photo[0])}}" width="295"
                                                    height="369" />
                                            </a>
                                            @if($product->discount)
                                                    <span class="price-dec">{{$product->discount}} % Off</span>
                                                    @endif

                                            {{-- <div class="product-action-vertical">
                                                
                                                <a title="Wishlist" href="{{route('add-to-wishlist',$product->slug)}}" class="wishlist" data-id="{{$product->id}}"><i class=" p-icon-heart-solid "></i></a>

                                                <div class="product-action-2">
                                                        <a title="Add to cart" href="{{route('add-to-cart',$product->slug)}}" style="margin-right: 18px;">
                                                           <i class=" p-icon-cart-solid "></i>
                                                        </a>
                                                    </div>
                                               
                                            </div> --}}
                                        </figure>
                                        <div class="product-details">

                                            {{-- <div class="ratings-container">

                                    @php
                                    $rate=ceil($product->getReview->avg('rate'))
                                    @endphp
                                    @for($i=1; $i<=5; $i++)
                                    @if($rate>=$i)
                                    <i class="fa fa-star"></i>
                                    @else 
                                        <i class="fa fa-star-o"></i>
                                    @endif
                                    @endfor
                                   <a href="#" class="total-review">({{$product['getReview']->count()}}) Review</a>
                                </div> --}}
                                            <h5 class="product-name">
                                                <a href=" {{route('product-detail',$product->slug)}}">
                                                  {{$product->title}}
                                                </a>
                                            </h5>
                                             @php
                                             $after_discount=($product->price-($product->price*$product->discount)/100);
                                             @endphp
                                            <span class="product-price">
                                                <del class="old-price">Rs{{number_format($product->price,2)}}</del>
                                                <ins class="new-price">Rs{{number_format($after_discount,2)}}</ins>
                                            </span>
                                            
                                        </div>
                                        <span class="product-price buy">
                                            <a href="{{route('product-detail',$product->slug)}}">Buy Now</a>
                                         </span>
                                    </div>
                                


                                    <!-- End .product -->
                                </div>
                                @endforeach
                            @else
                                    <h4 class="text-warning" style="margin:100px auto;">There are no products.</h4>
                            @endif
                            
                              
                              
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- End .page-content-->
        </main>
        <!-- End Main -->
       
    <!-- Modal end -->
    @endsection
    <!-- Plugins JS File -->
    <script src="{{asset('frontend/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/sticky/sticky.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/imagesloaded/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/elevatezoom/jquery.elevatezoom.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/owl-carousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/nouislider/nouislider.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/wnumb/wNumb.min.js')}}"></script>
    <!-- Main JS File -->
    <script src="{{asset('frontend/js/main.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    {{-- <script>
        $('.cart').click(function(){
            var quantity=1;
            var pro_id=$(this).data('id');
            $.ajax({
                url:"{{route('add-to-cart')}}",
                type:"POST",
                data:{
                    _token:"{{csrf_token()}}",
                    quantity:quantity,
                    pro_id:pro_id
                },
                success:function(response){
                    console.log(response);
                    if(typeof(response)!='object'){
                        response=$.parseJSON(response);
                    }
                    if(response.status){
                        swal('success',response.msg,'success').then(function(){
                            document.location.href=document.location.href;
                        });
                    }
                    else{
                        swal('error',response.msg,'error').then(function(){
                            // document.location.href=document.location.href;
                        });
                    }
                }
            })
        });
    </script> --}}
    <script>
        $(document).ready(function(){
        /*----------------------------------------------------*/
        /*  Jquery Ui slider js
        /*----------------------------------------------------*/
        if ($("#slider-range").length > 0) {
            const max_value = parseInt( $("#slider-range").data('max') ) || 500;
            const min_value = parseInt($("#slider-range").data('min')) || 0;
            const currency = $("#slider-range").data('currency') || '';
            let price_range = min_value+'-'+max_value;
            if($("#price_range").length > 0 && $("#price_range").val()){
                price_range = $("#price_range").val().trim();
            }
            
            let price = price_range.split('-');
            $("#slider-range").slider({
                range: true,
                min: min_value,
                max: max_value,
                values: price,
                slide: function (event, ui) {
                    $("#amount").val(currency + ui.values[0] + " -  "+currency+ ui.values[1]);
                    $("#price_range").val(ui.values[0] + "-" + ui.values[1]);
                }
            });
            }
        if ($("#amount").length > 0) {
            const m_currency = $("#slider-range").data('currency') || '';
            $("#amount").val(m_currency + $("#slider-range").slider("values", 0) +
                "  -  "+m_currency + $("#slider-range").slider("values", 1));
            }
        })
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
   
    <script>
        $(document).ready(function(){
        /*----------------------------------------------------*/
        /*  Jquery Ui slider js
        /*----------------------------------------------------*/
        if ($("#slider-range").length > 0) {
            const max_value = parseInt( $("#slider-range").data('max') ) || 500;
            const min_value = parseInt($("#slider-range").data('min')) || 0;
            const currency = $("#slider-range").data('currency') || '';
            let price_range = min_value+'-'+max_value;
            if($("#price_range").length > 0 && $("#price_range").val()){
                price_range = $("#price_range").val().trim();
            }
            
            let price = price_range.split('-');
            $("#slider-range").slider({
                range: true,
                min: min_value,
                max: max_value,
                values: price,
                slide: function (event, ui) {
                    $("#amount").val(currency + ui.values[0] + " -  "+currency+ ui.values[1]);
                    $("#price_range").val(ui.values[0] + "-" + ui.values[1]);
                }
            });
            }
        if ($("#amount").length > 0) {
            const m_currency = $("#slider-range").data('currency') || '';
            $("#amount").val(m_currency + $("#slider-range").slider("values", 0) +
                "  -  "+m_currency + $("#slider-range").slider("values", 1));
            }
        })
    </script>

