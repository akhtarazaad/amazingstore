<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from d-themes.com/html/panda/demo1.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 19 Feb 2022 15:08:47 GMT -->
<head>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

    <title>Amazing Store</title>

    <meta name="keywords" content="" />
    <meta name="description" content="Amazing - Store">
    <meta name="author" content="Amazing Store">

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="{{asset('frontend/images/icons/dd.svg')}}">
        
    <!-- Preload Font -->

    <link rel="preload" href="{{asset('frontend/vendor/fontawesome-free/webfonts/fa-solid-900.woff2')}}" as="font" type="font/woff2"
        crossorigin="anonymous">
    <link rel="preload" href="{{asset('frontend/vendor/fontawesome-free/webfonts/fa-brands-400.woff2')}}" as="font" type="font/woff2"
        crossorigin="anonymous">

    <script>
        WebFontConfig = {
            google: { families: [ 'Josefin Sans:300,400,600,700' ] }
        };
        ( function ( d ) {
            var wf = d.createElement( 'script' ), s = d.scripts[ 0 ];
            wf.src = '{{asset('frontend/js/webfont.js')}}';
            wf.async = true;
            s.parentNode.insertBefore( wf, s );
        } )( document );
    </script>


     <link rel="stylesheet" type="text/css" href="{{asset('frontend/vendor/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/vendor/animate/animate.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/vendor/owl-carousel/owl.carousel.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/vendor/magnific-popup/magnific-popup.min.css')}}">

    <!-- Main CSS File -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/style.min.css')}}">
</head>
@php
$settings=DB::table('settings')->get();

@endphp
<body class="home">
    <div class="page-wrapper">
        <header class="header">
            <div class="header-top">
                <div class="container">
                    <div class="header-left">
                        <a href="tel:#" class="call">
                            <i class="fa fa-phone"></i>
                            <span>@foreach($settings as $data) {{$data->phone}} @endforeach</span>
                        </a>
                        <span class="divider"></span>
                        <a href="/contact" class="contact">
                            <i class="fa fa-envelope"></i>
                            <span style="text-transform: lowercase; !important "> @foreach($settings as $data) {{$data->email}} @endforeach</span>
                        </a>
                    </div>
                    <div class="header-right">
                        
                            
                            @auth 

                                @if(Auth::user()->role=='user')
                                <i class="fa fa-map-marker"><a href="{{route('order.track')}}"> Track Order</a></i>
                                <i class="fa fa-user"> <a href="{{route('user')}}"  target="_blank"> Dashboard</a></i>
                                <i class="fa fa-power-off"> <a href="{{route('user.logout')}}"> Logout</a></i>
                                @else
                                 <i class="fa fa-power-off"><a href="{{route('login.form')}}"> Login /Register</a></i>
                                @endif


                            @else
                                <i class="fa fa-power-off"><a href="{{route('login.form')}}"> Login /Register</a></i>
                            @endauth
                            
                      
                        <span class="divider"></span>
                        <!-- End DropDown Menu -->
                        <div class="social-links">
                            <a href="#" class="social-link fab fa-facebook-f" title="Facebook"></a>
                            <a href="#" class="social-link fab fa-twitter" title="Twitter"></a>
                            <a href="#" class="social-link fab fa-pinterest" title="Pinterest"></a>
                            <a href="#" class="social-link fab fa-linkedin-in" title="Linkedin"></a>
                        </div>
                        <!-- End of Social Links -->
                    </div>
                </div>
            </div>
            <!-- End HeaderTop -->
            <div class="header-middle has-center sticky-header fix-top sticky-content">
                <div class="container">
                    <div class="header-left">
                        <a href="#" class="mobile-menu-toggle" title="Mobile Menu">
                            <i class="p-icon-bars-solid"></i>
                        </a>
                        @php
                            $settings=DB::table('settings')->get();
                        @endphp  
                        <a href="{{route('home')}}" class="logo">
                             <img src="@foreach($settings as $data) {{asset($data->logo)}} @endforeach" alt="logo" width="200">
                        </a>
                        <!-- End of Divider -->
                    </div>
                    <div class="header-center">
                        <nav class="main-nav">
                            <ul class="menu">
                                <li class="{{Request::path()=='home' ? 'active' : ''}}">
                                    <a href="{{route('home')}}">Home</a>
                                </li>
                                
                                <li class="@if(Request::path()=='product-grids'||Request::path()=='product-lists')  active  @endif">
                                    <a href="{{route('product-grids')}}">Products</a>
                                   
                                </li>
                                {{Helper::getHeaderCategory()}}
                                <li class="{{Request::path()=='blog' ? 'active' : ''}}">
                                    <a href="{{route('blog')}}">Blog</a>
                                    
                                </li>
                                <li class="{{Request::path()=='about-us' ? 'active' : ''}}">
                                    <a href="{{route('about-us')}}">About Us</a>
                                    
                                </li>

                                <li class="{{Request::path()=='contact' ? 'active' : ''}}">
                                    <a href="{{route('contact')}}">Contact Us</a>
                                    
                                </li>
                               
                            </ul>
                        </nav>
                    </div>
                    <div class="header-right">
                         <div class="header-search hs-toggle">
                            <a class="search-toggle" href="#" title="Search">
                                <i class="p-icon-search-solid"></i>
                            </a>
                            <form method="POST" action="{{route('product.search')}}" class="form-simple">
                                 @csrf
                                <input name="search" type="search" autocomplete="off" placeholder="Search in..." required>
                                <button class="btn btn-search" type="submit">
                                    <i class="p-icon-search-solid"></i>
                                </button>
                            </form>
                        </div>
                        <div class="dropdown login-dropdown off-canvas">
                           
                            <!-- End Login Toggle -->
                            <div class="canvas-overlay"></div>
                            <a href="#" class="btn-close"></a>
                            <div class="dropdown-box scrollable">
                                <div class="login-popup">
                                    <div class="form-box">
                                        <div class="tab tab-nav-underline tab-nav-boxed">
                                            <ul class="nav nav-tabs nav-fill mb-4">
                                                <li class="nav-item">
                                                    <a class="nav-link active lh-1 ls-normal" href="#signin">Login</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link lh-1 ls-normal" href="#register">Register</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="signin">
                                                <form class="user"  method="POST" action="{{ route('login') }}">
                                                  @csrf
                                                        <div class="form-group">
                                                            <input type="email" id="exampleInputEmail" name="email"
                                                                placeholder="Username or Email Address" value="{{ old('email') }}" required="">
                                                               
                                                            <input type="password" id="exampleInputPassword"
                                                                name="password" placeholder="password"
                                                                required="">
                                                        </div>
                                                        <div class="form-footer">
                                                            <div class="form-checkbox">
                                                                <input type="checkbox" id="remember"
                                                                    name="remember">
                                                                <label for="signin-remember">
                                                                {{ __('Remember Me') }}
                                                                </label>
                                                            </div>
                                                            <a href="#" class="lost-link">Lost your password?</a>
                                                        </div>
                                                        <button class="btn btn-dark btn-block"
                                                            type="submit">Login</button>
                                                    </form>
                                                    <div class="form-choice text-center">
                                                        <label>or Login With</label>
                                                        <div class="social-links social-link-active ">
                                                            <a href="#" title="Facebook"
                                                                class="social-link social-facebook fab fa-facebook-f"></a>
                                                            <a href="#" title="Twitter"
                                                                class="social-link social-twitter fab fa-twitter"></a>
                                                            <a href="#" title="Linkedin"
                                                                class="social-link social-linkedin fab fa-linkedin-in"></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="tab-pane" id="register">
                                                    <form action="#">
                                                        <div class="form-group">
                                                            <input type="text" id="register-user" name="register-user"
                                                                placeholder="Username" required="">
                                                            <input type="email" id="register-email"
                                                                name="register-email" placeholder="Your Email Address"
                                                                required="">
                                                            <input type="password" id="register-password"
                                                                name="register-password" placeholder="Password"
                                                                required="">
                                                        </div>
                                                        <div class="form-footer mb-5">
                                                            <div class="form-checkbox">
                                                                <input type="checkbox" id="register-agree"
                                                                    name="register-agree" required="">
                                                                <label for="register-agree">I
                                                                    agree to the
                                                                    privacy policy</label>
                                                            </div>
                                                        </div>
                                                        <button class="btn btn-dark btn-block"
                                                            type="submit">Register</button>
                                                    </form>
                                                    <div class="form-choice text-center">
                                                        <label class="ls-m">or Register With</label>
                                                        <div class="social-links social-link-active ">
                                                            <a href="#" title="Facebook"
                                                                class="social-link social-facebook fab fa-facebook-f"></a>
                                                            <a href="#" title="Twitter"
                                                                class="social-link social-twitter fab fa-twitter"></a>
                                                            <a href="#" title="Linkedin"
                                                                class="social-link social-linkedin fab fa-linkedin-in"></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button title="Close (Esc)" type="button" class="mfp-close"><span>×</span></button>
                                </div>
                            </div>
                            <!-- End Dropdown Box -->
                        </div>
                      <!-- Start -------------------------->
                        @php 
                        $total_prod=0;
                        $total_amount=0;
                        @endphp
                        @if(session('wishlist'))
                        @foreach(session('wishlist') as $wishlist_items)
                        @php
                        $total_prod+=$wishlist_items['quantity'];
                        $total_amount+=$wishlist_items['amount'];
                        @endphp
                        @endforeach
                        @endif
                        
                        <div class="dropdown cart-dropdown off-canvas mr-0 mr-lg-2">
                            <a href="{{route('wishlist')}}" class="wishlist wishlist-toggle " title="Wishlist">
                                <i class="p-icon-heart-solid">
                                    <span class="cart-count" style="right: 4px;">{{Helper::wishlistCount()}}</span>
                                </i>
                            </a>
                        </div>
                        <!--  @auth
                         
                                <div class="shopping-item">
                                    <div class="dropdown-cart-header">
                                        <span>{{count(Helper::getAllProductFromWishlist())}} Items</span>
                                        <a href="{{route('wishlist')}}">View Wishlist</a>
                                    </div>
                                    <ul class="shopping-list">
                                        {{-- {{Helper::getAllProductFromCart()}} --}}
                                            @foreach(Helper::getAllProductFromWishlist() as $data)
                                                    @php
                                                        $photo=explode(',',$data->product['photo']);
                                                    @endphp
                                                    <li>
                                                        <a href="{{route('wishlist-delete',$data->id)}}" class="remove" title="Remove this item"><i class="fa fa-remove"></i></a>
                                                        <a class="cart-img" href="#"><img src="{{$photo[0]}}" alt="{{$photo[0]}}"></a>
                                                        <h4><a href="{{route('product-detail',$data->product['slug'])}}" target="_blank">{{$data->product['title']}}</a></h4>
                                                        <p class="quantity">{{$data->quantity}} x - <span class="amount">${{number_format($data->price,2)}}</span></p>
                                                    </li>
                                            @endforeach
                                    </ul>
                                    <div class="bottom">
                                        <div class="total">
                                            <span>Total</span>
                                            <span class="total-amount">${{number_format(Helper::totalWishlistPrice(),2)}}</span>
                                        </div>
                                        <a href="{{route('cart')}}" class="btn animate">Cart</a>
                                    </div>
                                </div>
                               
                            @endauth -->
                                {{-- <div class="sinlge-bar">
                            <a href="{{route('wishlist')}}" class="single-icon"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                        </div> --}}     
                     
                        <!-- Cart ------------------------->
                        
                    <div class="dropdown cart-dropdown off-canvas mr-0 mr-lg-2">
                            <a href="{{route('cart')}}" class="wishlist wishlist-toggle " title="Wishlist">
                                <i class="p-icon-cart-solid">
                                    <span class="cart-count" style="right: 4px;">{{Helper::cartCount()}}</span>
                                </i>
                            </a>
                             <!--  @auth
                               
                                <div class="shopping-item">
                                    <div class="dropdown-cart-header">
                                        <span>{{count(Helper::getAllProductFromCart())}} Items</span>
                                        <a href="{{route('cart')}}">View Cart</a>
                                    </div>
                                    <ul class="shopping-list">
                                        {{-- {{Helper::getAllProductFromCart()}} --}}
                                            @foreach(Helper::getAllProductFromCart() as $data)
                                                    @php
                                                        $photo=explode(',',$data->product['photo']);
                                                    @endphp
                                                    <li>
                                                        <a href="{{route('cart-delete',$data->id)}}" class="remove" title="Remove this item"><i class="fa fa-remove"></i></a>
                                                        <a class="cart-img" href="#"><img src="{{$photo[0]}}" alt="{{$photo[0]}}"></a>
                                                        <h4><a href="{{route('product-detail',$data->product['slug'])}}" target="_blank">{{$data->product['title']}}</a></h4>
                                                        <p class="quantity">{{$data->quantity}} x - <span class="amount">${{number_format($data->price,2)}}</span></p>
                                                    </li>
                                            @endforeach
                                    </ul>
                                    <div class="bottom">
                                        <div class="total">
                                            <span>Total</span>
                                            <span class="total-amount">${{number_format(Helper::totalCartPrice(),2)}}</span>
                                        </div>
                                        <a href="{{route('checkout')}}" class="btn animate">Checkout</a>
                                    </div>
                                </div>
                               
                            @endauth -->
                        </div>
                        <!-- end ---------------------------------->
                       
                    </div>
                </div>
            </div>
        </header>

        <main class="main">
             <div class="page-header" style="background-color: #fff7ec">
                <h1 class="page-title">Blog Detail Page</h1>
            </div>
            <nav class="breadcrumb-nav has-border">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{route('home')}}">Home</a></li>
                        <li>Blog Detail</li>
                       
                    </ul>
                </div>
            </nav><br>
            <div class="page-content">
                <div class="container mb-10 pb-6">
                    <div class="row">
                        <div class="col-lg-9 pr-lg-6">
                            <div class="posts">
                                <article class="post post-single">
                                    <figure class="post-media">
                                        <a href="#">
                                            <img src="{{asset($post->photo)}}" width="905" height="500" alt="{{asset($post->photo)}}" />
                                        </a>
                                    </figure>
                                    <div class="post-details mt-6">
                                        <h2 class="blog-title">{{$post->title}}</h2>
                                        <div class="post-meta">
                                            by
                                            <a href="#" title="Posts by John Doe" class="text-uppercase ml-1 mr-1"
                                                rel="author">{{$post->author_info['name']}} </a>
                                            on
                                            <span class="post-date ml-1"><a href="#"> {{$post->created_at->format('M d, Y')}}</a></span>
                                            <span class="divider mr-2 ml-2"></span>
                                            <i class="far fa-comment-alt mr-2"></i>
                                            <a href="#post-comments"
                                                class="comments-link hash-scroll font-weight-light">
                                                <mark>({{$post->allComments->count()}})</mark>
                                                Comments</a>
                                        </div>
                                        @if($post->quote)
                                        <blockquote> <i class="fa fa-quote-left"></i> {!! ($post->quote) !!}</blockquote>
                                        @endif
                                        <p>{!! ($post->description) !!}</p>
                                        
                                    </div>
                                </article>
                                <div class="share-social">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="content-tags">
                                                <h4>Tags:</h4>
                                                <ul class="tag-inner">
                                                    @php 
                                                        $tags=explode(',',$post->tags);
                                                    @endphp
                                                    @foreach($tags as $tag)
                                                    <li><a href="javascript:void(0);">{{$tag}}</a></li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @auth
                                <div class="reply pb-4">
                                    <h3 class="title title-simple text-left text-normal">Write a Comments</h3>
                                    <p>Your email address will not be published. Required fields are marked*</p>

                                    <form id="commentForm" action="{{route('post-comment.store',$post->slug)}}" method="POST">
                                         @csrf
                                        <div class="row gutter-lg mb-md-2">
                                          {{--  <div class="col-md-6 mb-5">
                                                <label>Your name*</label>
                                                <input type="text" name="name"
                                                    placeholder="Enter your name" required="required">
                                            </div>
                                            <div class="col-md-6 mb-5">
                                                <label>Your email address*</label>
                                                <input type="email" name="email"
                                                    placeholder="Enter your email" required="required">
                                            </div> --}}
                                        </div>
                                        <label>Your comment*</label>
                                        <textarea id="comment" name="comment" cols="30" rows="5" class="mb-6"
                                            placeholder="Enter your comment" required=""></textarea>
                                            <input type="hidden" name="post_id" value="{{ $post->id }}" />
                                            <input type="hidden" name="parent_id" id="parent_id" value="" />
                                        <button type="submit" class="btn btn-dim">POST COMMENT<i
                                                class="d-icon-arrow-right"></i></button>
                                    </form>
                                </div>
                                @else 
                            <p class="text-center p-5">
                                You need to <a href="{{route('login.form')}}" style="color:rgb(54, 54, 204)">Login</a> OR <a style="color:blue" href="{{route('register.form')}}">Register</a> for comment.

                            </p>

                           
                            <!--/ End Form -->

                            @endauth            
                                <!-- End Reply -->

                                <!---Comments Show-->
                                    <div class="comments border-no mb-10 pt-0 pb-2" id="post-comments">
                                    <h2 class="title title-line title-underline"><span>({{$post->allComments->count()}}) Comments</span></h2>
                                    <ul>
                                        <li>
                                            <div class="comment">
                                                
                                                <div class="comment-body">
                                                      @include('frontend.pages.comment', ['comments' => $post->comments, 'post_id' => $post->id, 'depth' => 3])
                                                </div>
                                            </div>
                                          
                                        </li>
                                      
                                    </ul>
                                </div>
                                <!---End Comments Section-->
                            </div>

                            <!-- End Posts -->
                        </div>
                         <aside class="col-lg-3 right-sidebar sidebar-fixed sticky-sidebar-wrapper">
                            <div class="sidebar-overlay">
                            </div>
                            <a class="sidebar-close" href="#"><i class="p-icon-times"></i></a>
                            <a href="#" class="sidebar-toggle"><i class="fas fa-chevron-left"></i></a>
                            <div class="sidebar-content">
                                <div class="sticky-sidebar"
                                    data-sticky-options="{'paddingOffsetTop': 89, 'paddingOffsetBottom': 20}">
                                    <div class="widget widget-search border-no mb-9">
                                        <form method="GET" action="{{route('blog.search')}}" class="form-simple">
                                            <input type="text" name="search" autocomplete="off"
                                                placeholder="Enter your keywords..." required />
                                            <button class="btn btn-search btn-link" type="submit">
                                                <i class="p-icon-search-solid"></i>
                                            </button>
                                        </form>
                                    </div>
                                    <div class="widget widget-sidebar widget-collapsible border-no">
                                        <h3 class="widget-title title-underline"><span
                                                class="title-text">Categories</span>
                                        </h3>
                                        <ul class="widget-body filter-items search-ul">
                                    @if(!empty($_GET['category']))
                                    @php 
                                    $filter_cats=explode(',',$_GET['category']);
                                    @endphp
                                    @endif
                                    <form action="{{route('blog.filter')}}" method="POST">
                                    @csrf
                                    {{-- {{count(Helper::postCategoryList())}} --}}
                                    @foreach(Helper::postCategoryList('posts') as $cat)
                                    <li>
                                        <a href="{{route('blog.category',$cat->slug)}}">{{$cat->title}} </a>
                                    </li>
                                    @endforeach
                                </form>
                                        </ul>
                                    </div>
                                   
                                    <div class="widget widget-posts widget-sidebar widget-collapsible">
                                        <h3 class="widget-title title-underline"><span class="title-text">Browse
                                                Tags</span></h3>
                                        <div class="widget-body widget-tags">
                                          
                                @if(!empty($_GET['tag']))
                                    @php 
                                        $filter_tags=explode(',',$_GET['tag']);
                                    @endphp
                                @endif
                                <form action="{{route('blog.filter')}}" method="POST">
                                    @csrf
                                    @foreach(Helper::postTagList('posts') as $tag)
                                        <li>
                                            <li>
                                                <a href="{{route('blog.tag',$tag->title)}}">{{$tag->title}} </a>
                                            </li>
                                        </li>
                                    @endforeach
                                </form>
                           
                                            
                                        </div>
                                    </div>
                                    <div class="widget widget-sidebar">
                                        <h3 class="widget-title title-underline"><span class="title-text">Recent
                                                Posts</span></h3>
                                                 @foreach($recent_posts as $post)
                                        <div class="widget-body pt-6 pb-4">
                                             @php 
                                            $author_info=DB::table('users')->select('name')->where('id',$post->added_by)->get();
                                        @endphp
                                                <div class="post-col">
                                                    <div class="post post-list-sm">
                                                        <figure class="post-media">
                                                            <a href="{{route('blog.detail',$post->slug)}}">
                                                                <img src="{{asset($post->photo)}}" width="85"
                                                                    height="85" alt="{{$post->photo}}" />
                                                            </a>
                                                        </figure>
                                                        <div class="post-details">
                                                            <h5 class="post-title"><a href="blog-single.html">{{$post->title}}</a>
                                                            </h5>
                                                            <div class="post-meta">
                                                                <a href="blog-single.html" class="post-date">{{$post->created_at->format('d M, y')}}</a>
                                                            </div>
                                                             <li><i class="fa fa-user" aria-hidden="true"></i> 
                                                @foreach($author_info as $data)
                                                    @if($data->name)
                                                        {{$data->name}}
                                                    @else
                                                        Anonymous
                                                    @endif
                                                @endforeach
                                            </li>

                                                            
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                
                                           
                                        </div>
                                         @endforeach
                                    </div>
                                    
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </main>
            <footer class="footer" style="margin-bottom: -20px; !important">
            <div class="container">
                
                <!-- End FooterTop -->
                <div class="footer-middle" style="padding: 8px 0px; !important">
                    <div class="footer-left">
                        <ul class="widget-body">
                            <li>
                                <a href="tel:#" class="footer-icon-box">
                                    <i class="p-icon-phone-solid"></i>
                                    <span>@foreach($settings as $data) {{$data->phone}} @endforeach</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="">
                                    <i class="p-icon-map"></i>
                                    <span>@foreach($settings as $data) {{$data->address}} @endforeach</span>
                                </a>
                            </li>
                            <li>
                                <a href="mailto:mail@panda.com" class="">
                                    <i class="p-icon-message"></i>
                                    <span><a href="mailto:info@yourwebsite.com" style="text-transform: lowercase; !important ">@foreach($settings as $data) {{$data->email}} @endforeach</a></span>
                                </a>
                            </li>
                            <li>
                                <a href="Javascript://" class="">
                                    <i class="p-icon-clock"></i>
                                     <span>Mon-Fri: 09:00AM - 09:00PM</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer-center">
                        <a href="demo1.html" class="logo-footer">
                           <img src="@foreach($settings as $data) {{asset($data->logo)}} @endforeach" alt="logo-footer" width="200" height="41">
                        </a>
                        <div class="social-links">
                            <a href="#" class="social-link fab fa-facebook-f" title="Facebook"></a>
                            <a href="#" class="social-link fab fa-twitter" title="Twitter"></a>
                            <a href="#" class="social-link fab fa-pinterest" title="Pinterest"></a>
                            <a href="#" class="social-link fab fa-linkedin-in" title="Linkedin"></a>
                        </div>
                        <!-- End of Social Links -->
                    </div>
                   @include('frontend.pages.newsletter')
                </div>
                <!-- End FooterMiddle -->
                <div class="footer-bottom">
                    <p class="copyright">Amazing Store © 2022. All Rights Reserved</p>
                    <figure>
                        <img src="{{asset('frontend/images/payment.png')}}" alt="payment" width="159" height="29">
                    </figure>
                </div>
                <!-- End FooterBottom -->
            </div>
        </footer>
        <!-- End Footer -->
    </div>
    <!-- Sticky Footer -->
    <div class="sticky-footer sticky-content fix-bottom">
        <a href="demo1.html" class="sticky-link">
            <i class="p-icon-home"></i>
            <span>Home</span>
        </a>
        <a href="shop.html" class="sticky-link">
            <i class="p-icon-category"></i>
            <span>Categories</span>
        </a>
        <a href="wishlist.html" class="sticky-link">
            <i class="p-icon-heart-solid"></i>
            <span>Wishlist</span>
        </a>
        <a href="account.html" class="sticky-link">
            <i class="p-icon-user-solid"></i>
            <span>Account</span>
        </a>
        <div class="header-search hs-toggle dir-up">
            <a href="#" class="search-toggle sticky-link">
                <i class="p-icon-search-solid"></i>
                <span>Search</span>
            </a>
            <form action="#" class="form-simple">
                <input type="text" name="search" autocomplete="off" placeholder="Search your keyword..." required />
                <button class="btn btn-search" type="submit">
                    <i class="p-icon-search-solid"></i>
                </button>
            </form>
        </div>
    </div>
    <!-- Scroll Top -->
    <a id="scroll-top" class="scroll-top" href="#top" title="Top" role="button"> <i class="p-icon-arrow-up"></i>
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 70">
            <circle id="progress-indicator" fill="transparent" stroke="#000000" stroke-miterlimit="10" cx="35" cy="35"
                r="34" style="stroke-dasharray: 108.881, 400;"></circle>
        </svg>
    </a>

    <!-- MobileMenu -->
    <div class="mobile-menu-wrapper">
        <div class="mobile-menu-overlay">
        </div>
        <!-- End Overlay -->
        <a class="mobile-menu-close" href="#"><i class="p-icon-times"></i></a>
        <!-- End CloseButton -->
        <div class="mobile-menu-container scrollable">
            <form action="#" class="inline-form">
                <input type="search" name="search" autocomplete="off" placeholder="Search your keyword..." required />
                <button class="btn btn-search" type="submit">
                    <i class="p-icon-search-solid"></i>
                </button>
            </form>
            <!-- End Search Form -->
            <ul class="mobile-menu mmenu-anim">
                <li>
                    <a href="demo1.html">Home</a>
                </li>
                <li>
                    <a href="shop.html" class="active">Shop</a>
                    <ul>
                        <li>
                            <a href="#">
                                Shop Layouts
                            </a>
                            <ul>
                                <li><a href="shop-list.html">Shop list</a></li>
                                <li><a href="shop-3-cols.html">3 Columns mode</a>
                                </li>
                                <li><a href="shop-4-cols.html">4 Columns mode</a></li>
                                <li><a href="shop-5-cols.html">5 Columns mode</a>
                                </li>
                                <li><a href="shop-6-cols.html">6 Columns mode</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">
                                Shop Variations
                            </a>
                            <ul>
                                <li><a href="shop-left-sidebar.html">With left sidebar</a>
                                </li>
                                <li><a href="shop-full-width.html">Full width</a>
                                </li>
                                <li><a href="shop-horizontal-filter.html">Horizontal filter</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">
                                Product Details
                            </a>
                            <ul>
                                <li><a href="product-simple.html">Default</a></li>
                                <li><a href="product-gallery.html">Gallery</a></li>
                                <li><a href="product-sticky.html">Sticky info</a></li>
                                <li><a href="product-full.html">Full width</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">
                                Woo Subpages
                            </a>
                            <ul>
                                <li><a href="cart.html">Cart</a></li>
                                <li><a href="checkout.html">Checkout</a></li>
                                <li><a href="wishlist.html">Wishlist</a></li>
                                <li><a href="account.html">My account</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="active">
                    <a href="#">Elements</a>
                    <ul>
                        <li>
                            <a href="#">Elements 1</a>
                            <ul>
                                <li><a href="element-accordions.html">Accordion</a></li>
                                <li><a href="element-alerts.html">Alert & Notification</a></li>
                                <li><a href="element-banner.html">Banner
                                    </a></li>
                                <li><a href="element-banner-effect.html">Banner Effect
                                    </a></li>
                                <li><a href="element-blog.html">Blog</a></li>
                                <li><a href="element-button.html">Button</a></li>
                                <li><a href="element-columns.html">Columns
                                    </a></li>
                                <li><a href="element-countdown.html">Countdown</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Elements 2</a>
                            <ul>
                                <li><a href="element-creative-grid.html">Creative Grid</a></li>
                                <li><a href="element-counter.html">Counter
                                    </a></li>
                                <li><a href="element-entrance-effect.html">Entrance Effect
                                    </a></li>
                                <li><a href="element-mouse-tracking.html">Mouse Tracking Effect
                                    </a></li>
                                <li><a href="element-hotspot.html">Hotspot
                                    </a></li>
                                <li><a href="element-icon-box.html">Icon Box</a></li>
                                <li><a href="element-icons.html">Icon Library</a></li>
                                <li><a href="element-image-box.html">Image box
                                    </a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Elements 3</a>
                            <ul>
                                <li><a href="element-image-gallery.html">Image Gallery</a></li>
                                <li><a href="element-categories.html">Category</a></li>
                                <li><a href="element-products.html">Products
                                    </a></li>
                                <li><a href="element-product-banner.html">Products + Banner
                                    </a></li>
                                <li><a href="element-product-tabs.html">Product Tab
                                    </a>
                                </li>
                                <li><a href="element-section.html">Section Divider

                                    </a></li>
                                <li><a href="element-slider.html">Slider
                                    </a></li>
                                <li><a href="element-social.html">Social Icons
                                    </a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Elements 4</a>
                            <ul>
                                <li><a href="element-tabs.html">Tabs
                                    </a></li>
                                <li><a href="element-testimonial.html">Testimonial

                                    </a></li>
                                <li><a href="element-title.html">Title</a></li>
                                <li><a href="element-typography.html">Typography
                                    </a></li>
                                <li><a href="element-video.html">Video</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="blog.html">Blog</a>
                    <ul>
                        <li><a href="blog.html">Classic</a></li>
                        <li><a href="blog-single.html">Single Post</a></li>
                        <li><a href="blog-2-grid.html">Grid 2 Columns</a></li>
                        <li><a href="blog-3-grid.html">Grid 3 Columns</a></li>
                        <li><a href="blog-4-grid.html">Grid 4 Columns</a></li>
                        <li><a href="blog-sidebar.html">Grid Sidebar</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Pages</a>
                    <ul>
                        <li><a href="about.html">About Us</a></li>
                        <li><a href="contact.html">Contact Us</a></li>
                        <li><a href="account.html">My Account</a></li>
                        <li><a href="faq.html">Faqs</a></li>
                        <li><a href="error.html">Error 404</a></li>
                        <li><a href="coming.html">Coming Soon</a></li>
                    </ul>
                </li>
                <li><a href="https://d-themes.com/buynow/pandahtml/">Buy Panda!</a></li>
            </ul>
            <!-- End MobileMenu -->
        </div>
    </div>
    <!-- Plugins JS File -->
    <script src="{{asset('frontend/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/sticky/sticky.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/imagesloaded/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/elevatezoom/jquery.elevatezoom.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/owl-carousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/nouislider/nouislider.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/wnumb/wNumb.min.js')}}"></script>
    <!-- Main JS File -->
    <script src="{{asset('frontend/js/main.min.js')}}"></script>
</body>


<!-- Mirrored from d-themes.com/html/panda/shop-3-cols.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 19 Feb 2022 15:07:18 GMT -->
</html>
@push('styles')
<script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5f2e5abf393162001291e431&product=inline-share-buttons' async='async'></script>
@endpush
@push('scripts')
<script>
$(document).ready(function(){
    
    (function($) {
        "use strict";

        $('.btn-reply.reply').click(function(e){
            e.preventDefault();
            $('.btn-reply.reply').show();

            $('.comment_btn.comment').hide();
            $('.comment_btn.reply').show();

            $(this).hide();
            $('.btn-reply.cancel').hide();
            $(this).siblings('.btn-reply.cancel').show();

            var parent_id = $(this).data('id');
            var html = $('#commentForm');
            $( html).find('#parent_id').val(parent_id);
            $('#commentFormContainer').hide();
            $(this).parents('.comment-list').append(html).fadeIn('slow').addClass('appended');
          });  

        $('.comment-list').on('click','.btn-reply.cancel',function(e){
            e.preventDefault();
            $(this).hide();
            $('.btn-reply.reply').show();

            $('.comment_btn.reply').hide();
            $('.comment_btn.comment').show();

            $('#commentFormContainer').show();
            var html = $('#commentForm');
            $( html).find('#parent_id').val('');

            $('#commentFormContainer').append(html);
        });
        
 })(jQuery)
})
</script>
@endpush