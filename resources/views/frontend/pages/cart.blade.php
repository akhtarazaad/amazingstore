@extends('frontend.master')

@section('title','E-SHOP || Register Page')

@section('main-content')
         <div class="main cart">
            <div class="page-content pt-8 pb-10 mb-4">
               {{-- <div class="page-header" style="background-color: #fff7ec">
                <h1 class="page-title font-weight-light text-capitalize pt-2">Add To Cart</h1>
            </div> --}}
            <nav class="breadcrumb-nav has-border">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{route('home')}}">Home</a></li>
                        <li>Add to cart</li>
                    </ul>
                </div>
            </nav>
            <!-- Start-->
                 <div class="container mt-7 mb-2">
                    <div class="row">
                        <div class="col-lg-8 col-md-12 pr-lg-6">
                            <form action="{{route('cart.update')}}" method="POST">
                            @csrf
                            <table class="shop-table cart-table">
                                <thead>
                                    <tr>
                                        <th><span>Product</span></th>
                                        <th><span>Name</span></th>
                                        <th><span>Price</span></th>
                                        <th><span>Quantity</span></th>
                                        <th>Subtotal</th>
                                        <th><i class="fa fa-trash remove-icon"></i></th>
                                    </tr>
                                </thead>
                                <tbody>
                               
                                @if(Helper::getAllProductFromCart())
                                @foreach(Helper::getAllProductFromCart() as $key=>$cart)
                                    <tr>
                                        @php 
                                        $photo=explode(',',$cart->product['photo']);
                                        @endphp
                                        <td class="product-thumbnail">
                                            <figure>
                                                <a href="product-simple.html">
                                                    <img src="{{$photo[0]}}" width="90" height="112"
                                                        alt="product">
                                                </a>
                                            </figure>
                                        </td>
                                        <td class="product-name">
                                            <div class="product-name-section">
                                                <a href="{{route('product-detail',$cart->product['slug'])}}">{{$cart->product['title']}}</a>
                                                <p class="product-des">{!!($cart['summary']) !!}</p>
                                            </div>
                                        </td>
                                        <td class="product-subtotal">
                                            <span class="amount">Rs{{$cart['amount']}}</span>
                                        </td>
                                        <td class="product-quantity">
                                            <div class="input-group">
                                                <button class="quantity-minus p-icon-minus-solid" data-field="quant[{{$key}}]"></button>
                                                <input class="quantity form-control" type="number" min="1"
                                                    max="1000000" name="quant[{{$key}}]" value="{{$cart->quantity}}">
                                                <button class="quantity-plus p-icon-plus-solid" data-field="quant[{{$key}}]"></button>
                                            </div>
                                        </td>
                                        <td class="product-price">
                                            <span class="amount">Rs{{number_format($cart['price'],2)}}</span>
                                        </td>
                                        <td class="product-remove">
                                            <a href="{{route('cart-delete',$cart->id)}}" class="btn-remove" title="Remove this product">
                                                <i class="fa fa-trash remove-icon"></i>
                                            </a>
                                        </td>
                                    </tr>
                                  
                                    @endforeach
                                    @else     
                                    There are no any carts available. <a href="{{route('product-grids')}}" style="color:blue;">Continue shopping</a>
                                @endif
                                </tbody>
                            </table>
                            <div class="cart-actions mb-6 pt-6">
                                <a href="{{route('product-grids')}}" class="btn btn-dim btn-icon-left mr-4 mb-4"><i
                                        class="p-icon-arrow-long-left"></i>Continue Shopping</a>
                                <button type="submit" class="btn btn-outline btn-dim">Update
                                    Cart</button>
                            </div>
                        </form>

                            <!-- Copan -->
                            <div class="cart-coupon-box pt-5 pb-8">
                                <h4 class="title coupon-title text-capitalize mb-4">Coupon Discount</h4>
                                <form action="{{route('coupon-store')}}" method="POST">
                                    <input type="text" name="code" class="input-text mb-6" id="coupon_code"
                                        value="" placeholder="Enter coupon code here..." required>
                                    <button type="submit" class="btn btn-dark btn-outline">Apply
                                        Coupon</button>
                                </form>
                            </div>
                             {{-- <div class="checkbox">`
                                        @php 
                                            $shipping=DB::table('shippings')->where('status','active')->limit(1)->get();
                                        @endphp
                                        <label class="checkbox-inline" for="2"><input name="news" id="2" type="checkbox" onchange="showMe('shipping');"> Shipping</label>
                                    </div> --}}
                        </div>

                        <!--side cart total-->
                          <aside class="col-lg-4 sticky-sidebar-wrapper">
                            <div class="sticky-sidebar" data-sticky-options="{'bottom': 20}">
                                <div class="summary mb-4">
                                    <h3 class="summary-title">Cart Totals</h3>
                                    <table class="shipping mb-2">
                                        <tr class="summary-subtotal">
                                            <td>
                                                <h4 class="summary-subtitle" data-price="{{Helper::totalCartdiscount()}}">Subtotal</h4>
                                            </td>
                                            <td>
                                                <p class="summary-subtotal-price">Rs{{number_format(Helper::totalCartdiscount(),2)}}</p>
                                            </td>
                                        </tr>
                                       
                                    </table>
                                    <table class="shipping mb-2">
                                        <tr class="summary-subtotal">
                                            {{-- <div id="shipping" style="display:none;">
                                            <li class="shipping">
                                                Shipping {{session('shipping_price')}}
                                                @if(count(Helper::shipping())>0 && Helper::cartCount()>0)
                                                    <div class="form-select">
                                                        <select name="shipping" class="nice-select">
                                                            <option value="">Select</option>
                                                            @foreach(Helper::shipping() as $shipping)
                                                            <option value="{{$shipping->id}}" class="shippingOption" data-price="{{$shipping->price}}">{{$shipping->type}}: ${{$shipping->price}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                @else 
                                                    <div class="form-select">
                                                        <span>Free</span>
                                                    </div>
                                                @endif
                                            </li>
                                        </div>
                                         --}}
                                          {{-- {{dd(Session::get('coupon')['value'])}} --}}
                                        @if(session()->has('coupon'))
                                        <li class="coupon_price" data-price="{{Session::get('coupon')['value']}}">You Save<span>${{number_format(Session::get('coupon')['value'],2)}}</span></li>
                                        @endif
                                        @php
                                            $total_amount=Helper::totalCartdiscount();
                                            if(session()->has('coupon')){
                                                $total_amount=$total_amount-Session::get('coupon')['value'];
                                            }
                                        @endphp
                                        @if(session()->has('coupon'))
                                            <td>

                                                <h4 class="summary-subtitle" id="order_total_price">You Pay</h4>
                                            </td>
                                            <td>
                                                <p class="summary-subtotal-price">Rs{{number_format($total_amount,2)}}</p>
                                            </td>
                                            @else
                                            <td>

                                                <h4 class="summary-subtitle" id="order_total_price">You Pay</h4>
                                            </td>
                                            <td>
                                                <p class="summary-subtotal-price">Rs{{number_format($total_amount,2)}}</p>
                                            </td>
                                            @endif
                                        </tr>
                                       
                                    </table>

                                   
                                    <a href="{{route('checkout')}}" class="btn btn-dim btn-checkout btn-block">Proceed to
                                        checkout</a>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
                <!--End Here-->
            </div>
        </div>
        @endsection
@push('scripts')
	<script src="{{asset('frontend/js/nice-select/js/jquery.nice-select.min.js')}}"></script>
	<script src="{{ asset('frontend/js/select2/js/select2.min.js') }}"></script>
	<script>
		$(document).ready(function() { $("select.select2").select2(); });
  		$('select.nice-select').niceSelect();
	</script>
	<script>
		$(document).ready(function(){
			$('.shipping select[name=shipping]').change(function(){
				let cost = parseFloat( $(this).find('option:selected').data('price') ) || 0;
				let subtotal = parseFloat( $('.order_subtotal').data('price') ); 
				let coupon = parseFloat( $('.coupon_price').data('price') ) || 0; 
				// alert(coupon);
				$('#order_total_price span').text('$'+(subtotal + cost-coupon).toFixed(2));
			});

		});

	</script>

@endpush