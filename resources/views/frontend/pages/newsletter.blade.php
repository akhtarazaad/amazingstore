 <div class="footer-right">
<div class="widget-newsletter">
    <h4 class="widget-title">Subscribe Newsletter</h4>
    <p>Subscribe to the Amazing Store eCommerce Newsletter<br> updates from your favourite products.
    </p>
    <form action="{{route('subscribe')}}" method="post" class="form-simple">
        @csrf
        <input type="email" name="email"  placeholder="Email address here..."
            required="">
        <button class="btn btn-link" type="submit" style="color: whitesmoke">Subscribe</button>
    </form>
</div>
</div>