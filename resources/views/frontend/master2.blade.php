<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from d-themes.com/html/panda/demo1.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 19 Feb 2022 15:08:47 GMT -->
<head>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

    <title>Amazing Store</title>

    <meta name="keywords" content="" />
    <meta name="description" content="Amazing - Store">
    <meta name="author" content="Amazing Store">

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="{{asset('frontend/images/icons/favicon.png')}}">
        
    <!-- Preload Font -->

    <link rel="preload" href="{{asset('frontend/vendor/fontawesome-free/webfonts/fa-solid-900.woff2')}}" as="font" type="font/woff2"
        crossorigin="anonymous">
    <link rel="preload" href="{{asset('frontend/vendor/fontawesome-free/webfonts/fa-brands-400.woff2')}}" as="font" type="font/woff2"
        crossorigin="anonymous">

    <script>
        WebFontConfig = {
            google: { families: [ 'Josefin Sans:300,400,600,700' ] }
        };
        ( function ( d ) {
            var wf = d.createElement( 'script' ), s = d.scripts[ 0 ];
            wf.src = '{{asset('frontend/js/webfont.js')}}';
            wf.async = true;
            s.parentNode.insertBefore( wf, s );
        } )( document );
    </script>


    <link rel="stylesheet" type="text/css" href="{{asset('frontend/vendor/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/vendor/animate/animate.min.css')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="/dd.svg">
<link rel="icon" type="image/png" sizes="32x32" href="/dd.svg">
<link rel="icon" type="image/png" sizes="16x16" href="/dd.svg">
<!-- <link rel="manifest" href="/site.webmanifest">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5"> -->
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">

    <!-- Plugin CSS File -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/vendor/owl-carousel/owl.carousel.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/vendor/magnific-popup/magnific-popup.min.css')}}">
    <!-- Main CSS File -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/demo1.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/mycss.css')}}">
</head>
@php
$settings=DB::table('settings')->get();

@endphp
<body class="home">
    <div class="page-wrapper">
        <header class="header">
            <div class="header-top">
                <div class="container">
                    <div class="header-left">
                        <a href="tel:#" class="call">
                            <i class="fa fa-phone"></i>
                            <span>@foreach($settings as $data) {{$data->phone}} @endforeach</span>
                        </a>
                        <span class="divider"></span>
                        <a href="/contact" class="contact">
                            <i class="fa fa-envelope"></i>
                            <span> @foreach($settings as $data) {{$data->email}} @endforeach</span>
                        </a>
                    </div>
                    <div class="header-right">
                        
                          <i class="fa fa-map-marker"><a href="{{route('order.track')}}"> Track Order</a></i>
                            {{-- <li><i class="ti-alarm-clock"></i> <a href="#">Daily deal</a></li> --}}
                            @auth 
                                @if(Auth::user()->role=='admin')
                                   <i class="fa fa-user"> <a href="{{route('admin')}}"  target="_blank"> Dashboard</a></i>
                                @else 
                                    <i class="fa fa-user"> <a href="{{route('user')}}"  target="_blank"> Dashboard</a></i>
                                @endif
                               <i class="fa fa-power-off"> <a href="{{route('user.logout')}}"> Logout</a></i>

                            @else
                                <i class="fa fa-power-off"><a href="{{route('login.form')}}"> Login /Register</a></i>
                            @endauth
                            
                      
                        <span class="divider"></span>
                        <!-- End DropDown Menu -->
                        <div class="social-links">
                            <a href="#" class="social-link fab fa-facebook-f" title="Facebook"></a>
                            <a href="#" class="social-link fab fa-twitter" title="Twitter"></a>
                            <a href="#" class="social-link fab fa-pinterest" title="Pinterest"></a>
                            <a href="#" class="social-link fab fa-linkedin-in" title="Linkedin"></a>
                        </div>
                        <!-- End of Social Links -->
                    </div>
                </div>
            </div>
            <!-- End HeaderTop -->
            <div class="header-middle has-center sticky-header fix-top sticky-content">
                <div class="container">
                    <div class="header-left">
                        <a href="#" class="mobile-menu-toggle" title="Mobile Menu">
                            <i class="p-icon-bars-solid"></i>
                        </a>
                        @php
                            $settings=DB::table('settings')->get();
                        @endphp  
                        <a href="{{route('home')}}" class="logo">
                            <img src="@foreach($settings as $data) {{$data->logo}} @endforeach" alt="logo" width="80">
                        </a>
                        <!-- End of Divider -->
                    </div>
                    <div class="header-center">
                        <nav class="main-nav">
                            <ul class="menu">
                                <li class="{{Request::path()=='home' ? 'active' : ''}}">
                                    <a href="{{route('home')}}">Home</a>
                                </li>
                                <li class="{{Request::path()=='about-us' ? 'active' : ''}}">
                                    <a href="{{route('about-us')}}">About Us</a>
                                    
                                </li>

                                <li class="@if(Request::path()=='product-grids'||Request::path()=='product-lists')  active  @endif">
                                    <a href="{{route('product-grids')}}">Products</a>
                                   
                                </li>
                                {{Helper::getHeaderCategory()}}
                                <li class="{{Request::path()=='blog' ? 'active' : ''}}">
                                    <a href="{{route('blog')}}">Blog</a>
                                    
                                </li>
                                <li class="{{Request::path()=='contact' ? 'active' : ''}}">
                                    <a href="{{route('contact')}}">Contact Us//</a>
                                    
                                </li>
                               
                            </ul>
                        </nav>
                    </div>
                    <div class="header-right">
                        <div class="header-search hs-toggle">
                            <a class="search-toggle" href="#" title="Search">
                                <i class="p-icon-search-solid"></i>
                            </a>
                            <form action="#" class="form-simple">
                                <input type="search" autocomplete="off" placeholder="Search in..." required>
                                <button class="btn btn-search" type="submit">
                                    <i class="p-icon-search-solid"></i>
                                </button>
                            </form>
                        </div>
                        <div class="dropdown login-dropdown off-canvas">
                           
                            <!-- End Login Toggle -->
                            <div class="canvas-overlay"></div>
                            <a href="#" class="btn-close"></a>
                            <div class="dropdown-box scrollable">
                                <div class="login-popup">
                                    <div class="form-box">
                                        <div class="tab tab-nav-underline tab-nav-boxed">
                                            <ul class="nav nav-tabs nav-fill mb-4">
                                                <li class="nav-item">
                                                    <a class="nav-link active lh-1 ls-normal" href="#signin">Login</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link lh-1 ls-normal" href="#register">Register</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="signin">
                                                <form class="user"  method="POST" action="{{ route('login') }}">
                                                  @csrf
                                                        <div class="form-group">
                                                            <input type="email" id="exampleInputEmail" name="email"
                                                                placeholder="Username or Email Address" value="{{ old('email') }}" required="">
                                                               
                                                            <input type="password" id="exampleInputPassword"
                                                                name="password" placeholder="password"
                                                                required="">
                                                        </div>
                                                        <div class="form-footer">
                                                            <div class="form-checkbox">
                                                                <input type="checkbox" id="remember"
                                                                    name="remember">
                                                                <label for="signin-remember">
                                                                {{ __('Remember Me') }}
                                                                </label>
                                                            </div>
                                                            <a href="#" class="lost-link">Lost your password?</a>
                                                        </div>
                                                        <button class="btn btn-dark btn-block"
                                                            type="submit">Login</button>
                                                    </form>
                                                    <div class="form-choice text-center">
                                                        <label>or Login With</label>
                                                        <div class="social-links social-link-active ">
                                                            <a href="#" title="Facebook"
                                                                class="social-link social-facebook fab fa-facebook-f"></a>
                                                            <a href="#" title="Twitter"
                                                                class="social-link social-twitter fab fa-twitter"></a>
                                                            <a href="#" title="Linkedin"
                                                                class="social-link social-linkedin fab fa-linkedin-in"></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="tab-pane" id="register">
                                                    <form action="#">
                                                        <div class="form-group">
                                                            <input type="text" id="register-user" name="register-user"
                                                                placeholder="Username" required="">
                                                            <input type="email" id="register-email"
                                                                name="register-email" placeholder="Your Email Address"
                                                                required="">
                                                            <input type="password" id="register-password"
                                                                name="register-password" placeholder="Password"
                                                                required="">
                                                        </div>
                                                        <div class="form-footer mb-5">
                                                            <div class="form-checkbox">
                                                                <input type="checkbox" id="register-agree"
                                                                    name="register-agree" required="">
                                                                <label for="register-agree">I
                                                                    agree to the
                                                                    privacy policy</label>
                                                            </div>
                                                        </div>
                                                        <button class="btn btn-dark btn-block"
                                                            type="submit">Register</button>
                                                    </form>
                                                    <div class="form-choice text-center">
                                                        <label class="ls-m">or Register With</label>
                                                        <div class="social-links social-link-active ">
                                                            <a href="#" title="Facebook"
                                                                class="social-link social-facebook fab fa-facebook-f"></a>
                                                            <a href="#" title="Twitter"
                                                                class="social-link social-twitter fab fa-twitter"></a>
                                                            <a href="#" title="Linkedin"
                                                                class="social-link social-linkedin fab fa-linkedin-in"></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button title="Close (Esc)" type="button" class="mfp-close"><span>×</span></button>
                                </div>
                            </div>
                            <!-- End Dropdown Box -->
                        </div>

                         <div class="col-lg-2 col-md-3 col-12">
                    <div class="header-right">
                        <!-- Search Form -->
                        <div class="sinlge-bar shopping">
                            @php 
                                $total_prod=0;
                                $total_amount=0;
                            @endphp
                           @if(session('wishlist'))
                                @foreach(session('wishlist') as $wishlist_items)
                                    @php
                                        $total_prod+=$wishlist_items['quantity'];
                                        $total_amount+=$wishlist_items['amount'];
                                    @endphp
                                @endforeach
                           @endif
                            <a href="{{route('wishlist')}}" class="single-icon"><i class="fa fa-heart"></i> <span class="total-count">{{Helper::wishlistCount()}}</span></a>
                            <!-- Shopping Item -->
                            @auth
                                <div class="shopping-item">
                                    <div class="dropdown-cart-header">
                                        <span>{{count(Helper::getAllProductFromWishlist())}} Items</span>
                                        <a href="{{route('wishlist')}}">View Wishlist</a>
                                    </div>
                                    <ul class="shopping-list">
                                        {{-- {{Helper::getAllProductFromCart()}} --}}
                                            @foreach(Helper::getAllProductFromWishlist() as $data)
                                                    @php
                                                        $photo=explode(',',$data->product['photo']);
                                                    @endphp
                                                    <li>
                                                        <a href="{{route('wishlist-delete',$data->id)}}" class="remove" title="Remove this item"><i class="fa fa-remove"></i></a>
                                                        <a class="cart-img" href="#"><img src="{{$photo[0]}}" alt="{{$photo[0]}}"></a>
                                                        <h4><a href="{{route('product-detail',$data->product['slug'])}}" target="_blank">{{$data->product['title']}}</a></h4>
                                                        <p class="quantity">{{$data->quantity}} x - <span class="amount">${{number_format($data->price,2)}}</span></p>
                                                    </li>
                                            @endforeach
                                    </ul>
                                    <div class="bottom">
                                        <div class="total">
                                            <span>Total</span>
                                            <span class="total-amount">${{number_format(Helper::totalWishlistPrice(),2)}}</span>
                                        </div>
                                        <a href="{{route('cart')}}" class="btn animate">Cart</a>
                                    </div>
                                </div>
                            @endauth
                            <!--/ End Shopping Item -->
                        </div>
                        {{-- <div class="sinlge-bar">
                            <a href="{{route('wishlist')}}" class="single-icon"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                        </div> --}}
                        <div class="sinlge-bar shopping">
                            <a href="{{route('cart')}}" class="single-icon"><i class="fa fa-shopping-bag"></i> <span class="total-count">{{Helper::cartCount()}}</span></a>
                            <!-- Shopping Item -->
                            @auth
                                <div class="shopping-item">
                                    <div class="dropdown-cart-header">
                                        <span>{{count(Helper::getAllProductFromCart())}} Items</span>
                                        <a href="{{route('cart')}}">View Cart</a>
                                    </div>
                                    <ul class="shopping-list">
                                        {{-- {{Helper::getAllProductFromCart()}} --}}
                                            @foreach(Helper::getAllProductFromCart() as $data)
                                                    @php
                                                        $photo=explode(',',$data->product['photo']);
                                                    @endphp
                                                    <li>
                                                        <a href="{{route('cart-delete',$data->id)}}" class="remove" title="Remove this item"><i class="fa fa-remove"></i></a>
                                                        <a class="cart-img" href="#"><img src="{{$photo[0]}}" alt="{{$photo[0]}}"></a>
                                                        <h4><a href="{{route('product-detail',$data->product['slug'])}}" target="_blank">{{$data->product['title']}}</a></h4>
                                                        <p class="quantity">{{$data->quantity}} x - <span class="amount">${{number_format($data->price,2)}}</span></p>
                                                    </li>
                                            @endforeach
                                    </ul>
                                    <div class="bottom">
                                        <div class="total">
                                            <span>Total</span>
                                            <span class="total-amount">${{number_format(Helper::totalCartPrice(),2)}}</span>
                                        </div>
                                        <a href="{{route('checkout')}}" class="btn animate">Checkout</a>
                                    </div>
                                </div>
                            @endauth
                            <!--/ End Shopping Item -->
                        </div>
                    </div>
                </div>
                       
                    </div>
                </div>
            </div>
        </header>
        <!-- End Header -->
        @yield('main-content')
        <!-- End Main -->
        <footer class="footer">
            <div class="container">
                <div class="footer-top">
                    <ul class="menu menu-type2">
                        <li>
                            <a href="about.html">About us</a>
                        </li>
                        <li>
                            <a href="#">our team</a>
                        </li>
                        <li>
                            <a href="faq.html">faq</a>
                        </li>
                        <li>
                            <a href="account.html">my account</a>
                        </li>
                        <li>
                            <a href="/contact">contact us</a>
                        </li>
                    </ul>
                </div>
                <!-- End FooterTop -->
                <div class="footer-middle">
                    <div class="footer-left">
                        <ul class="widget-body">
                            <li>
                                <a href="tel:#" class="footer-icon-box">
                                    <i class="p-icon-phone-solid"></i>
                                    <span>+456 789 000</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="">
                                    <i class="p-icon-map"></i>
                                    <span>25 West 21th Street, Miami FL, USA</span>
                                </a>
                            </li>
                            <li>
                                <a href="mailto:mail@panda.com" class="">
                                    <i class="p-icon-message"></i>
                                    <span>info@panda.com</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="">
                                    <i class="p-icon-clock"></i>
                                    <span>Mon-Fri: 10:00 - 18:00</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer-center">
                        <a href="demo1.html" class="logo-footer">
                            <img src="{{asset('frontend/images/logo.png')}}" alt="logo-footer" width="171" height="41">
                        </a>
                        <div class="social-links">
                            <a href="#" class="social-link fab fa-facebook-f" title="Facebook"></a>
                            <a href="#" class="social-link fab fa-twitter" title="Twitter"></a>
                            <a href="#" class="social-link fab fa-pinterest" title="Pinterest"></a>
                            <a href="#" class="social-link fab fa-linkedin-in" title="Linkedin"></a>
                        </div>
                        <!-- End of Social Links -->
                    </div>
                   @include('frontend.pages.newsletter')
                </div>
                <!-- End FooterMiddle -->
                <div class="footer-bottom">
                    <p class="copyright">Panda eCommerce © 2022. All Rights Reserved</p>
                    <figure>
                        <img src="{{asset('frontend/images/payment.png')}}" alt="payment" width="159" height="29">
                    </figure>
                </div>
                <!-- End FooterBottom -->
            </div>
        </footer>
        <!-- End Footer -->
    </div>
    <!-- Sticky Footer -->
    <div class="sticky-footer sticky-content fix-bottom">
        <a href="demo1.html" class="sticky-link">
            <i class="p-icon-home"></i>
            <span>Home</span>
        </a>
        <a href="shop.html" class="sticky-link">
            <i class="p-icon-category"></i>
            <span>Categories</span>
        </a>
        <a href="wishlist.html" class="sticky-link">
            <i class="p-icon-heart-solid"></i>
            <span>Wishlist</span>
        </a>
        <a href="account.html" class="sticky-link">
            <i class="p-icon-user-solid"></i>
            <span>Account</span>
        </a>
        <div class="header-search hs-toggle dir-up">
            <a href="#" class="search-toggle sticky-link">
                <i class="p-icon-search-solid"></i>
                <span>Search</span>
            </a>
            <form action="#" class="form-simple">
                <input type="text" name="search" autocomplete="off" placeholder="Search your keyword..." required />
                <button class="btn btn-search" type="submit">
                    <i class="p-icon-search-solid"></i>
                </button>
            </form>
        </div>
    </div>
    <!-- Scroll Top -->
    <a id="scroll-top" class="scroll-top" href="#top" title="Top" role="button"> <i class="p-icon-arrow-up"></i>
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 70">
            <circle id="progress-indicator" fill="transparent" stroke="#000000" stroke-miterlimit="10" cx="35" cy="35"
                r="34" style="stroke-dasharray: 108.881, 400;"></circle>
        </svg>
    </a>

    <!-- MobileMenu -->
    <div class="mobile-menu-wrapper">
        <div class="mobile-menu-overlay">
        </div>
        <!-- End Overlay -->
        <a class="mobile-menu-close" href="#"><i class="p-icon-times"></i></a>
        <!-- End CloseButton -->
        <div class="mobile-menu-container scrollable">
            <form action="#" class="inline-form">
                <input type="search" name="search" autocomplete="off" placeholder="Search your keyword..." required />
                <button class="btn btn-search" type="submit">
                    <i class="p-icon-search-solid"></i>
                </button>
            </form>
            <!-- End Search Form -->
            <ul class="mobile-menu mmenu-anim">
                <li class="{{Request::path()=='home' ? 'active' : ''}}">
                    <a href="{{route('home')}}">Home</a>
                </li>
                <li class="@if(Request::path()=='product-grids'||Request::path()=='product-lists')  active  @endif">
                    <a href="{{route('product-grids')}}">Products</a>
                   
                </li>
            </li>
            {{Helper::getHeaderCategory()}}
            <li class="{{Request::path()=='blog' ? 'active' : ''}}">
                <a href="{{route('blog')}}">Blog</a>
                
            </li>
            <li class="{{Request::path()=='about-us' ? 'active' : ''}}">
                <a href="{{route('about-us')}}">About Us</a>
                
            </li>
            <li class="{{Request::path()=='contact' ? 'active' : ''}}">
                <a href="{{route('contact')}}">Contact Us</a>
                
            </li>
               
                        
                       
            
        </div>
            <!-- End MobileMenu -->
        </div>
    </div>
    <!-- <div class="newsletter-popup mfp-hide" id="newsletter-popup">
        <figure>
            <img src="{{asset('frontend/images/newsletter-popup.jpg')}}" width="500" height="269" alt="newsletter">
        </figure>
        <div class="newsletter-content">
            <h3>Join Our Mailing List</h3>
            <p>Stay informed! Monthly tips and discount.</p>
            <form action="#" method="get" class="inline-form mx-auto">
                <input type="email" name="email" id="email2" placeholder="Email address here..." required="">
                <button class="btn btn-dark" type="submit">SUBMIT</button>
            </form>
            <div class="form-checkbox">
                <input type="checkbox" id="hide-newsletter-popup" name="hide-newsletter-popup" required="">
                <label for="hide-newsletter-popup">Don't show this popup again</label>
            </div>
            <div class="social-links">
                <a href="#" title="facebook" class="social-link fab fa-facebook-f"></a>
                <a href="#" title="twitter" class="social-link fab fa-twitter"></a>
                <a href="#" title="pinterest" class="social-link fab fa-pinterest"></a>
                <a href="#" title="linkedin" class="social-link fab fa-linkedin-in"></a>
            </div>
        </div>
    </div> -->
    <!-- Plugins JS File -->
    <script src="{{asset('frontend/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/owl-carousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/imagesloaded/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('frontend/vendor/elevatezoom/jquery.elevatezoom.min.js')}}"></script>
    <!-- Main JS File -->
    <script src="{{asset('frontend/js/main.js')}}"></script>
</body>


<!-- Mirrored from d-themes.com/html/panda/demo1.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 19 Feb 2022 15:08:55 GMT -->
</html>