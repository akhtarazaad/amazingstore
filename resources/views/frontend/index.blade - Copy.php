@extends('frontend.master')
@section('title','E-SHOP || HOME PAGE')
@section('main-content')
<main class="main">
            <div class="page-content">
                <section class="intro-section">
                    @if(count($banners)>0)
                    @foreach($banners as $key=>$banner)
                   
                    @endforeach

                    <div class="intro-slider owl-carousel owl-theme owl-nav-arrow row animation-slider cols-1 gutter-no mb-8"
                        data-owl-options="{
                                'nav': true,
                                'dots': false,
                                'loop': false,
                                'items': 1,
                                'responsive': {
                                    '0': {
                                        'nav': false,
                                        'autoplay': true
                                    },
                                    '768': {
                                        'nav': true
                                    }
                                }
                            }">
                             @foreach($banners as $key=>$banner)
                        <div class="banner banner-fixed banner1">
                            <figure>
                                <img src="{{$banner->photo}}" alt="banner" width="1903" height="400"
                                    style="background-color: #f8f6f6;">
                            </figure>
                            <div class="banner-content y-50 pb-1">
                                <h4 class="banner-subtitle title-underline2 font-weight-normal text-dim slide-animate appear-animate"
                                    data-animation-options="{
                                                'name': 'fadeInUpShorter',
                                                'delay': '.2s'
                                            }">
                                    <span>{!! html_entity_decode($banner->description) !!}</span></h4>
                                <h3 class="banner-title text-dark lh-1 mb-7 slide-animate appear-animate"
                                    data-animation-options="{
                                                'name': 'fadeInUpShorter',
                                                'delay': '.4s'
                                            }">
                                   {{$banner->title}}</h3>
                                <a href="shop.html" class="btn btn-dark slide-animate appear-animate"
                                    data-animation-options="{
                                                'name': 'fadeInUpShorter',
                                                'delay': '.6s'
                                            }">SHop
                                    now<i class="p-icon-arrow-long-right"></i></a>
                            </div>
                        </div>
                           @endforeach   
                    </div>
                    @endif
                    <div class="container">
                        <div class="owl-carousel owl-theme owl-box-border row cols-md-3 cols-sm-2 cols-1 appear-animate"
                            data-owl-options="{
                                                'nav': false,
                                                'dots': false,
                                                'loop': false,
                                                'responsive': {
                                                    '0': {
                                                        'items': 1,
                                                        'autoplay': true
                                                    },
                                                    '576': {
                                                        'items': 2,
                                                        'autoplay': true
                                                    },
                                                    '768': {
                                                        'items': 3,
                                                        'dots': false
                                                    }
                                                }
                                            }">
                            <div class="icon-box icon-box-side">
                                <span class="icon-box-icon">
                                    <i class="p-icon-shipping-solid"></i>
                                </span>
                                <div class="icon-box-content">
                                    <h4 class="icon-box-title">FREE SHIPPING & RETURN</h4>
                                    <p>Free shipping on orders over $99</p>
                                </div>
                            </div>
                            <div class="icon-box icon-box-side">
                                <span class="icon-box-icon">
                                    <i class="p-icon-quality"></i>
                                </span>
                                <div class="icon-box-content">
                                    <h4 class="icon-box-title">QUALITY GUARANTEED</h4>
                                    <p>We offer high quality of products</p>
                                </div>
                            </div>
                            <div class="icon-box icon-box-side">
                                <span class="icon-box-icon">
                                    <i class="p-icon-fax2"></i>
                                </span>
                                <div class="icon-box-content">
                                    <h4 class="icon-box-title">SECURE PAYMENT</h4>
                                    <p>We ensure secure payment!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="container mt-10 pt-7 mb-7 appear-animate">
                    <h2 class="title-underline2 text-center mb-2"><span>Top Products</span></h2>
                    <div class="tab tab-nav-center product-tab product-tab-type2">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" href="#canned">
                                    <figure>
                                        <img src="{{asset('frontend/images/elements/product_tab/nav-1.jpg')}}" width="160" height="130"
                                            alt="Nav img" />
                                    </figure>
                                    <div class="nav-title">Canned</div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#fruits2">
                                    <figure>
                                        <img src="{{asset('frontend/images/elements/product_tab/nav-2.jpg')}}" width="160" height="130"
                                            alt="Nav img" />
                                    </figure>
                                    <div class="nav-title">Fruits</div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#coffees">
                                    <figure>
                                        <img src="{{asset('frontend/images/elements/product_tab/nav-3.jpg')}}" width="160" height="130"
                                            alt="Nav img" />
                                    </figure>
                                    <div class="nav-title">Coffees</div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#meats">
                                    <figure>
                                        <img src="{{asset('frontend/images/elements/product_tab/nav-4.jpg')}}" width="160" height="130"
                                            alt="Nav img" />
                                    </figure>
                                    <div class="nav-title">Meats</div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#vegetables">
                                    <figure>
                                        <img src="{{asset('frontend/images/elements/product_tab/nav-5.jpg')}}" width="160" height="130"
                                            alt="Nav img" />
                                    </figure>
                                    <div class="nav-title">Vegetables</div>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="canned">
                                <div class="owl-carousel owl-theme row cols-lg-4 cols-md-3 cols-2" data-owl-options="{
                                            'items': 4,
                                            'nav': false,
                                            'dots': true,
                                            'margin': 20,
                                            'loop': false,
                                            'responsive': {
                                                '0': {
                                                    'items': 2
                                                },
                                                '768': {
                                                    'items': 3
                                                },
                                                '992': {
                                                    'items': 4
                                                }
                                            }
                                        }">

                                @if($product_lists)
                                @foreach($product_lists as $key=>$product)
                                <div class="product-wrap">
                           
                                    <div class="product shadow-media text-center">
                                        <figure class="product-media">
                                            <a href="product-simple.html">
                                                @php 
                                                $photo=explode(',',$product->photo);
                                                // dd($photo);
                                                @endphp
                                                <img src="{{$photo[0]}}" alt="{{$photo[0]}}" width="295"
                                                    height="369" />
                                                <img class="hover-img" src="{{$photo[0]}}" alt="{{$photo[0]}}" width="295"
                                                    height="369" />

                                            </a>
                                            @if($product->discount)
                                                    <span class="price-dec">{{$product->discount}} % Off</span>
                                                    @endif

                                            <div class="product-action-vertical">
                                                
                                                <a title="Wishlist" href="{{route('add-to-wishlist',$product->slug)}}" class="wishlist" data-id="{{$product->id}}"><i class=" p-icon-heart-solid "></i></a>

                                                <div class="product-action-2">
                                                        <a title="Add to cart" href="{{route('add-to-cart',$product->slug)}}" style="margin-right: 18px;">
                                                           <i class=" p-icon-cart-solid "></i>
                                                        </a>
                                                    </div>
                                               
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:60%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="product-simple.html#content-reviews"
                                                    class="rating-reviews">(14)</a>
                                            </div>
                                            <h5 class="product-name">
                                                <a href=" {{route('product-detail',$product->slug)}}">
                                                  {{$product->title}}
                                                </a>
                                            </h5>
                                             @php
                                             $after_discount=($product->price-($product->price*$product->discount)/100);
                                             @endphp
                                            <span class="product-price">
                                                <del class="old-price">${{number_format($after_discount,2)}}</del>
                                                <ins class="new-price">${{number_format($product->price,2)}}</ins>
                                            </span>
                                        </div>
                                    </div>


                                    <!-- End .product -->
                                </div>
                                @endforeach
                                @endif
                                    <!-- End .product -->

    

                                </div>
                            </div>
                            <div class="tab-pane" id="fruits2">
                                <div class="owl-carousel owl-theme row cols-lg-4 cols-md-3 cols-2" data-owl-options="{
                                            'items': 4,
                                            'nav': false,
                                            'dots': true,
                                            'margin': 20,
                                            'loop': false,
                                            'responsive': {
                                                '0': {
                                                    'items': 2
                                                },
                                                '768': {
                                                    'items': 3
                                                },
                                                '992': {
                                                    'items': 4
                                                }
                                            }
                                        }">
                                    <div class="product shadow-media text-center">
                                        <figure class="product-media">
                                            <a href="product-simple.html">
                                                <img src="{{asset('frontend/images/products/1-295x369.jpg')}}" alt="product" width="295"
                                                    height="369" />
                                            </a>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to Cart">
                                                    <i class="p-icon-cart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to Wishlist">
                                                    <i class="p-icon-heart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                                    <i class="p-icon-compare-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                                    <i class="p-icon-search-solid"></i>
                                                </a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:60%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="product-simple.html#content-reviews"
                                                    class="rating-reviews">(12)</a>
                                            </div>
                                            <h5 class="product-name">
                                                <a href="product-simple.html">
                                                    Fresh Peach
                                                </a>
                                            </h5>
                                            <span class="product-price">
                                                <span class="price">$42.00</span>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- End .product -->

                                    <div class="product shadow-media text-center">
                                        <figure class="product-media">
                                            <a href="product-simple.html">
                                                <img src="{{asset('frontend/images/products/19-295x369.jpg')}}" alt="product" width="295"
                                                    height="369" />
                                                <img src="{{asset('frontend/images/products/19-2-295x369.jpg')}}" alt="product" width="295"
                                                    height="369" />
                                            </a>
                                            <div class="product-label-group">
                                                <label class="product-label label-sale">-40%</label>
                                            </div>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to Cart">
                                                    <i class="p-icon-cart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to Wishlist">
                                                    <i class="p-icon-heart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                                    <i class="p-icon-compare-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                                    <i class="p-icon-search-solid"></i>
                                                </a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:60%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="product-simple.html#content-reviews"
                                                    class="rating-reviews">(12)</a>
                                            </div>
                                            <h5 class="product-name">
                                                <a href="product-simple.html">
                                                    Fresh Mulberry
                                                </a>
                                            </h5>
                                            <span class="product-price">
                                                <del class="old-price">$28.00</del>
                                                <ins class="new-price">$12.00</ins>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- End .product -->

                                    <div class="product shadow-media text-center">
                                        <figure class="product-media">
                                            <a href="product-simple.html">
                                                <img src="{{asset('frontend/images/products/4-295x369.jpg')}}" alt="product" width="295"
                                                    height="369" />
                                            </a>
                                            <div class="product-label-group">
                                                <label class="product-label label-new">New</label>
                                                <label class="product-label label-sale">-38%</label>
                                            </div>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to Cart">
                                                    <i class="p-icon-cart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to Wishlist">
                                                    <i class="p-icon-heart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                                    <i class="p-icon-compare-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                                    <i class="p-icon-search-solid"></i>
                                                </a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:60%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="product-simple.html#content-reviews"
                                                    class="rating-reviews">(12)</a>
                                            </div>
                                            <h5 class="product-name">
                                                <a href="product-simple.html">
                                                    Fresh Cherry
                                                </a>
                                            </h5>
                                            <span class="product-price">
                                                <del class="old-price">$90.00</del>
                                                <ins class="new-price">$36.00</ins>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- End .product -->

                                    <div class="product shadow-media text-center">
                                        <figure class="product-media">
                                            <a href="product-simple.html">
                                                <img src="{{asset('frontend/images/products/3-295x369.jpg')}}" alt="product" width="295"
                                                    height="369" />
                                            </a>
                                            <div class="product-label-group">
                                                <label class="product-label label-sale">-24%</label>
                                            </div>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to Cart">
                                                    <i class="p-icon-cart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to Wishlist">
                                                    <i class="p-icon-heart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                                    <i class="p-icon-compare-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                                    <i class="p-icon-search-solid"></i>
                                                </a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:60%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="product-simple.html#content-reviews"
                                                    class="rating-reviews">(12)</a>
                                            </div>
                                            <h5 class="product-name">
                                                <a href="product-simple.html">
                                                    Organic Palmetto
                                                </a>
                                            </h5>
                                            <span class="product-price">
                                                <del class="old-price">$22.00</del>
                                                <ins class="new-price">$16.00</ins>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- End .product -->


                                </div>
                            </div>
                            <div class="tab-pane" id="coffees">
                                <div class="owl-carousel owl-theme row cols-lg-4 cols-md-3 cols-2" data-owl-options="{
                                            'items': 4,
                                            'nav': false,
                                            'dots': true,
                                            'margin': 20,
                                            'loop': false,
                                            'responsive': {
                                                '0': {
                                                    'items': 2
                                                },
                                                '768': {
                                                    'items': 3
                                                },
                                                '992': {
                                                    'items': 4
                                                }
                                            }
                                        }">
                                    <div class="product shadow-media text-center">
                                        <figure class="product-media">
                                            <a href="product-simple.html">
                                                <img src="{{asset('frontend/images/products/7-295x369.jpg')}}" alt="product" width="295"
                                                    height="369" />
                                                <img src="{{asset('frontend/images/products/7-2-295x369.jpg')}}" alt="product" width="295"
                                                    height="369" />
                                            </a>
                                            <div class="product-label-group">
                                                <label class="product-label label-sale">-24%</label>
                                            </div>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to Cart">
                                                    <i class="p-icon-cart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to Wishlist">
                                                    <i class="p-icon-heart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                                    <i class="p-icon-compare-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                                    <i class="p-icon-search-solid"></i>
                                                </a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:100%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="product-simple.html#content-reviews"
                                                    class="rating-reviews">(12)</a>
                                            </div>
                                            <h5 class="product-name">
                                                <a href="product-simple.html">
                                                    Salted Caramel
                                                </a>
                                            </h5>
                                            <span class="product-price">
                                                <del class="old-price">$22.00</del>
                                                <ins class="new-price">$16.00</ins>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- End .product -->

                                    <div class="product shadow-media text-center">
                                        <figure class="product-media">
                                            <a href="product-simple.html">
                                                <img src="{{asset('frontend/images/products/8-295x369.jpg')}}" alt="product" width="295"
                                                    height="369" />
                                            </a>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to Cart">
                                                    <i class="p-icon-cart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to Wishlist">
                                                    <i class="p-icon-heart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                                    <i class="p-icon-compare-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                                    <i class="p-icon-search-solid"></i>
                                                </a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:60%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="product-simple.html#content-reviews"
                                                    class="rating-reviews">(12)</a>
                                            </div>
                                            <h5 class="product-name">
                                                <a href="product-simple.html">
                                                    Broccoli
                                                </a>
                                            </h5>
                                            <span class="product-price">
                                                <span class="price">$42.00</span>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- End .product -->

                                    <div class="product shadow-media text-center">
                                        <figure class="product-media">
                                            <a href="product-simple.html">
                                                <img src="{{asset('frontend/images/products/5-295x369.jpg')}}" alt="product" width="295"
                                                    height="369" />
                                                <img src="{{asset('frontend/images/products/5-2-295x369.jpg')}}" alt="product" width="295"
                                                    height="369" />
                                            </a>
                                            <div class="product-label-group">
                                                <label class="product-label label-sale">-40%</label>
                                            </div>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to Cart">
                                                    <i class="p-icon-cart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to Wishlist">
                                                    <i class="p-icon-heart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                                    <i class="p-icon-compare-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                                    <i class="p-icon-search-solid"></i>
                                                </a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:0%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="product-simple.html#content-reviews"
                                                    class="rating-reviews">(12)</a>
                                            </div>
                                            <h5 class="product-name">
                                                <a href="product-simple.html">
                                                    Peanuts
                                                </a>
                                            </h5>
                                            <span class="product-price">
                                                <del class="old-price">$28.00</del>
                                                <ins class="new-price">$12.00</ins>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- End .product -->

                                    <div class="product shadow-media text-center">
                                        <figure class="product-media">
                                            <a href="product-simple.html">
                                                <img src="{{asset('frontend/images/products/6-295x369.jpg')}}" alt="product" width="295"
                                                    height="369" />
                                            </a>
                                            <div class="product-label-group">
                                                <label class="product-label label-new">New</label>
                                                <label class="product-label label-sale">-38%</label>
                                            </div>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to Cart">
                                                    <i class="p-icon-cart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to Wishlist">
                                                    <i class="p-icon-heart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                                    <i class="p-icon-compare-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                                    <i class="p-icon-search-solid"></i>
                                                </a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:60%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="product-simple.html#content-reviews"
                                                    class="rating-reviews">(12)</a>
                                            </div>
                                            <h5 class="product-name">
                                                <a href="product-simple.html">
                                                    Peas
                                                </a>
                                            </h5>
                                            <span class="product-price">
                                                <del class="old-price">$90.00</del>
                                                <ins class="new-price">$36.00</ins>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- End .product -->

                                </div>
                            </div>
                            <div class="tab-pane" id="meats">
                                <div class="owl-carousel owl-theme row cols-lg-4 cols-md-3 cols-2" data-owl-options="{
                                            'items': 4,
                                            'nav': false,
                                            'dots': true,
                                            'margin': 20,
                                            'loop': false,
                                            'responsive': {
                                                '0': {
                                                    'items': 2
                                                },
                                                '768': {
                                                    'items': 3
                                                },
                                                '992': {
                                                    'items': 4
                                                }
                                            }
                                        }">
                                    <div class="product shadow-media text-center">
                                        <figure class="product-media">
                                            <a href="product-simple.html">
                                                <img src="{{asset('frontend/images/products/11-295x369.jpg')}}" alt="product" width="295"
                                                    height="369" />
                                            </a>
                                            <div class="product-label-group">
                                                <label class="product-label label-new">New</label>
                                                <label class="product-label label-sale">-38%</label>
                                            </div>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to Cart">
                                                    <i class="p-icon-cart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to Wishlist">
                                                    <i class="p-icon-heart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                                    <i class="p-icon-compare-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                                    <i class="p-icon-search-solid"></i>
                                                </a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:0%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="product-simple.html#content-reviews"
                                                    class="rating-reviews">(12)</a>
                                            </div>
                                            <h5 class="product-name">
                                                <a href="product-simple.html">
                                                    Fresh Pork
                                                </a>
                                            </h5>
                                            <span class="product-price">
                                                <del class="old-price">$90.00</del>
                                                <ins class="new-price">$36.00</ins>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- End .product -->


                                    <div class="product shadow-media text-center">
                                        <figure class="product-media">
                                            <a href="product-simple.html">
                                                <img src="{{asset('frontend/images/products/20-295x369.jpg')}}" alt="product" width="295"
                                                    height="369" />
                                            </a>
                                            <div class="product-label-group">
                                                <label class="product-label label-sale">-40%</label>
                                            </div>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to Cart">
                                                    <i class="p-icon-cart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to Wishlist">
                                                    <i class="p-icon-heart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                                    <i class="p-icon-compare-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                                    <i class="p-icon-search-solid"></i>
                                                </a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:60%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="product-simple.html#content-reviews"
                                                    class="rating-reviews">(12)</a>
                                            </div>
                                            <h5 class="product-name">
                                                <a href="product-simple.html">
                                                    Prime Beef
                                                </a>
                                            </h5>
                                            <span class="product-price">
                                                <del class="old-price">$28.00</del>
                                                <ins class="new-price">$12.00</ins>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- End .product -->

                                    <div class="product shadow-media text-center">
                                        <figure class="product-media">
                                            <a href="product-simple.html">
                                                <img src="{{asset('frontend/images/products/13-295x369.jpg')}}" alt="product" width="295"
                                                    height="369" />
                                                <img src="{{asset('frontend/images/products/13-2-295x369.jpg')}}" alt="product" width="295"
                                                    height="369" />
                                            </a>
                                            <div class="product-label-group">
                                                <label class="product-label label-sale">-24%</label>
                                            </div>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to Cart">
                                                    <i class="p-icon-cart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to Wishlist">
                                                    <i class="p-icon-heart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                                    <i class="p-icon-compare-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                                    <i class="p-icon-search-solid"></i>
                                                </a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:60%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="product-simple.html#content-reviews"
                                                    class="rating-reviews">(12)</a>
                                            </div>
                                            <h5 class="product-name">
                                                <a href="product-simple.html">
                                                    Salted Beef
                                                </a>
                                            </h5>
                                            <span class="product-price">
                                                <del class="old-price">$22.00</del>
                                                <ins class="new-price">$16.00</ins>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- End .product -->



                                </div>
                            </div>
                            <div class="tab-pane" id="vegetables">
                                <div class="owl-carousel owl-theme row cols-lg-4 cols-md-3 cols-2" data-owl-options="{
                                            'items': 4,
                                            'nav': false,
                                            'dots': true,
                                            'margin': 20,
                                            'loop': false,
                                            'responsive': {
                                                '0': {
                                                    'items': 2
                                                },
                                                '768': {
                                                    'items': 3
                                                },
                                                '992': {
                                                    'items': 4
                                                }
                                            }
                                        }">
                                    <div class="product shadow-media text-center">
                                        <figure class="product-media">
                                            <a href="product-simple.html">
                                                <img src="{{asset('frontend/images/products/5-295x369.jpg')}}" alt="product" width="295"
                                                    height="369" />
                                                <img src="{{asset('frontend/images/products/5-2-295x369.jpg')}}" alt="product" width="295"
                                                    height="369" />
                                            </a>
                                            <div class="product-label-group">
                                                <label class="product-label label-sale">-40%</label>
                                            </div>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to Cart">
                                                    <i class="p-icon-cart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to Wishlist">
                                                    <i class="p-icon-heart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                                    <i class="p-icon-compare-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                                    <i class="p-icon-search-solid"></i>
                                                </a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:20%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="product-simple.html#content-reviews"
                                                    class="rating-reviews">(12)</a>
                                            </div>
                                            <h5 class="product-name">
                                                <a href="product-simple.html">
                                                    Peanuts
                                                </a>
                                            </h5>
                                            <span class="product-price">
                                                <del class="old-price">$28.00</del>
                                                <ins class="new-price">$12.00</ins>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- End .product -->

                                    <div class="product shadow-media text-center">
                                        <figure class="product-media">
                                            <a href="product-simple.html">
                                                <img src="{{asset('frontend/images/products/6-295x369.jpg')}}" alt="product" width="295"
                                                    height="369" />
                                            </a>
                                            <div class="product-label-group">
                                                <label class="product-label label-new">New</label>
                                                <label class="product-label label-sale">-38%</label>
                                            </div>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to Cart">
                                                    <i class="p-icon-cart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to Wishlist">
                                                    <i class="p-icon-heart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                                    <i class="p-icon-compare-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                                    <i class="p-icon-search-solid"></i>
                                                </a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:60%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="product-simple.html#content-reviews"
                                                    class="rating-reviews">(12)</a>
                                            </div>
                                            <h5 class="product-name">
                                                <a href="product-simple.html">
                                                    Peas
                                                </a>
                                            </h5>
                                            <span class="product-price">
                                                <del class="old-price">$90.00</del>
                                                <ins class="new-price">$36.00</ins>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- End .product -->

                                    <div class="product shadow-media text-center">
                                        <figure class="product-media">
                                            <a href="product-simple.html">
                                                <img src="{{asset('frontend/images/products/7-295x369.jpg')}}" alt="product" width="295"
                                                    height="369" />
                                                <img src="{{asset('frontend/images/products/7-2-295x369.jpg')}}" alt="product" width="295"
                                                    height="369" />
                                            </a>
                                            <div class="product-label-group">
                                                <label class="product-label label-sale">-24%</label>
                                            </div>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to Cart">
                                                    <i class="p-icon-cart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to Wishlist">
                                                    <i class="p-icon-heart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                                    <i class="p-icon-compare-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                                    <i class="p-icon-search-solid"></i>
                                                </a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:100%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="product-simple.html#content-reviews"
                                                    class="rating-reviews">(12)</a>
                                            </div>
                                            <h5 class="product-name">
                                                <a href="product-simple.html">
                                                    Salted Caramel
                                                </a>
                                            </h5>
                                            <span class="product-price">
                                                <del class="old-price">$22.00</del>
                                                <ins class="new-price">$16.00</ins>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- End .product -->

                                    <div class="product shadow-media text-center">
                                        <figure class="product-media">
                                            <a href="product-simple.html">
                                                <img src="{{asset('frontend/images/products/8-295x369.jpg')}}" alt="product" width="295"
                                                    height="369" />
                                            </a>
                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                                    data-target="#addCartModal" title="Add to Cart">
                                                    <i class="p-icon-cart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-wishlist"
                                                    title="Add to Wishlist">
                                                    <i class="p-icon-heart-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                                    <i class="p-icon-compare-solid"></i>
                                                </a>
                                                <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                                    <i class="p-icon-search-solid"></i>
                                                </a>
                                            </div>
                                        </figure>
                                        <div class="product-details">
                                            <div class="ratings-container">
                                                <div class="ratings-full">
                                                    <span class="ratings" style="width:60%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                                <a href="product-simple.html#content-reviews"
                                                    class="rating-reviews">(12)</a>
                                            </div>
                                            <h5 class="product-name">
                                                <a href="product-simple.html">
                                                    Broccoli
                                                </a>
                                            </h5>
                                            <span class="product-price">
                                                <span class="price">$42.00</span>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- End .product -->

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="benefit-section appear-animate" style="background: #fafaf8;">
                    <div class="container">
                        <h4 class="subtitle title-underline2 text-uppercase text-center"><span>Why Top Products?</span>
                        </h4>
                        <h2 class="title justify-content-center text-center pb-6 mb-10">100% Natural, 100% Organic</h2>
                        <figure class="img-back floating">
                            <img class="layer" src="{{asset('frontend/images/demos/demo1/banner/banner1.jpg')}}" width="674" height="514"
                                alt="banner" />
                        </figure>
                        <div class="row appear-animate">
                            <div class="col-md-6">
                                <div class="icon-box ml-md-4">
                                    <span class="icon-box-icon">
                                        <i class="p-icon-heartbeat-solid" style="font-size: 2.05em;"></i>
                                    </span>
                                    <div class="icon-box-content">
                                        <h4 class="icon-box-title">Good for Health</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur
                                            eiusmod tempor incididunt ut labore.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pos-right">
                                <div class="icon-box mr-md-4">
                                    <span class="icon-box-icon" style="margin-bottom: 1.9rem;">
                                        <i class="p-icon-quality"></i>
                                    </span>
                                    <div class="icon-box-content">
                                        <h4 class="icon-box-title">High Nutrition</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur
                                            eiusmod tempor incididunt ut labore.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="icon-box ml-md-4">
                                    <span class="icon-box-icon" style="margin-bottom: 2rem;">
                                        <i class="p-icon-fruit"></i>
                                    </span>
                                    <div class="icon-box-content">
                                        <h4 class="icon-box-title">Always Fresh</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur
                                            eiusmod tempor incididunt ut labore.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pos-right">
                                <div class="icon-box mr-md-4">
                                    <span class="icon-box-icon" style="margin-bottom: 15px;">
                                        <i class="p-icon-filter" style="font-size: 1.9em;"></i>
                                    </span>
                                    <div class="icon-box-content">
                                        <h4 class="icon-box-title">No Fertilizer</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur
                                            eiusmod tempor incididunt ut labore.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="new-section container mt-10 pt-8 appear-animate">
                    <h2 class="title title-underline2 justify-content-center mb-8"><span>New Arrivals</span></h2>
                    <div class="owl-carousel owl-theme owl-nav-arrow owl-nav-outer owl-nav-image-center row cols-lg-4 cols-md-3 cols-2"
                        data-owl-options="{
                                    'items': 4,
                                    'nav': false,
                                    'dots': true,
                                    'margin': 20,
                                    'loop': false,
                                    'responsive': {
                                        '0': {
                                            'items': 2
                                        },
                                        '768': {
                                            'items': 3,
                                            'dots': true
                                        },
                                        '992': {
                                            'items': 4
                                        },
                                        '1400': {
                                            'nav': true,
                                            'dots': false
                                        }
                                    }
                                }">
                        <div class="product shadow-media text-center">
                            <figure class="product-media">
                                <a href="product-simple.html">
                                    <img src="{{asset('frontend/images/products/21-295x369.jpg')}}" alt="product" width="295" height="369" />
                                </a>
                                <div class="product-label-group">
                                    <label class="product-label label-new">Top</label>
                                </div>
                                <div class="product-action-vertical">
                                    <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                        data-target="#addCartModal" title="Add to Cart">
                                        <i class="p-icon-cart-solid"></i>
                                    </a>
                                    <a href="#" class="btn-product-icon btn-wishlist" title="Add to Wishlist">
                                        <i class="p-icon-heart-solid"></i>
                                    </a>
                                    <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                        <i class="p-icon-compare-solid"></i>
                                    </a>
                                    <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                        <i class="p-icon-search-solid"></i>
                                    </a>
                                </div>
                            </figure>
                            <div class="product-details">
                                <div class="ratings-container">
                                    <div class="ratings-full">
                                        <span class="ratings" style="width:60%"></span>
                                        <span class="tooltiptext tooltip-top"></span>
                                    </div>
                                    <a href="product-simple.html#content-reviews" class="rating-reviews">(12)</a>
                                </div>
                                <h5 class="product-name">
                                    <a href="product-simple.html">
                                        Butter Glutinous Cake
                                    </a>
                                </h5>
                                <span class="product-price">
                                    <del class="old-price">$28.00</del>
                                    <ins class="new-price">$12.00</ins>
                                </span>
                            </div>
                        </div>
                        <!-- End .product -->

                        <div class="product shadow-media text-center">
                            <figure class="product-media">
                                <a href="product-simple.html">
                                    <img src="{{asset('frontend/images/products/19-295x369.jpg')}}" alt="product" width="295" height="369" />
                                </a>
                                <div class="product-label-group">
                                    <label class="product-label label-new">Top</label>
                                </div>
                                <div class="product-action-vertical">
                                    <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                        data-target="#addCartModal" title="Add to Cart">
                                        <i class="p-icon-cart-solid"></i>
                                    </a>
                                    <a href="#" class="btn-product-icon btn-wishlist" title="Add to Wishlist">
                                        <i class="p-icon-heart-solid"></i>
                                    </a>
                                    <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                        <i class="p-icon-compare-solid"></i>
                                    </a>
                                    <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                        <i class="p-icon-search-solid"></i>
                                    </a>
                                </div>
                            </figure>
                            <div class="product-details">
                                <div class="ratings-container">
                                    <div class="ratings-full">
                                        <span class="ratings" style="width:100%"></span>
                                        <span class="tooltiptext tooltip-top"></span>
                                    </div>
                                    <a href="product-simple.html#content-reviews" class="rating-reviews">(12)</a>
                                </div>
                                <h5 class="product-name">
                                    <a href="product-simple.html">
                                        Fresh Mulberry
                                    </a>
                                </h5>
                                <span class="product-price">
                                    <del class="old-price">$90.00</del>
                                    <ins class="new-price">$36.00</ins>
                                </span>
                            </div>
                        </div>
                        <!-- End .product -->

                        <div class="product shadow-media text-center">
                            <figure class="product-media">
                                <a href="product-simple.html">
                                    <img src="{{asset('frontend/images/products/20-295x369.jpg')}}" alt="product" width="295" height="369" />
                                </a>
                                <div class="product-action-vertical">
                                    <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                        data-target="#addCartModal" title="Add to Cart">
                                        <i class="p-icon-cart-solid"></i>
                                    </a>
                                    <a href="#" class="btn-product-icon btn-wishlist" title="Add to Wishlist">
                                        <i class="p-icon-heart-solid"></i>
                                    </a>
                                    <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                        <i class="p-icon-compare-solid"></i>
                                    </a>
                                    <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                        <i class="p-icon-search-solid"></i>
                                    </a>
                                </div>
                            </figure>
                            <div class="product-details">
                                <div class="ratings-container">
                                    <div class="ratings-full">
                                        <span class="ratings" style="width:60%"></span>
                                        <span class="tooltiptext tooltip-top"></span>
                                    </div>
                                    <a href="product-simple.html#content-reviews" class="rating-reviews">(12)</a>
                                </div>
                                <h5 class="product-name">
                                    <a href="product-simple.html">
                                        Prime Beef
                                    </a>
                                </h5>
                                <span class="product-price">
                                    <del class="old-price">$22.00</del>
                                    <ins class="new-price">$16.00</ins>
                                </span>
                            </div>
                        </div>
                        <!-- End .product -->

                        <div class="product shadow-media text-center">
                            <figure class="product-media">
                                <a href="product-simple.html">
                                    <img src="{{asset('frontend/images/products/22-295x369.jpg')}}" alt="product" width="295" height="369" />
                                </a>
                                <div class="product-label-group">
                                    <label class="product-label label-new">Top</label>
                                </div>
                                <div class="product-action-vertical">
                                    <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                        data-target="#addCartModal" title="Add to Cart">
                                        <i class="p-icon-cart-solid"></i>
                                    </a>
                                    <a href="#" class="btn-product-icon btn-wishlist" title="Add to Wishlist">
                                        <i class="p-icon-heart-solid"></i>
                                    </a>
                                    <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                        <i class="p-icon-compare-solid"></i>
                                    </a>
                                    <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                        <i class="p-icon-search-solid"></i>
                                    </a>
                                </div>
                            </figure>
                            <div class="product-details">
                                <div class="ratings-container">
                                    <div class="ratings-full">
                                        <span class="ratings" style="width:100%"></span>
                                        <span class="tooltiptext tooltip-top"></span>
                                    </div>
                                    <a href="product-simple.html#content-reviews" class="rating-reviews">(12)</a>
                                </div>
                                <h5 class="product-name">
                                    <a href="product-simple.html">
                                        Organic Garlic
                                    </a>
                                </h5>
                                <span class="product-price">
                                    <span class="price">$42.00</span>
                                </span>
                            </div>
                        </div>
                        <!-- End .product -->

                        <div class="product shadow-media text-center">
                            <figure class="product-media">
                                <a href="product-simple.html">
                                    <img src="{{asset('frontend/images/products/4-295x369.jpg')}}" alt="product" width="295" height="369" />
                                </a>
                                <div class="product-action-vertical">
                                    <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                        data-target="#addCartModal" title="Add to Cart">
                                        <i class="p-icon-cart-solid"></i>
                                    </a>
                                    <a href="#" class="btn-product-icon btn-wishlist" title="Add to Wishlist">
                                        <i class="p-icon-heart-solid"></i>
                                    </a>
                                    <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                        <i class="p-icon-compare-solid"></i>
                                    </a>
                                    <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                        <i class="p-icon-search-solid"></i>
                                    </a>
                                </div>
                            </figure>
                            <div class="product-details">
                                <div class="ratings-container">
                                    <div class="ratings-full">
                                        <span class="ratings" style="width:80%"></span>
                                        <span class="tooltiptext tooltip-top"></span>
                                    </div>
                                    <a href="product-simple.html" class="rating-reviews">(6)</a>
                                </div>
                                <h5 class="product-name">
                                    <a href="product-simple.html">
                                        Fresh Cherry
                                    </a>
                                </h5>
                                <span class="product-price">
                                    <span class="price">$36.00</span>
                                </span>
                            </div>
                        </div>
                        <!-- End .product -->
                    </div>
                    <div class="row mt-10 appear-animate">
                        <div class="col-md-6">
                            <div class="banner banner-fixed banner3 mb-md-0 mb-4">
                                <figure>
                                    <img src="{{asset('frontend/images/demos/demo1/banner/banner2.jpg')}}" alt="banner" width="610"
                                        height="250" style="background-color: #86786b;">
                                </figure>
                                <div class="banner-content y-50">
                                    <h4 class="banner-subtitle font-weight-normal text-white ls-1 mt-1">
                                        JUST ARRIVED</h4>
                                    <h3 class="banner-title text-white lh-1 mb-3">New DietCoke</h3>
                                    <a href="shop.html" class="btn btn-underline btn-link text-white">Shop
                                        Now<i class="p-icon-arrow-long-right pl-1"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="banner banner-fixed banner3">
                                <figure>
                                    <img src="{{asset('frontend/images/demos/demo1/banner/banner3.jpg')}}" alt="banner" width="610"
                                        height="250" style="background-color: #ddd;">
                                </figure>
                                <div class="banner-content y-50">
                                    <h4 class="banner-subtitle font-weight-normal text-dim ls-1 mt-1">
                                        BEST SELLING</h4>
                                    <h3 class="banner-title text-dark lh-1 mb-3">
                                        Special Snack</h3>
                                    <a href="shop.html" class="btn btn-underline btn-link text-dark">Shop
                                        Now<i class="p-icon-arrow-long-right pl-1"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="feature-section appear-animate">
                    <div class="container mt-10 pt-8 mb-10 pb-2">
                        <h2 class="title title-underline2 justify-content-center mb-8"><span>Our Featured</span>
                        </h2>
                        <div class="owl-carousel owl-theme owl-nav-arrow owl-nav-outer owl-nav-image-center row cols-lg-5 cols-md-3 cols-2"
                            data-owl-options="{
                                            'items': 5,
                                            'nav': false,
                                            'dots': true,
                                            'margin': 20,
                                            'loop': false,
                                            'responsive': {
                                                '0': {
                                                    'items': 2
                                                },
                                                '768': {
                                                    'items': 3,
                                                    'dots': true
                                                },
                                                '992': {
                                                    'items': 5
                                                },
                                                '1400': {
                                                    'nav': true,
                                                    'dots': false
                                                }
                                            }
                                        }">
                            <div class="product shadow-media text-center">
                                <figure class="product-media">
                                    <a href="product-simple.html">
                                        <img src="{{asset('frontend/images/products/25-232x290.jpg')}}" alt="product" width="232"
                                            height="290" />
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                            data-target="#addCartModal" title="Add to Cart">
                                            <i class="p-icon-cart-solid"></i>
                                        </a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to Wishlist">
                                            <i class="p-icon-heart-solid"></i>
                                        </a>
                                        <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                            <i class="p-icon-compare-solid"></i>
                                        </a>
                                        <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                            <i class="p-icon-search-solid"></i>
                                        </a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:60%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="product-simple.html#content-reviews" class="rating-reviews">(12)</a>
                                    </div>
                                    <h5 class="product-name">
                                        <a href="product-simple.html">
                                            Low-Fat Peanut Oil
                                        </a>
                                    </h5>
                                    <span class="product-price">
                                        <span class="price">$48.00</span>
                                    </span>
                                </div>
                            </div>
                            <!-- End .product -->
                            <div class="product shadow-media text-center">
                                <figure class="product-media">
                                    <a href="product-simple.html">
                                        <img src="{{asset('frontend/images/products/4-232x290.jpg')}}" alt="product" width="232"
                                            height="290" />
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                            data-target="#addCartModal" title="Add to Cart">
                                            <i class="p-icon-cart-solid"></i>
                                        </a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to Wishlist">
                                            <i class="p-icon-heart-solid"></i>
                                        </a>
                                        <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                            <i class="p-icon-compare-solid"></i>
                                        </a>
                                        <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                            <i class="p-icon-search-solid"></i>
                                        </a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="product-simple.html#content-reviews" class="rating-reviews">(12)</a>
                                    </div>
                                    <h5 class="product-name">
                                        <a href="product-simple.html">
                                            Fresh Cherry
                                        </a>
                                    </h5>
                                    <span class="product-price mb-1">
                                        <del class="old-price">$24.00</del>
                                        <ins class="new-price">$16.00</ins>
                                    </span>
                                </div>
                            </div>
                            <!-- End .product -->
                            <div class="product shadow-media text-center">
                                <figure class="product-media">
                                    <a href="product-simple.html">
                                        <img src="{{asset('frontend/images/products/24-232x290.jpg')}}" alt="product" width="232"
                                            height="290" />
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                            data-target="#addCartModal" title="Add to Cart">
                                            <i class="p-icon-cart-solid"></i>
                                        </a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to Wishlist">
                                            <i class="p-icon-heart-solid"></i>
                                        </a>
                                        <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                            <i class="p-icon-compare-solid"></i>
                                        </a>
                                        <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                            <i class="p-icon-search-solid"></i>
                                        </a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:60%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="product-simple.html#content-reviews" class="rating-reviews">(12)</a>
                                    </div>
                                    <h5 class="product-name">
                                        <a href="product-simple.html">
                                            Dark Chocolate
                                        </a>
                                    </h5>
                                    <span class="product-price">
                                        <del class="old-price">$28.00</del>
                                        <ins class="new-price">$22.00</ins>
                                    </span>
                                </div>
                            </div>
                            <!-- End .product -->
                            <div class="product shadow-media text-center">
                                <figure class="product-media">
                                    <a href="product-simple.html">
                                        <img src="{{asset('frontend/images/products/27-232x290.jpg')}}" alt="product" width="232"
                                            height="290" />
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                            data-target="#addCartModal" title="Add to Cart">
                                            <i class="p-icon-cart-solid"></i>
                                        </a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to Wishlist">
                                            <i class="p-icon-heart-solid"></i>
                                        </a>
                                        <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                            <i class="p-icon-compare-solid"></i>
                                        </a>
                                        <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                            <i class="p-icon-search-solid"></i>
                                        </a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:60%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="product-simple.html#content-reviews" class="rating-reviews">(12)</a>
                                    </div>
                                    <h5 class="product-name">
                                        <a href="product-simple.html">
                                            Box Cake
                                        </a>
                                    </h5>
                                    <span class="product-price">
                                        <span class="price">$38.00</span>
                                    </span>
                                </div>
                            </div>
                            <!-- End .product -->
                            <div class="product shadow-media text-center">
                                <figure class="product-media">
                                    <a href="product-simple.html">
                                        <img src="{{asset('frontend/images/products/17-232x290.jpg')}}" alt="product" width="232"
                                            height="290" />
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                            data-target="#addCartModal" title="Add to Cart">
                                            <i class="p-icon-cart-solid"></i>
                                        </a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to Wishlist">
                                            <i class="p-icon-heart-solid"></i>
                                        </a>
                                        <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                            <i class="p-icon-compare-solid"></i>
                                        </a>
                                        <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                            <i class="p-icon-search-solid"></i>
                                        </a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:90%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="product-simple.html#content-reviews" class="rating-reviews">(12)</a>
                                    </div>
                                    <h5 class="product-name">
                                        <a href="product-simple.html">
                                            Dry Jujube
                                        </a>
                                    </h5>
                                    <span class="product-price">
                                        <del class="old-price">$22.00</del>
                                        <ins class="new-price">$15.00</ins>
                                    </span>
                                </div>
                            </div>
                            <!-- End .product -->
                            <div class="product shadow-media text-center">
                                <figure class="product-media">
                                    <a href="product-simple.html">
                                        <img src="{{asset('frontend/images/products/21-295x369.jpg')}}" alt="product" width="295"
                                            height="369" />
                                    </a>
                                    <div class="product-label-group">
                                        <label class="product-label label-new">Top</label>
                                    </div>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                            data-target="#addCartModal" title="Add to Cart">
                                            <i class="p-icon-cart-solid"></i>
                                        </a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to Wishlist">
                                            <i class="p-icon-heart-solid"></i>
                                        </a>
                                        <a href="#" class="btn-product-icon btn-compare" title="Compare">
                                            <i class="p-icon-compare-solid"></i>
                                        </a>
                                        <a href="#" class="btn-product-icon btn-quickview" title="Quick View">
                                            <i class="p-icon-search-solid"></i>
                                        </a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:60%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="product-simple.html#content-reviews" class="rating-reviews">(12)</a>
                                    </div>
                                    <h5 class="product-name">
                                        <a href="product-simple.html">
                                            Butter Glutinous Cake
                                        </a>
                                    </h5>
                                    <span class="product-price">
                                        <del class="old-price">$28.00</del>
                                        <ins class="new-price">$12.00</ins>
                                    </span>
                                </div>
                            </div>
                            <!-- End .product -->
                        </div>
                    </div>
                    <div class="banner banner-fixed banner1 appear-animate">
                        <figure>
                            <img src="{{asset('frontend/images/demos/demo1/banner/banner4.jpg')}}" width="1920" height="500" alt="banner"
                                style="background: #f6f1ec;" />
                        </figure>
                        <div class="banner-content y-50 pb-1">
                            <h3 class="banner-subtitle title-underline2 font-weight-normal text-uppercase text-dim pb-1 appear-animate"
                                data-animation-options="{
                                            'name': 'fadeInUpShorter',
                                            'delay': '.2s'
                                        }">
                                <span>100% Organic</span></h3>
                            <h2 class="banner-title text-dark lh-1 mb-7 appear-animate" data-animation-options="{
                                            'name': 'fadeInUpShorter',
                                            'delay': '.4s'
                                        }">
                                Fresh Vegetables<br>for you</h2>
                            <a href="shop.html" class="btn btn-dark appear-animate" data-animation-options="{
                                            'name': 'fadeInUpShorter',
                                            'delay': '.6s'
                                        }">SHop
                                now<i class="p-icon-arrow-long-right"></i></a>
                        </div>
                    </div>
                </section>
                <section class="recent-section container mt-10 pt-7 mb-10 pb-6">
                    <h4 class="subtitle title-underline2 text-uppercase text-center"><span>Our Blog</span>
                    </h4>
                    <h2 class="title justify-content-center text-center">Recent Stories And Articles</h2>
                    <div class="owl-carousel owl-theme row cols-lg-3 cols-sm-2 cols-1 mb-10 pb-8" data-owl-options="{
                                        'items': 3,
                                        'nav': false,
                                        'dots': true,
                                        'margin': 20,
                                        'loop': false,
                                        'responsive': {
                                            '0': {
                                                'items': 1
                                            },
                                            '568': {
                                                'items': 2
                                            },
                                            '992': {
                                                'items': 3
                                            }
                                        }
                                }">
                        <div class="post post-border post-center overlay-zoom overlay-dark">
                            <figure class="post-media">
                                <a href="blog-single.html">
                                    <img src="{{asset('frontend/images/blog/1-400x250.jpg')}}" width="400" height="250" alt="post">
                                </a>
                                <div class="post-calendar">
                                    18 Feb 2021
                                </div>
                            </figure>
                            <div class="post-details">
                                <p class="post-cats"><a href="blog.html">Vegetable</a>,<a href="blog.html">
                                        Fruit</a></p>
                                <h3 class="post-title"><a href="blog-single.html">Aliquam id diam
                                        maecenas<br>ultricies
                                        get mauris</a>
                                </h3>
                                <div class="post-meta">
                                    <a href="blog.html" class="post-author">
                                        <img src="{{asset('frontend/images/agents/1.jpg')}}" class="post-agent" width="31" height="31"
                                            alt="agent">By<span>Anna</span></a>
                                    <a href="blog-single.html#post-comments" class="post-comments hash-scroll">
                                        <i class="p-icon-email"></i>0
                                    </a>
                                    <div class="post-share">
                                        <i class="p-icon-socials"></i>
                                        <div class="social-links dirVertical">
                                            <a href="#" title="Facebook" class="social-link fab fa-facebook-f"></a>
                                            <a href="#" title="Twitter" class="social-link fab fa-twitter"></a>
                                            <a href="#" title="Pinterest" class="social-link fab fa-pinterest"></a>
                                            <a href="#" title="Linkedin" class="social-link fab fa-linkedin-in"></a>
                                        </div>
                                    </div>
                                </div>
                                <p class="post-content">Lorem ipsum dolor sit amet,anadipis sed do sed doeiu smod
                                    tempo...<a href="blog-single.html" class="ml-2 text-primary">(read more)</a></p>
                            </div>
                        </div>
                        <div class="post post-border post-center overlay-zoom overlay-dark">
                            <figure class="post-media">
                                <a href="blog-single.html">
                                    <img src="{{asset('frontend/images/blog/2-400x250.jpg')}}" width="400" height="250" alt="post">
                                </a>
                                <div class="post-calendar">
                                    18 Feb 2021
                                </div>
                            </figure>
                            <div class="post-details">
                                <p class="post-cats"><a href="blog.html">Vegetable</a>,<a href="blog.html"> Fruit</a>
                                </p>
                                <h3 class="post-title"><a href="blog-single.html">Aliquam id diam
                                        maecenas<br>ultricies
                                        get mauris</a>
                                </h3>
                                <div class="post-meta">
                                    <a href="blog.html" class="post-author">
                                        <img src="{{asset('frontend/images/agents/2.jpg')}}" class="post-agent" width="31" height="31"
                                            alt="agent">By<span>Anna</span></a>
                                    <a href="blog-single.html#post-comments" class="post-comments hash-scroll">
                                        <i class="p-icon-email"></i>0
                                    </a>
                                    <div class="post-share">
                                        <i class="p-icon-socials"></i>
                                        <div class="social-links dirVertical">
                                            <a href="#" title="Facebook" class="social-link fab fa-facebook-f"></a>
                                            <a href="#" title="Twitter" class="social-link fab fa-twitter"></a>
                                            <a href="#" title="Pinterest" class="social-link fab fa-pinterest"></a>
                                            <a href="#" title="Linkedin" class="social-link fab fa-linkedin-in"></a>
                                        </div>
                                    </div>
                                </div>
                                <p class="post-content">Lorem ipsum dolor sit amet,anadipis sed do sed doeiu smod
                                    tempo...<a href="blog-single.html" class="ml-2 text-primary">(read more)</a></p>
                            </div>
                        </div>
                        <div class="post post-border post-center overlay-zoom overlay-dark">
                            <figure class="post-media">
                                <a href="blog-single.html">
                                    <img src="{{asset('frontend/images/blog/3-400x250.jpg')}}" width="400" height="250" alt="post">
                                </a>
                                <div class="post-calendar">
                                    18 Feb 2021
                                </div>
                            </figure>
                            <div class="post-details">
                                <p class="post-cats"><a href="blog.html">Vegetable</a>,<a href="blog.html"> Fruit</a>
                                </p>
                                <h3 class="post-title"><a href="blog-single.html">Aliquam id diam
                                        maecenas<br>ultricies
                                        get mauris</a>
                                </h3>
                                <div class="post-meta">
                                    <a href="blog.html" class="post-author">
                                        <img src="{{asset('frontend/images/agents/3.jpg')}}" class="post-agent" width="31" height="31"
                                            alt="agent">By<span>Anna</span></a>
                                    <a href="blog-single.html#post-comments" class="post-comments hash-scroll">
                                        <i class="p-icon-email"></i>0
                                    </a>
                                    <div class="post-share">
                                        <i class="p-icon-socials"></i>
                                        <div class="social-links dirVertical">
                                            <a href="#" title="Facebook" class="social-link fab fa-facebook-f"></a>
                                            <a href="#" title="Twitter" class="social-link fab fa-twitter"></a>
                                            <a href="#" title="Pinterest" class="social-link fab fa-pinterest"></a>
                                            <a href="#" title="Linkedin" class="social-link fab fa-linkedin-in"></a>
                                        </div>
                                    </div>
                                </div>
                                <p class="post-content">Lorem ipsum dolor sit amet,anadipis sed do sed doeiu smod
                                    tempo...<a href="blog-single.html" class="ml-2 text-primary">(read more)</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="row appear-animate">
                        <div class="col-xl-4 col-md-6">
                            <h4 class="title title-underline title-line mb-4">Trending</h4>

                            <div class="product product-list-sm mb-4">
                                <figure class="product-media">
                                    <a href="product-simple.html">
                                        <img src="{{asset('frontend/images/products/25-150x188.jpg')}}" alt="product" width="150"
                                            height="188">
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:60%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="product-simple.html" class="rating-reviews">(12)</a>
                                    </div>
                                    <h5 class="product-name">
                                        <a href="product-simple.html">
                                            Vegetability Oil
                                        </a>
                                    </h5>
                                    <span class="product-price">
                                        <del class="old-price">$90.00</del>
                                        <ins class="new-price">$36.00</ins>
                                    </span>
                                </div>
                            </div>
                            <div class="product product-list-sm mb-4">
                                <figure class="product-media">
                                    <a href="product-simple.html">
                                        <img src="{{asset('frontend/images/products/26-150x188.jpg')}}" alt="product" width="150"
                                            height="188">
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:60%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="product-simple.html" class="rating-reviews">(12)</a>
                                    </div>
                                    <h5 class="product-name">
                                        <a href="product-simple.html">
                                            Fresh Chestnut
                                        </a>
                                    </h5>
                                    <span class="product-price">
                                        <del class="old-price">$90.00</del>
                                        <ins class="new-price">$36.00</ins>
                                    </span>
                                </div>
                            </div>
                            <div class="product product-list-sm mb-4">
                                <figure class="product-media">
                                    <a href="product-simple.html">
                                        <img src="{{asset('frontend/images/products/9-150x188.jpg')}}" alt="product" width="150" height="188">
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:60%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="product-simple.html" class="rating-reviews">(12)</a>
                                    </div>
                                    <h5 class="product-name">
                                        <a href="product-simple.html">
                                            Crown Daisy
                                        </a>
                                    </h5>
                                    <span class="product-price">
                                        <del class="old-price">$90.00</del>
                                        <ins class="new-price">$36.00</ins>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-6">
                            <h4 class="title title-underline title-line mb-4">Best Selling</h4>

                            <div class="product product-list-sm mb-4">
                                <figure class="product-media">
                                    <a href="product-simple.html">
                                        <img src="{{asset('frontend/images/products/17-150x188.jpg')}}" alt="product" width="150"
                                            height="188">
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:60%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="product-simple.html" class="rating-reviews">(12)</a>
                                    </div>
                                    <h5 class="product-name">
                                        <a href="product-simple.html">
                                            Dry Jujube
                                        </a>
                                    </h5>
                                    <span class="product-price">
                                        <del class="old-price">$90.00</del>
                                        <ins class="new-price">$36.00</ins>
                                    </span>
                                </div>
                            </div>
                            <div class="product product-list-sm mb-4">
                                <figure class="product-media">
                                    <a href="product-simple.html">
                                        <img src="{{asset('frontend/images/products/11-150x188.jpg')}}" alt="product" width="150"
                                            height="188">
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:60%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="product-simple.html" class="rating-reviews">(12)</a>
                                    </div>
                                    <h5 class="product-name">
                                        <a href="product-simple.html">
                                            Fresh Pork
                                        </a>
                                    </h5>
                                    <span class="product-price">
                                        <del class="old-price">$90.00</del>
                                        <ins class="new-price">$36.00</ins>
                                    </span>
                                </div>
                            </div>
                            <div class="product product-list-sm mb-4">
                                <figure class="product-media">
                                    <a href="product-simple.html">
                                        <img src="{{asset('frontend/images/products/10-150x188.jpg')}}" alt="product" width="150"
                                            height="188">
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:60%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="product-simple.html" class="rating-reviews">(12)</a>
                                    </div>
                                    <h5 class="product-name">
                                        <a href="product-simple.html">
                                            Carrot Sap
                                        </a>
                                    </h5>
                                    <span class="product-price">
                                        <del class="old-price">$90.00</del>
                                        <ins class="new-price">$36.00</ins>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-6">
                            <h4 class="title title-underline title-line mb-4">Top Rated</h4>

                            <div class="product product-list-sm mb-4">
                                <figure class="product-media">
                                    <a href="product-simple.html">
                                        <img src="{{asset('frontend/images/products/30-150x188.jpg')}}" alt="product" width="150"
                                            height="188">
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:60%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="product-simple.html" class="rating-reviews">(12)</a>
                                    </div>
                                    <h5 class="product-name">
                                        <a href="product-simple.html">
                                            Chocolate Bun
                                        </a>
                                    </h5>
                                    <span class="product-price">
                                        <del class="old-price">$90.00</del>
                                        <ins class="new-price">$36.00</ins>
                                    </span>
                                </div>
                            </div>
                            <div class="product product-list-sm mb-4">
                                <figure class="product-media">
                                    <a href="product-simple.html">
                                        <img src="{{asset('frontend/images/products/4-150x188.jpg')}}" alt="product" width="150" height="188">
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:60%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="product-simple.html" class="rating-reviews">(12)</a>
                                    </div>
                                    <h5 class="product-name">
                                        <a href="product-simple.html">
                                            Fresh Cherry
                                        </a>
                                    </h5>
                                    <span class="product-price">
                                        <del class="old-price">$90.00</del>
                                        <ins class="new-price">$36.00</ins>
                                    </span>
                                </div>
                            </div>
                            <div class="product product-list-sm mb-4">
                                <figure class="product-media">
                                    <a href="product-simple.html">
                                        <img src="{{asset('frontend/images/products/28-150x188.jpg')}}" alt="product" width="150"
                                            height="188">
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:60%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="product-simple.html" class="rating-reviews">(12)</a>
                                    </div>
                                    <h5 class="product-name">
                                        <a href="product-simple.html">
                                            Ice Coffee
                                        </a>
                                    </h5>
                                    <span class="product-price">
                                        <del class="old-price">$90.00</del>
                                        <ins class="new-price">$36.00</ins>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </main>
        @endsection