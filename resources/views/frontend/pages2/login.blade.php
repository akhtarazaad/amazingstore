@extends('frontend.master')
@section('title','Amazing Store || Login Page')
@section('main-content')
 <main class="main">
            
            <div class="page-content">
                <div class="container pt-8 pb-10">
                    <div class="login-popup mx-auto pl-6 pr-6 pb-9">
                        <div class="form-box">
                            <div class="tab tab-nav-underline tab-nav-boxed">
                                <ul class="nav nav-tabs nav-fill align-items-center justify-content-center mb-4">
                                    <li class="nav-item">
                                        <a class="nav-link active lh-1 ls-normal" href="#signin-1">Login</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#register-1" class="nav-link lh-1 ls-normal">Register</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="signin-1">
                                        <form class="form" method="post" action="{{route('login.submit')}}">
                                         @csrf
                                            <div class="form-group">
                                                <input type="email" name="email"
                                                    placeholder="Email Address" required="required">
                                                    
                                                <input type="password" name="password"
                                                    placeholder="Password" required="required" >
                                                    
                                            </div>
                                            <div class="form-footer">
                                                <div class="form-checkbox">
                                                    <input type="checkbox" id="signin-remember-1"
                                                        name="signin-remember-1">
                                                    <label for="signin-remember-1">Remember
                                                        me</label>
                                                </div>
                                                <a href="#" class="lost-link d-block ">Lost your password?</a>
                                            </div>
                                            <button class="btn btn-dark btn-block" type="submit">Login</button>
                                        </form>
                                    </div>
                                    <div class="tab-pane" id="register-1">
                                         <form class="form" method="post" action="{{route('register.submit')}}">
                                        @csrf
                                            <div class="form-group">
                                                <input type="text" name="name"
                                                    placeholder="Your Name" required="required">

                                                <input type="text" name="email"
                                                    placeholder="Your Email Address" required="required">

                                                <input type="password"
                                                    name="password" placeholder="Password" required="required">

                                                <input type="password"
                                                name="password_confirmation" placeholder="Confirm Password" required="required">
                                            </div>
                                            <div class="form-footer mb-5">
                                                <div class="form-checkbox">
                                                    <input type="checkbox" id="register-agree-1" name="register-agree-1"
                                                        required="">
                                                    <label for="register-agree-1">I
                                                        agree to the
                                                        privacy policy</label>
                                                </div>
                                            </div>
                                            <button class="btn btn-dark btn-block" type="submit">Register</button>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
@endsection
        