@extends('frontend.master')
@section('title','Amazing Store || Order Track Page')
@section('main-content')


    <div class="container">
       
            <p style="font-weight: 500;">To track your order please enter your Order ID in the box below and press the "Track" button. This was given
                to you on your receipt and in the confirmation email you should have received.</p>
            <form class="row tracking_form my-4" action="{{route('product.track.order')}}" method="post" novalidate="novalidate" style="font-family: auto; !important">
              @csrf
                <div class="col-md-8 form-group">
                    <input type="text" class="form-control p-2"  name="order_number" placeholder="Enter your order number">
                </div>


                <div class="col-md-8 form-group">
                     <br><button type="submit" value="submit" class="btn submit_btn">Track Order</button>
                </div>
            </form><br>
        
    </div>


@endsection