@extends('backend.layouts.master')

@section('title','Order Detail')

@section('main-content')
<div class="card">
<h5 class="card-header">Quick       <a href="{{route('order.pdf',$quick->id)}}" class=" btn btn-sm btn-primary shadow-sm float-right"><i class="fas fa-download fa-sm text-white-50"></i> Generate PDF</a>
  </h5>
  <div class="card-body">
    @if($quick)
    <table class="table table-striped table-hover">
      @php
          $shipping_charge=DB::table('quick_buy')->where('id',$quick->quick_id)->pluck('total_amount');
      @endphp 
      <thead>
        <tr>
            <th>S.N.</th>
            <th>quick No.</th>
            <th>Name</th>
            <th>Email</th>
            <th>Quantity</th>
            <th>Charge</th>
            <th>Total Amount</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <tr>
            <td>{{$quick->id}}</td>
            <td>{{$quick->order_number}}</td>
            <td>{{$quick->first_name}} {{$quick->last_name}}</td>
            <td>{{$quick->email}}</td>
            <td>{{$quick->quantity}}</td>
            <td>@foreach($shipping_charge as $data) $ {{number_format($data,2)}} @endforeach</td>
            <td>${{number_format($quick->total_amount,2)}}</td>
            <td>
                @if($quick->status=='new')
                  <span class="badge badge-primary">{{$quick->status}}</span>
                @elseif($quick->status=='process')
                  <span class="badge badge-warning">{{$quick->status}}</span>
                @elseif($quick->status=='delivered')
                  <span class="badge badge-success">{{$quick->status}}</span>
                @else
                  <span class="badge badge-danger">{{$quick->status}}</span>
                @endif
            </td>
            <td>
                <a href="{{route('quick.edit',$quick->id)}}" class="btn btn-primary btn-sm float-left mr-1" style="height:30px; width:30px;border-radius:50%" data-toggle="tooltip" title="edit" data-placement="bottom"><i class="fas fa-edit"></i></a>
                <form method="POST" action="{{route('quick.destroy',[$quick->id])}}">
                  @csrf 
                  @method('delete')
                      <button class="btn btn-danger btn-sm dltBtn" data-id={{$quick->id}} style="height:30px; width:30px;border-radius:50%" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fas fa-trash-alt"></i></button>
                </form>
            </td>
          
        </tr>
      </tbody>
    </table>

    <section class="confirmation_part section_padding">
      <div class="order_boxes">
        <div class="row">
          <div class="col-lg-6 col-lx-4">
            <div class="order-info">
              <h4 class="text-center pb-4">ORDER INFORMATION</h4>
              <table class="table">
                    <tr class="">
                        <td>Quick Number</td>
                        <td> : {{$quick->order_number}}</td>
                    </tr>
                    <tr>
                        <td>Quick Date</td>
                        <td> : {{$quick->created_at->format('D d M, Y')}} at {{$quick->created_at->format('g : i a')}} </td>
                    </tr>
                    <tr>
                        <td>Quantity</td>
                        <td> : {{$quick->quantity}}</td>
                    </tr>
                    <tr>
                        <td>Quick Status</td>
                        <td> : {{$quick->status}}</td>
                    </tr>
                    <tr>
                      @php
                          $shipping_charge=DB::table('quick_buy')->where('id',$quick->shipping_id)->pluck('total_amount');
                      @endphp
                        <td>Shipping Charge</td>
                        <td> : $ {{number_format($shipping_charge[0],2)}}</td>
                    </tr>
                    <tr>
                      <td>Coupon</td>
                      <td> : $ {{number_format($quick->coupon,2)}}</td>
                    </tr>
                    <tr>
                        <td>Total Amount</td>
                        <td> : $ {{number_format($quick->total_amount,2)}}</td>
                    </tr>
                    <tr>
                        <td>Payment Method</td>
                        <td> : @if($quick->payment_method=='cod') Cash on Delivery @else Paypal @endif</td>
                    </tr>
                    <tr>
                        <td>Payment Status</td>
                        <td> : {{$quick->payment_status}}</td>
                    </tr>
              </table>
            </div>
          </div>

          <div class="col-lg-6 col-lx-4">
            <div class="shipping-info">
              <h4 class="text-center pb-4">SHIPPING INFORMATION</h4>
              <table class="table">
                    <tr class="">
                        <td>Full Name</td>
                        <td> : {{$quick->first_name}} {{$quick->last_name}}</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td> : {{$quick->email}}</td>
                    </tr>
                    <tr>
                        <td>Phone No.</td>
                        <td> : {{$quick->phone}}</td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td> : {{$quick->address1}}, {{$quick->address2}}</td>
                    </tr>
                    <tr>
                        <td>Country</td>
                        <td> : {{$quick->country}}</td>
                    </tr>
                    <tr>
                        <td>Post Code</td>
                        <td> : {{$quick->post_code}}</td>
                    </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    @endif

  </div>
</div>
@endsection

@push('styles')
<style>
    .order-info,.shipping-info{
        background:#ECECEC;
        padding:20px;
    }
    .order-info h4,.shipping-info h4{
        text-decoration: underline;
    }

</style>
@endpush