@extends('backend.layouts.master')

@section('title','Order Detail')

@section('main-content')
<div class="card">
  <h5 class="card-header">quick Edit</h5>
  <div class="card-body">
    <form action="{{route('quick.update',$quick->id)}}" method="POST">
      @csrf
      @method('PATCH')
      <div class="form-group">
        <label for="status">Status :</label>
        <select name="status" id="" class="form-control">
          <option value="">--Select Status--</option>
          <option value="new" {{(($quick->status=='new')? 'selected' : '')}}>New</option>
          <option value="process" {{(($quick->status=='process')? 'selected' : '')}}>process</option>
          <option value="delivered" {{(($quick->status=='delivered')? 'selected' : '')}}>Delivered</option>
          <option value="cancel" {{(($quick->status=='cancel')? 'selected' : '')}}>Cancel</option>
        </select>
      </div>
      <button type="submit" class="btn btn-primary">Update</button>
    </form>
  </div>
</div>
@endsection

@push('styles')
<style>
    .order-info,.shipping-info{
        background:#ECECEC;
        padding:20px;
    }
    .order-info h4,.shipping-info h4{
        text-decoration: underline;
    }

</style>
@endpush