<!DOCTYPE html>
<html lang="en">

@include('backend.layouts.head')

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    @include('backend.layouts.sidebar')
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        @include('backend.layouts.header')
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        @yield('main-content')
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
      @include('backend.layouts.footer')
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  <script>
    @if(Session::has('message'))
    var type = "{{Session::get('alert-type','info')}}"
    switch(type){
      case 'info':
      toastr.info("{{Session::get('message')}}");
      break;
      case 'success':
      toastr.success("{{Session::get('message')}}");
      break;
      case 'warning':
      toastr.warning("{{Session::get('message')}}");
      break;
      case 'error':
      toastr.error("{{Session::get('message')}}");
      break;
    }
    @endif
  </script>

</body>

</html>
