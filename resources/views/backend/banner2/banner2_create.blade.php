@extends('backend.layouts.master')

@section('title','E-SHOP || Banner Create')

@section('main-content')

<div class="card">
    <h5 class="card-header">Add Banner</h5>
    <div class="card-body">
      <form method="post" action="{{ route('banner2.store') }}" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
          <label for="inputTitle" class="col-form-label">Title <span class="text-danger">*</span></label>
        <input type="text"  name="title" class="form-control" > 
        @error('title')
        <span class="text-danger">{{$message}}</span>
        @enderror
        </div>

        <div class="form-group">
           <label for="inputTitle" class="col-form-label">Description <span class="text-danger">*</span></label>
          <input type="text" name="description" class="form-control" >
          @error('description')
          <span class="text-danger">{{$message}}</span>
          @enderror
        </div>

       <div class="form-group">
        <h5>Banner Image <span class="text-danger">*</span></h5>
        <div class="controls">
       <input type="file" name="slider_image" class="form-control">
         @error('slider_image') 
       <span class="text-danger">{{ $message }}</span>
       @enderror 
        </div>
      </div>
        
       
        <div class="form-group mb-3">
         
           <button class="btn btn-success" type="submit">Add New</button>
        </div>
      </form>
    </div>
</div>

@endsection

@push('styles')
<link rel="stylesheet" href="{{asset('backend/summernote/summernote.min.css')}}">
@endpush
@push('scripts')
<script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
<script src="{{asset('backend/summernote/summernote.min.js')}}"></script>
<script>
    $('#lfm').filemanager('image');

    $(document).ready(function() {
    $('#description').summernote({
      placeholder: "Write short description.....",
        tabsize: 2,
        height: 150
    });
    });
</script>
@endpush