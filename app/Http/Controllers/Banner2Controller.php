<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Banner2;
use Carbon\Carbon;
use Image;

class Banner2Controller extends Controller
{

	public function Banner2View(){
		$banners2 = Banner2::latest()->get();
		return view('backend.banner2.banner2_view',compact('banners2'));
	}

	 public function create()
    {
        return view('backend.banner2.banner2_create');
    }

    public function Banner2Store(Request $request)
    {
    	$request->validate([

    		'slider_image' => 'required',
    	],[
    		'slider_image.required' => 'Plz Select One Image',

    	]);

    	$image = $request->file('slider_image');
    	$name_gen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
    	Image::make($image)->resize(1600,550)->save('storage/photos/1/banner/'.$name_gen); 
    	$save_url = 'storage/photos/1/banner/'.$name_gen;

	Banner2::insert([
		'title' => $request->title,
		'description' => $request->description,
		'slider_image' => $save_url,

    	]);

	    $notification = array(
			'message' => 'Slider Inserted Successfully',
			'alert-type' => 'success'
		);

		return redirect()->route('manage-banner');

  
    }

    public function Banner2Edit($id){
    	 $banners2 = Banner2::findOrFail($id);
		return view('backend.banner2.banner2_edit',compact('banners2'));

    }

    public function Banner2Update(Request $request){
    	$banner2_id = $request->id;
    	$old_img = $request->old_image;

    	if ($request->file('slider_image')) {

    	unlink($old_img);
    	$image = $request->file('slider_image');
    	$name_gen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
    	Image::make($image)->resize(1600,550)->save('storage/photos/1/'.$name_gen);
    	$save_url = 'storage/photos/1/'.$name_gen;

	Banner2::findOrFail($banner2_id)->update([
		'title' => $request->title,
		'description' => $request->description,
		'slider_image' => $save_url,

    	]);

	    $notification = array(
			'message' => 'Banner Updated Successfully',
			'alert-type' => 'info'
		);

		return redirect()->route('manage-banner')->with($notification);

    	}else{

    	Banner2::findOrFail($banner2_id)->update([
		'title' => $request->title,
		'description' => $request->description,


    	]);

	    $notification = array(
			'message' => 'Banner Updated Without Image Successfully',
			'alert-type' => 'info'
		);

		return redirect()->route('manage-banner')->with($notification);

    	} // end else 
    }

    public function Banner2Delete($id){
    	$banners2 = Banner2::findOrFail($id);
    	$img = $banners2->slider_image;
    	unlink($img);
    	Banner2::findOrFail($id)->delete();

    	$notification = array(
			'message' => 'Slider Delectd Successfully',
			'alert-type' => 'info'
		);

		return redirect()->route('manage-banner')->with($notification);

    }

    public function Banner2Inactive($id){
    	Banner2::findOrFail($id)->update(['status' => 0]);

    	$notification = array(
			'message' => 'Slider Inactive Successfully',
			'alert-type' => 'info'
		);

		return redirect()->route('manage-banner')->with($notification);

    }


   public function Banner2Active($id){
   	Banner2::findOrFail($id)->update(['status' => 1]);

    	$notification = array(
			'message' => 'Slider Active Successfully',
			'alert-type' => 'info'
		);

		return redirect()->route('manage-banner')->with($notification);
    	
    }







}