<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\View;

use App\Quick;
use DB;
use Illuminate\Support\Str;

use Illuminate\Http\Request;

class QuickController extends Controller
{
      // public function index() {
    //     return view('/');
    // }
    public function index()
    {
        $quick=Quick::orderBy('id','DESC')->paginate(10);
        return view('backend.quick.index')->with('quick',$quick);
    }
    public function placeorderSubmit(Request $request){
        //   print_r($request->all());
        // dd($request);
        $quick_buy = new Quick();

        $quick_buy->products_id = 3;
        $quick_buy->user_id = 1;
        $quick_buy->sub_total = 1;
        $quick_buy->shipping_id = 1;
        $quick_buy->coupon = 2; 
        $quick_buy->slug = $request->input('slug');
        $quick_buy->total_amount = $request->input('total_amount');
        $quick_buy->quantity = $request->input('quantity');
        $quick_buy->payment_method = $request->input('payment_method');
       
        $quick_buy->payment_status =  "unpaid";
        $quick_buy->status = "new";
        $quick_buy->first_name = $request->input('first_name');
        $quick_buy->last_name = "manshaaaa";
       
        $quick_buy->email = $request->input('email');
        $quick_buy->phone = $request->input('phone');
        $quick_buy->country = "pakistan";
        $quick_buy->post_code = 56000;
       
        // $quick_buy->address2 = "lahore";
        $quick_buy->address = $request->input('address');
        
        $quick_buy->save();
        // dd($quick_buy);
        
        return redirect()->route('order-here');
        
    }

    public function edit($id)
    {
        $quick=Quick::find($id);
        return view('backend.quick.edit')->with('quick',$quick);
    }

    public function show($id)
    {
        $quick=Quick::find($id);
        // dd($quick);
        // return $order;
        return view('backend.quick.show')->with('quick',$quick);
        
    }

       public function update(Request $request, $id)
    {
        $quick=Quick::find($id);
        // dd($quick->cart);
        $this->validate($request,[
            'status'=>'required|in:new,process,delivered,cancel'
        ]);
        $data=$request->all();
        // return $request->status;
        // dd($request);
        // if($request->status=='delivered'){
        //     foreach($quick->cart as $cart){
                
        //         $product=$cart->product;
        //         // return $product;
        //         $product->stock -=$cart->quantity;
        //         $product->save();
        //     }
        // }
        $status=$quick->fill($data)->save();
        if($status){
            request()->session()->flash('success','Successfully updated order');
        }
        else{
            request()->session()->flash('error','Error while updating order');
        }
        return redirect()->route('quick.index');
    }


    public function destroy($id)
    {
        $quick=Quick::find($id);
        if($quick){
            $status=$quick->delete();
            if($status){
                request()->session()->flash('success','quick Successfully deleted');
            }
            else{
                request()->session()->flash('error','quick can not deleted');
            }
            return redirect()->route('quick.index');
        }
        else{
            request()->session()->flash('error','quick can not found');
            return redirect()->back();
        }
    }
}