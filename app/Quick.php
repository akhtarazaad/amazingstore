<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Cart;
class Quick extends Model
{
    protected $table = 'quick_buy';
    protected $fillable = [
        'products_id',
        'user_id',
        'sub_total',
        'shipping_id',
        'coupon',
        'slug',
        'total_amount',
        'quantity',
        'payment_method',
        'payment_status',
        'status',
        'first_name',
        'last_name',
        'email',
        'phone',
        'country',
        'post_code',
        'address',
        
        
    ];
    public function cart(){
        return $this->belongsTo(Cart::class,'cart_id');
    }
}
